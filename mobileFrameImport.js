/*
Process:
 - This program is called automatically via CLVOCLINK. once its finished running it will run again after a 120 second delay.
 - This program will read PW_ORDER_HEADER on the Mobile Frame SQL database to look for headers that have a complete count of detail records.
 - When a complete header is found it will query PW_ORDER_DETAIL with that headers GUID to pull all of the associated detail records. 
 - Those detail records will be written to PWAFIL.ORDERSIN on the iSeries. After which, this program will call CLORDERSIN which will process those orders.
 - It will then flag the detail records on the SQL db that have been imported and flag the header record as well to show that its records were imported.
 - Finally, if it is between 11 & 11:15 am it will import all header records regardless of whether or not they have all detail records in. 
 - If any incomplete headers are found and their details are imported it will flag that header record on the SQL db with the actual number of records that were imported.
 - Error alerts are built in throughout using the errorNotify function which uses MFIMPORT.SNDEMAIL and MFIMPORT.SNDMESSAGE CL programs.

Author: Tyler Hobbs
Date: 11/03/2021
*/

const dayjs = require("dayjs");

async function mobileFrameImport() {

  pjs.defineTable("ordersin","ORDERSIN", {write: true, userOpen: true, keyed:true});

  const programName = 'MF_Import';
  
  let headerTable = "HeaderCopy";
  let detailTable = "DetailCopy";
  let programForCLCall = "CLORDERSIX";

  const currentPort = profound.settings.port;

  if(currentPort === 8083){ //Running on Production Instance
    headerTable = "PW_ORDER_HEADER";
    detailTable = "PW_ORDER_DETAIL";
    programForCLCall = "PWACLP/CLORDERSIN";
  }


  const auto = pjs.require("pjslogging/autoLog.js");
  
    let logPGM = "mobileframe-import/mobileFrameImport";
    let logSeverity = 5;
    auto.autoLog(`${programName} is running on ${currentPort}...${dayjs()}`, programName, 5);
    console.log(`${programName} is running on ${currentPort}...${dayjs()}`);

 
    let date = new Date();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    var getHeaderCount = [];
    var getDetailRecord = [];
  
    /* // We no longer look if the table is locked sicne it cannot be locked since we call CLORDERSIN. 03/27/2023
  
    // function getAvailableFlag(){
    //   let returnFlag;
    //   pjs.define("isAvailble", { type: 'char', length: 1, dataArea: 'isAvailble' });
    //   pjs.retrieveDataArea(isAvailble);
    //   console.log("data area is=" + isAvailble);
    //   returnFlag = isAvailble;
    //   return returnFlag;
    // }
  
    // if (getAvailableFlag() != "Y")
    // {
    //   console.log("Ending MF Import PGM. isAvailble Data Area is not set to Y. Files may be locked.")
    //   return;
    // }
    */
  
    /*let headerQueryParms = 
      {
        headerTableValue: headerTable, 
        detailTableValue: detailTable
      };
  
      console.log(headerQueryParms);*/
  
    var headerQuery =
      "Select oh.GUID, " +
      "oh.id As HeaderID, " +
      "oh.FLAGGED, " +
      "oh.LINE_COUNT, " +
      "count(od.id) as DetailCount " +
      `FROM ${headerTable} AS oh ` + 
      `inner join ${detailTable} AS od on od.ORDER_HEADER_GUID = oh.GUID ` + 
      "WHERE oh.FLAGGED = '' AND oh.deleted_by < 0  AND oh.VENDOR_NO not in ('4','7','8','10')" + //VENDORS: 4 = DIAZ, 7 = VMC, 8 = BTC
      "GROUP BY oh.id, oh.GUID, oh.LINE_COUNT, od.deleted_by, oh.FLAGGED";
    
    auto.autoLog(`Querying mobile frame to retrieve orders from header table...`, programName, 5);
    console.log(`Querying mobile frame to retrieve orders from header table...`);

    try {
      getHeaderCount = pjs.query(pjs.getDB("Server2012-MFS"), headerQuery);
    } catch (err) {
      errorNotify(err, 20);
      return;
    }

    auto.autoLog(`Record count returned from header table ${getHeaderCount.length}`, programName, 5);
    console.log(`Record count returned from header table ${getHeaderCount.length}`);
    console.log(getHeaderCount);
  
  //NOTE: 10/12/2023 Chris Bates asked me to turn off the mismatched record import.  I am leaving the code here in case we need it again in the future. TH
    // if (hours === 11 && minutes <= 15) { //IF IT IS BETWEEN 11 & 11:15 PULL ALL HEADER RECORDS REGARDLESS OF COMPLETION. 10/13/21
    //   console.log( "It is between 11 and 11:15 so mismatched records are being flagged and imported next run for the Mobile Frame Import Process.");
  
    //   for (let i = 0; i < getHeaderCount.length; i++) {
    //     let allGUIDs = getHeaderCount[i]["GUID"];
    //     orderDetailQuery(allGUIDs);  
        
    //     //write to Order_Header the actualy # of deatil records into the "Mismatched" field, adjsut count to match exprected count so it'll be imported next time.
    //     if (getHeaderCount[i]["LINE_COUNT"] !== getHeaderCount[i]["DetailCount"]) {
    //       console.log("Incomplete Headers are pressent, running mismatch step in Mobile Frame Import Process.");
    //       let count = getHeaderCount[i]["DetailCount"];
    //       let mismatchedGuid = getHeaderCount[i]["GUID"];
    //       writeIncompleteHeaders(mismatchedGuid, count);
    //     }
    //   }
    // } else { // IF IT IS NOT BETWEEN 11 & 11:15 ONLY PULL RECORDS FOR COMPLETED HEADERS. 10/13/21
      for (let i = 0; i < getHeaderCount.length; i++) {
        if (getHeaderCount[i]["LINE_COUNT"] === getHeaderCount[i]["DetailCount"]) {
          let completeGUIDs = getHeaderCount[i]["GUID"];
          orderDetailQuery(completeGUIDs);
        }
      }
    // }
  
  // // not currently using GOT import!!!!!!!!!!
  //   var GOTImport = pjs.require('./GOTImport.js');
  
  //  // CALL GOTIMPORT PROCESS
  //   GOTImport = GOTImport.getGOTOrders();
  
  //   if (GOTImport.ErrMessage) {
  //     err = GOTImport.ErrMessage;
  //     console.log(err);
  //     errorNotify(err, 20);
  //   }
    auto.autoLog(`Calling ${programForCLCall}...`, programName, 5);
    console.log(`Calling ${programForCLCall}...`);

    pjs.call(programForCLCall);

    auto.autoLog(`${programName} is done running on ${currentPort}...${dayjs()}`, programName, 5);
    console.log(`${programName} is done running on ${currentPort}...${dayjs()}`);
  
    //------------------------------------------------------------PROGRAM STOPS HERE------------------------------------------------------------
  
    //------------------------------------------------------------Run Detail Querry-------------------------------------------------------------
    function orderDetailQuery(guid) {
  
      let detailQueryParms = 
        { 
          valueGUID: guid 
        };
  
      var detailQuery =
        "Select " +
        "od.id As DetailId, " +
        "oh.id As HeaderId, " +
        "od.LINE_ITEM_NO as lineItmNum, " +
        "oh.STORE_NO AS Store, " +
        "oh.ORDER_DT AS OrderDate, " +
        "oh.ORDER_TYPE as orderType, " +
        "oh.VENDOR_NO as vendorNumber, " +
        "od.QUANTITY as quantity, " +
        "od.ORDER_HEADER_GUID as Guid " +
        `FROM ${headerTable} AS oh ` + 
        `inner join ${detailTable} AS od ` + 
        "ON od.ORDER_HEADER_GUID = oh.GUID " +
        "WHERE od.FLAGGED = '' AND od.deleted_by < 0 AND oh.GUID  = @valueGUID"; 

      auto.autoLog(`Querying mobile frame to detail orders for GUID ${guid}`, programName, 5);
      console.log(`Querying mobile frame to detail orders for GUID ${guid}`);

      try {
        getDetailRecord = pjs.query(pjs.getDB("Server2012-MFS"),detailQuery,detailQueryParms);
  
        if (!getDetailRecord || getDetailRecord.length <= 0) {
          errorNotify("No records found in getDetailRecord MobileFrame SQL Table for guid = " + guid, 10);
          auto.autoLog(`No records found in getDetailRecord MobileFrame SQL Table for guid = ${guid}`, programName, 20);
          console.log(`No records found in getDetailRecord MobileFrame SQL Table for guid = ${guid}`);
          return;
        } 
  
      } catch (err) {
       errorNotify(err, 20);
       auto.autoLog(`Catch error occurred while fetching records in getDetailRecord MobileFrame SQL Table for guid = ${guid}`, programName, 20);
       console.log(`Catch error occurred while fetching records in getDetailRecord MobileFrame SQL Table for guid = ${guid}`);
        return;
      }

      ordersin.open();
      writeToOrdersIn(getDetailRecord);
      ordersin.close();
    }
  
    //------------------------------------Write the orders to ORDERSIN-------------------------------------
    function writeToOrdersIn(detailRecords) {
      var d = new Date();
      d = d.toISOString();
      d = d.replace(/T/g, " ");
      d = d.replace(/Z/g, "");
      var writeDate = String(d);

      auto.autoLog(`Writing records to ordersin table...`, programName, 5);
      console.log(`Writing records to ordersin table...`);
  
      for (let i = 0; i < detailRecords.length; i++) {
  
        var detailIDRecord = detailRecords[i]["DetailId"];
        var callID = detailRecords[i]["HeaderId"]; 
        var originalStoreNumber = detailRecords[i]["Store"];
        var storeNumber = padLeadingZeros(originalStoreNumber);
        var type = detailRecords[i]["orderType"]; 
        var vendor = detailRecords[i]["vendorNumber"]; 
        var lineItemNumber = detailRecords[i]["lineItmNum"]; 
        var originalQty = detailRecords[i]["quantity"];
          if(originalQty < 1) originalQty = 1;
        var qty = padLeadingZeros(originalQty);
        var originalDate = detailRecords[i]["OrderDate"]; 
        var orderDate = convertDate(originalDate);
        var processed = "";
        var Flag1 = "";
        var Flag2 = "";
  
        oincal = callID;  
        oinstr = storeNumber;
        ointyp = type;
        oinvnd = vendor;
        oinitm = lineItemNumber;
        oinqty = qty;
        oindte = orderDate;
        oinprs = processed;
        oinfg1 = Flag1;
        oinfg2 = Flag2;
        ordersin.write();
  
      //------------------------update the Order Detail file in SQL table to flag the items as "imported."------------------------
        // var newValues = {
        //   flagged: "b",
        //   last_update: writeDate,
        //   last_updated_by: -1
        // };
  
        try {
  
          // let filter = pjs.data.createCondition("id", "=", detailIDRecord);
          // pjs.data.update(pjs.getDB("Server2012-MFS"), detailTable, filter, newValues); 
          //ONCE THIS VERSION IS BACK IN PROD MAKE SURE TO TEST THAT USING THE VAR detailTable WORKS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  //NOTE: THIS IS A TEMPORARY FIX TO GET THE IMPORTED COUNT TO UPDATE IN THE HEADER FILE.  THE ABOVE CODE IS THE CORRECT WAY TO DO IT, BUT IT IS NOT WORKING. 03/27/2023        
          let detailParms = 
            { 
              valueDate: writeDate, 
              detailID: detailIDRecord 
            };

            auto.autoLog(`Flagging records in detail table...`, programName, 5);
            console.log(`Flagging records in detail table...`);
          let detailTestQuery = `update ${detailTable} set flagged = 'b', last_update = @valueDate, last_updated_by = -1 where id = @detailID`;
          pjs.query(pjs.getDB("Server2012-MFS"),detailTestQuery,detailParms);
  
        } catch (err) {
          errorNotify(err, 20);
          return;
        }
        //------------Update the header file.  We only need to do this once, on the last detail record----------------------
          if (i==detailRecords.length - 1) {
            // var localTimeStamp = pjs.char(pjs.timestamp(),"*iso");
            // localTimeStamp = localTimeStamp.replace(/T/g, " ");
            // localTimeStamp = localTimeStamp.substring(0,19) + ".000";

            var localTimeStamp = dayjs().format('YYYY-MM-DD HH:mm:ss');
  
            // var newValues2 = {
            //   flagged: "b",
            //   last_update: writeDate,
            //   last_updated_by: -1,
            //   EXPORTED : localTimeStamp
            // };

            auto.autoLog(`Flagging records in header table...`, programName, 5);
            console.log(`Flagging records in header table...`);
            try {
  
              // let filter2 = pjs.data.createCondition("id", "=", callID);
              // pjs.data.update(pjs.getDB("Server2012-MFS"), headerTable, filter2, newValues2); 
              //ONCE THIS VERSION IS BACK IN PROD MAKE SURE TO TEST THAT USING THE VAR headerTable WORKS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  //NOTE: THIS IS A TEMPORARY FIX TO GET THE IMPORTED COUNT TO UPDATE IN THE HEADER FILE.  THE ABOVE CODE IS THE CORRECT WAY TO DO IT, BUT IT IS NOT WORKING. 03/27/2023
              let headerParms = 
                { 
                  writeDateRecord: writeDate,
                  timeStampRecord: localTimeStamp, 
                  callIDRecord: callID 
                };

              let headerTestQuery = `update ${headerTable} set flagged = 'b', last_update = @writeDateRecord, last_updated_by = -1, EXPORTED = @timeStampRecord where id = @callIDRecord`;
              pjs.query(pjs.getDB("Server2012-MFS"),headerTestQuery,headerParms);
              
            } catch (err) {
              errorNotify(err, 20);
              return;
            }
          } 
      }
    }
  
  //NOTE: 10/12/2023 Chris Bates asked me to turn off the mismatched record import.  I am leaving the code here in case we need it again in the future. TH
  //------------Update the header file with actual imported count for incomplete records found between 11 & 11:15----------------------
  //   function writeIncompleteHeaders(mismatchedGuid, count) {
      
  //     // let filter = [pjs.data.createCondition("GUID", "=", mismatchedGuid)];
  //     // let newValues = {RECEIVED_COUNT: count};
  //     // pjs.data.update(pjs.getDB("Server2012-MFS"), headerTable, filter, newValues);
  //     //ONCE THIS VERSION IS BACK IN PROD MAKE SURE TO TEST THAT USING THE VAR headerTable WORKS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  // //NOTE: THIS IS A TEMPORARY FIX TO GET THE IMPORTED COUNT TO UPDATE IN THE HEADER FILE.  THE ABOVE CODE IS THE CORRECT WAY TO DO IT, BUT IT IS NOT WORKING. 03/27/2023
  //     let headerTestParms = 
  //       { 
  //         countValue: count,
  //         mismatchedGuidValue: mismatchedGuid
  //       };
  
  //     let headerTestQuery2 = `update ${headerTable} set RECEIVED_COUNT = @countValue where GUID = @mismatchedGuidValue`;
  //     pjs.query(pjs.getDB("Server2012-MFS"),headerTestQuery2, headerTestParms);
  //   }
  
    function convertDate(dateIn) {
      var D = dateIn.toISOString();
      D = D.replace(/-|:/g, "");
      D = D.replace(/T/g, " ");
      var newDate = D.slice(0, -3);
      return newDate;
    }
  
    function padLeadingZeros(num) {
      var s = num + '';
      while (s.length < 3) s = '0' + s;
      return s;
    }
  
    function errorNotify(error, severity){
      //severity   action
      // 10        console.log
      // 20        email + console.log
      // 30        send message to qsysopr on 400 + email + console.log
  
      let messagePrefix = pjs.char(pjs.timestamp(), "*iso")  +" Mobile Frame Import Error: ";
      console.log(messagePrefix + error);
      logSeverity = 10;
      // auto.autoLog(messagePrefix + error, logPGM, logSeverity );
  
      pjs.define("message", { type: 'char', length: 512});
      message = messagePrefix + error;
  
      if (severity >= 10)
      {
        console.log(`Trying to send email via SNDEMAIL with message: ${message}`);
        logSeverity = 10;
        // auto.autoLog(`Trying to send email via SNDEMAIL with message: ${message}`, logPGM, logSeverity );
        pjs.call("SNDEMAIL",  message);
      }
  
      if (severity >= 20)
      {
        console.log(`Trying to send message to qsysopr via SNDMESSAGE with message: ${message}`);
        logSeverity = 10;
        // auto.autoLog(`Trying to send message to qsysopr via SNDMESSAGE with message: ${message}`, logPGM, logSeverity );
        pjs.call("SNDMESSAGE",  message); //  TYLER no message was sent
      }
    }
    logSeverity = 5;
    // auto.autoLog("Program End", logPGM, logSeverity );   
  }
  exports.run = mobileFrameImport;
  