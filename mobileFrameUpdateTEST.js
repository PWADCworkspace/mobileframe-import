function mobileFrameUpdate() {
  console.time("starting");
  //STEP1: EXECUTE SQL TO CLEAR PW_CHNG_ITEM_test
  console.time("Deleting previous Store Items.");
  try {
    pjs.data.delete(
      pjs.getDB("Server2012-MFS-UpdateUpdate"),
      `${mobileUpdateHoldTable}`
    );
  } catch (error) {
    console.error(error);
  }
  console.timeEnd("Deleting previous Store Items.");

  let array = [];

  var sqlStatement = "SELECT " +
    "TYLER.CRIPFILE.STRNO, " +
    "concat(TYLER.invmst.itmcde, " +
    "TYLER.invmst.chkdig) as itemCodeLong, " +
    "TYLER.CRIPFILE.MULTI, " +
    "TYLER.CRIPFILE.CRPSRP, " +
    "TYLER.CRIPFILE.YTDPUR, " +
    "TYLER.CRIPFILE.WKPURC, " + 
    "TYLER.CRIPFILE.TRLPCT, " + 
    "TYLER.CRIPFILE.LPURDT, " +
    "TYLER.CRIPFILE.TRLSRP, " + 
    "TYLER.CRIPFILE.BOOK, " +
    "TYLER.CRIPFILE.SRPCD, " +
    "TYLER.ARFILE.STATUS " +
    "FROM TYLER.CRIPFILE " +
    "JOIN TYLER.ARFILE " +
    "ON TYLER.CRIPFILE.STRNO = TYLER.ARFILE.ARFKEY " +
    "JOIN TYLER.invmst " +
    "ON TYLER.CRIPFILE.CODE = TYLER.invmst.itmcde " +
    "WHERE (TYLER.ARFILE.STATUS='A') OR (TYLER.CRIPFILE.STRNO=169) order by TYLER.cripfile.strno";

  var c1 = pjs.allocStmt();
  c1.executeDirect(sqlStatement);

  if (sqlstate == "00000") {
    while (c1.hasMoreRows()) {
      var records = c1.fetch(200);
      // console.log(records);
      for (var i = 0; i < c1.rowCount(); i++) {
        var record = records[i];

        if (typeof record !== "undefined") {
          var lastYearMovement = record.trlsrp;
          var yearToDatePurchase = record.ytdpur;
          var lastYearWeekly = record.trlpct;
          var weeklyCycle = record.wkpurc;

          if (records.trlpct == 0 && records.wkpurc == 0) {
            var averageWeeklyMovementCalculation = 0;
          } else {
            averageWeeklyMovementCalculation =
              (lastYearMovement + yearToDatePurchase) /
              (lastYearWeekly + weeklyCycle);
          }

          averageWeeklyMovement = averageWeeklyMovementCalculation.toString();
          record.avgWklyMvmt = averageWeeklyMovement.slice(0, 10);
          array.push({
            storeNumber: record.strno, //STORE_NO
            itemCode: record.itemcodelong, //ITEM_CODE
            multi: record.multi, //MULTI
            cripSRP: record.crpsrp, //SRP
            yearToDatePurchase: record.ytdpur, //YTD_PURCHASE
            lastYearPurchase: record.trlsrp, //LY_PURCHASE
            weeklyPurchase: record.wkpurc, //YTD_Weeks_In_INV
            weeksInInventory: record.trlpct, // LYWEEK
            book: record.book, //BOOK
            srpCode: record.srpcd, //SRP_CODE
            averageWeeklyMovement: record.avgWklyMvmt //AVG_WEEKLY_MVT_UNITS
          });
        }
      }
    }
    c1.close();
  }
  console.log(array.length);
  console.time("Writing new Mobile Frame Warehouse Items.");

  var detailQuery =
    "INSERT INTO PW_CHNG_ITEMS_STORE_test " +
    "(STORE_NO, " +
    "ITEM_CODE, " +
    "MULTI, " +
    "SRP, " +
    "YTD_PURCHASE, " +
    "LY_PURCHASE, " +
    "YTD_Weeks_In_Inv, " +
    "LYWEEK, " +
    "BOOK, " +
    "SRP_CODE, " +
    "AVG_WEEKLY_MVT_UNITS) " +
    "VALUES (?,?,?,?,?,?,?,?,?,?,?)";

  try {
    // pjs.data.add(pjs.getDB("Server2012-MFS-UpdateUpdate"), "MobileUpdateUpdate.PW_CHNG_ITEM_test", itemArray);
    pjs.query(
      pjs.getDB("Server2012-MFS-UpdateUpdate"),
      detailQuery,
      array,
      null,
      null,
      100
    );

    if (sqlstate >= "02000") {
      //sqlError = true;
      sqlMsg =
        "SQL error during MobileFrame Warehouse Write. SQLstate: " + sqlstate;
      // log.pushToLog(storeNumber, userName, sqlMsg);
      console.log(sqlMsg);
      return;
    }
  } catch (err) {
    //sqlError = true;
    sqlMsg = "Sql Insert catch error during MobileFrame Warehouse Write.";
    // log.pushToLog(storeNumber, userName, sqlMsg);
    console.log(`STOP: ${err}`);
    return;
  }
  console.timeEnd("Writing new Mobile Frame Warehouse Items.");

  console.timeEnd("starting");
}
exports.run = mobileFrameUpdate;



//Dottis CL PGM to update the mfcripuf table before running this pgm: CLMFCRIPU

/*
Author : Samuel Gray and Tyler Hobbs
Last Modified: Samuel Gray
Modified Date: 02//2024

STEP 1:

STEP 2:

STEP 3:

STEP 4:


*/

async function mobileFrameUpdate() {
  console.time("Updating Mobile Frame Warehouse Items.");

  let cripFileArray = [];
  var warehouseItemArray = [];
  const dayjs = require("dayjs");
  // const customParseFormat = require('dayjs/plugin/customParseFormat');
  // dayjs.extend(customParseFormat);

  //Setting Tables
  //PROD
  const enviroment = "Prod";

  const storeItemsTable = 'dotti.mfcripuf';

  //old DB
  const mobileFrameUpdateDB = "Server2012-MFS-UpdateUpdate";
  const mobileUpdateHoldTable = "PW_CHNG_ITEMS_STORE_test";

  //new DB
  const mobileNew5DayOrders = "PW_ITEM_INBOUND_COPY"

  /*
  //DEV
  const enviroment = "Dev";

  const storeItemsTable = 'dotti.mfcripuf';
  const invMasterTable = "pwafil.invmst";
  const supTable = "pwafil.supfile";
  const omitTable = "pwafil.omitmsi";


  //old DB
  const mobileFrameUpdateDB = "Server2012-MFS-UpdateUpdate";
  const mobileUpdateHoldTable = "PW_CHNG_ITEMS_STORE_test";
  const mobileUpdate2Table = "PW_CHNG_ITEM_test";

  //new DB
  const mobileFrameNewDB = "MF-SQL";
  const mobileNewHoldTable = "";
  const mobileNew5DayOrders = "PW_ITEM_INBOUND_COPY";

  const
 */

  console.log(`Running mobileFrameUpdate in ${enviroment}... ${dayjs()}`);


  //STEP 1: EXECUTE SQL TO CLEAR PW_CHNG_ITEM_test
  console.time("Step 1: Deleting previous Store Items.");
  // try {
  //   pjs.data.delete(pjs.getDB(`${mobileFrameUpdateDB}`), `${mobileUpdateHoldTable}`);
  // } catch (error) {
  //   console.error(error);
  // }
  console.timeEnd("Deleting previous Store Items.");
  console.time("Creating new Store Items Array");

  var sqlStatement =
    "Select " +
    "mfstrno, " +
    "mfitem, " +
    "mfmulti, " +
    "mfcrpsrp, " +
    "mfytdpur, " +
    "mfwkpurc, " +
    "mftrlpct, " +
    "mflpurdt, " +
    "mftrlsrp, " +
    "mfbook, " +
    "mfsrpcd, " +
    "mfstatus " +
    `from ${storeItemsTable}`;

  console.log(`Grabbing new store items from ${storeItemsTable}`);
  var c1 = pjs.allocStmt();
  c1.executeDirect(sqlStatement);

  if (sqlstate == "00000") {
    console.log(`Looping through new store item data for database formatted`);
    while (c1.hasMoreRows()) {
      var records = c1.fetch(10000); //# of records to pull at a time.

    for (var i = 0; i < c1.rowCount(); i++) {
      var record = records[i];

      if (typeof record !== "undefined") {
        var lastYearMovement = record.mftrlsrp;
        var yearToDatePurchase = record.mfytdpur;
        var lastYearWeekly = record.mftrlpct;
        var weeklyCycle = record.mfwkpurc;

        if (records.mftrlpct == 0 && records.mfwkpurc == 0) {
          var averageWeeklyMovementCalculation = 0;
        } else {
          averageWeeklyMovementCalculation = (lastYearMovement + yearToDatePurchase) / (lastYearWeekly + weeklyCycle);
        }

        averageWeeklyMovement = averageWeeklyMovementCalculation.toString();
        record.mfavgWklyMvmt = averageWeeklyMovement.slice(0, 10);
        cripFileArray.push({
          storeNumber: record.mfstrno, //STORE_NO
          itemCode: record.mfitem.padStart(6, "0"), //Pad the item code to 6 digits with leading zeros
          // itemCode: record.mfitem, //ITEM_CODE
          multi: record.mfmulti, //MULTI
          cripSRP: record.mfcrpsrp, //SRP
          yearToDatePurchase: record.mfytdpur, //YTD_PURCHASE
          lastYearPurchase: record.mftrlsrp, //LY_PURCHASE
          weeklyPurchase: record.mfwkpurc, //YTD_Weeks_In_INV
          weeksInInventory: record.mftrlpct, // LYWEEK
          book: record.mfbook, //BOOK
          srpCode: record.mfsrpcd, //SRP_CODE
          averageWeeklyMovement: record.mfavgWklyMvmt //AVG_WEEKLY_MVT_UNITS
        });
      }
    }
    }
    c1.close();
  }
  console.timeEnd("Creating new Store Items Array");

  // console.log(cripFileArray.length);
  
  console.time("Writing new Mobile Frame Store Items.");

  var detailQuery =
    `INSERT INTO ${mobileUpdateHoldTable}` +
    "(STORE_NO, " +
    "ITEM_CODE, " +
    "MULTI, " +
    "SRP, " +
    "YTD_PURCHASE, " +
    "LY_PURCHASE, " +
    "YTD_Weeks_In_Inv, " +
    "LYWEEK, " +
    "BOOK, " +
    "SRP_CODE, " +
    "AVG_WEEKLY_MVT_UNITS) " +
    "VALUES (?,?,?,?,?,?,?,?,?,?,?)";

  try {
    console.log(`Writing formatted store item data to ${mobileUpdateHoldTable}`);
    // pjs.data.add(pjs.getDB(`${mobileFrameUpdateDB}`), "MobileUpdateUpdate.${mobileUpdate2Table}", itemArray);
    pjs.query(pjs.getDB(`${mobileFrameUpdateDB}`), detailQuery, cripFileArray, null, null);

    if (sqlstate >= "02000") {
      //sqlError = true;
      sqlMsg = "SQL error during MobileFrame Warehouse Write. SQLstate: " + sqlstate;
      // log.pushToLog(storeNumber, userName, sqlMsg);
      console.log(sqlMsg);
      return;
    }
  } catch (err) {
    //sqlError = true;
    sqlMsg = "Sql Insert catch error during MobileFrame Warehouse Write.";
    // log.pushToLog(storeNumber, userName, sqlMsg);
    console.log(`STOP: ${err}`);
    return;
  }
  console.timeEnd("Writing new Mobile Frame Store Items.");

  // END OF CRIP FILE PROCESSING ----------------------------------------------------------------------------------------------------------------

  //STEP1: EXECUTE SQL TO CLEAR PW_CHNG_ITEM_test
  console.time("Deleting previous Warehouse Items.");
  try {
    pjs.data.delete(pjs.getDB(`${mobileFrameUpdateDB}`), "${mobileUpdate2Table}");
  } catch (error) {
    console.error(error);
  }
  console.timeEnd("Deleting previous Warehouse Items.");

  //STEP2: Large Select Query on INVMST
  console.time("Grabbing new Mobile Frame Warehouse Items.");

  // var invmstQuery =
    "select * from " +
    "(select itmcde as itemcode, " +
    "CONCAT(RIGHT('00000' || ITMCDE, 5), CHKDIG)  AS ITMCDECHK_IN, " +
    "TRIM(REPLACE(ITMDSC, ',' , '')) AS ITMDSC_IN, " +
    "Trim(PKSIZE) AS PKSIZE, " +
    "Case WHEN length(LTRIM(RTRIM(LSHPDT))) < 6 THEN RIGHT('000000' || LSHPDT, 6)END AS LSHPDT_IN, " +
    "Case WHEN STATUS = 'D' THEN 'D' WHEN STATUS = 'L' THEN 'L' WHEN STATUS = 'I' THEN 'I' ELSE 'U' END AS STATUS_IN, " +
    "Case WHEN SELL > 0 THEN SELL WHEN CWSELL > 0 THEN CWSELL END AS SELL_IN, " +
    "TRIM(QTYLMT) AS QTYLMT_IN, " +
    "ORDHDR AS ORDHDR_IN, " +
    "Case WHEN DEPT = 'A' THEN 'Grocery' " +
    "WHEN DEPT = 'B' THEN 'Supplies' " +
    "WHEN DEPT = 'C' THEN 'Continutie' " +
    "WHEN DEPT = 'D' THEN 'Dairy' " +
    "WHEN DEPT = 'E' THEN 'Gro Repack' " +
    "WHEN DEPT = 'F' THEN 'Froz. Food' " +
    "WHEN DEPT = 'G' THEN 'H.B.A.' " +
    "WHEN DEPT = 'H' THEN 'Cigarettes' " +
    "WHEN DEPT = 'I' THEN 'Tobacco' " +
    "WHEN DEPT = 'K' THEN 'Oil' " +
    "WHEN DEPT = 'M' THEN 'Meat' " +
    "WHEN DEPT = 'N' THEN 'Froz. Meat' " +
    "WHEN DEPT = 'P' THEN 'Produce' " +
    "WHEN DEPT = 'S' THEN 'Gro. Deli' " +
    "WHEN DEPT = 'X' THEN 'PerishDeli' " +
    "WHEN DEPT = 'Z' THEN 'Froz. Deli' " +
    "ELSE 'UNKNOWN' END AS DEPT_IN, " +
    "Case WHEN Length(UPC) < 5 THEN RIGHT('00000' || UPC, 5) WHEN Length(UPC) > 5 THEN RIGHT('00000000000' || UPC, 11) END AS UPC_IN, " +
    "SUBKEY AS SUBKEY_IN, " +
    "BSSELL AS BSSELL_IN, " +
    "Trim(BUYER) AS BUYER_IN, " +
    "Trim(HISTYD) AS HISTYD_IN, " +
    "Trim(SALELY) AS SALELY_IN, " +
    "Trim(WKCNT) AS WKCNT_IN, " +
    "Trim(WKAVG) AS WKAVG_IN " +
    `from ${invMasterTable} invMst ` +
    "union " +
    "select sitem as itemcode, " +
    "CONCAT(RIGHT('00000' || ITMCDE, 5), CHKDIG)  AS ITMCDECHK_IN, " +
    "TRIM(REPLACE(ITMDSC, ',' , '')) AS ITMDSC_IN, " +
    "Trim(PKSIZE) AS PKSIZE, " +
    "Case WHEN length(LTRIM(RTRIM(LSHPDT))) < 6 THEN RIGHT('000000' || LSHPDT, 6)END AS LSHPDT_IN, " +
    "Case WHEN STATUS = 'D' THEN 'D' WHEN STATUS = 'L' THEN 'L' WHEN STATUS = 'I' THEN 'I' ELSE 'U' END AS STATUS_IN, " +
    "Case WHEN SELL > 0 THEN SELL WHEN CWSELL > 0 THEN CWSELL END AS SELL_IN, " +
    "TRIM(QTYLMT) AS QTYLMT_IN, " +
    "ORDHDR AS ORDHDR_IN, " +
    "Case WHEN DEPT = 'A' THEN 'Grocery' " +
    "WHEN DEPT = 'B' THEN 'Supplies' " +
    "WHEN DEPT = 'C' THEN 'Continutie' " +
    "WHEN DEPT = 'D' THEN 'Dairy' " +
    "WHEN DEPT = 'E' THEN 'Gro Repack' " +
    "WHEN DEPT = 'F' THEN 'Froz. Food' " +
    "WHEN DEPT = 'G' THEN 'H.B.A.' " +
    "WHEN DEPT = 'H' THEN 'Cigarettes' " +
    "WHEN DEPT = 'I' THEN 'Tobacco' " +
    "WHEN DEPT = 'K' THEN 'Oil' " +
    "WHEN DEPT = 'M' THEN 'Meat' " +
    "WHEN DEPT = 'N' THEN 'Froz. Meat' " +
    "WHEN DEPT = 'P' THEN 'Produce' " +
    "WHEN DEPT = 'S' THEN 'Gro. Deli' " +
    "WHEN DEPT = 'X' THEN 'PerishDeli' " +
    "WHEN DEPT = 'Z' THEN 'Froz. Deli' " +
    "ELSE 'UNKNOWN' END AS DEPT_IN, " +
    "Case WHEN Length(UPC) < 5 THEN RIGHT('00000' || UPC, 5) WHEN Length(UPC) > 5 THEN RIGHT('00000000000' || UPC, 11) END AS UPC_IN, " +
    "SUBKEY AS SUBKEY_IN, " +
    "BSSELL AS BSSELL_IN, " +
    "Trim(BUYER) AS BUYER_IN, " +
    "Trim(HISTYD) AS HISTYD_IN, " +
    "Trim(SALELY) AS SALELY_IN, " +
    "Trim(WKCNT) AS WKCNT_IN, " +
    "Trim(WKAVG) AS WKAVG_IN " +
    `from ${supTable} spTable ` +
    `join ${invMasterTable} invMst on invMst.itmcde = spTable.sitem2 ` +
    ") as fred " +
    `LEFT JOIN ${omitTable} B on fred.itemcode = B.OMITM ` +
    "where B.OMITM IS NULL " +
    "order by fred.itemcode";

  try {
    console.log(`Grabbing new mobile frame data`);
    var getinvmstRecord = pjs.query(invmstQuery);

    if (sqlstate >= "02000") {
      //sqlError = true;
      sqlMsg = "SQL error during read. SQLstate: " + sqlstate;
      // log.pushToLog(storeNumber, userName, sqlMsg);
      console.log(sqlMsg);
      return;
    }
  } catch (err) {
    //sqlError = true;
    sqlMsg = "Sql Insert catch error during INVMST read.";
    // log.pushToLog(storeNumber, userName, sqlMsg);
    console.log(err);
    return;
  }

  if (!getinvmstRecord || getinvmstRecord < 0) {
    //sqlError = true;
    sqlMsg = "Items not found.";
    console.log("no record found");
  }

  console.log(`Looping through new mobile frame data for data transformation`)
  for (let i = 0; i < getinvmstRecord.length; i++) {
    var itemCodeShort = getinvmstRecord[i]["itemcode"];
    if (itemCodeShort.length > 5) {
      console.log(`FOUND: \n ${itemCodeShort}`);
      var itemCodeShort = itemCodeShort.slice(0, 5);
    }

    var itemCodeCheckDigit = getinvmstRecord[i]["itmcdechk_in"];
    if (itemCodeCheckDigit.length > 6) {
      console.log(`FOUND: \n ${itemCodeCheckDigit}`);
      var itemCodeCheckDigit = itemCodeCheckDigit.slice(0, 6);
    }

    var lastShipDate = getinvmstRecord[i]["lshpdt_in"];
    if (lastShipDate == null) lastShipDate = "000000";
    var lastShipDateMonth = lastShipDate.slice(0, 2);
    var lastShipDateDay = lastShipDate.slice(2, 4);
    var lastShipDateYear = lastShipDate.slice(4, 6);
    var lastShiptDateFormat = `${lastShipDateMonth}/${lastShipDateDay}/${lastShipDateYear}`;

    var statusFormat = getinvmstRecord[i]["status_in"];
    if (statusFormat == null) statusFormat = "B";

    warehouseItemArray.push({
      icode: itemCodeShort,
      ITEM_CODE: itemCodeCheckDigit,
      ITEM_DESC: getinvmstRecord[i]["itmdsc_in"],
      PACK_SIZE: getinvmstRecord[i]["pksize"],
      LAST_SHIP_DT: lastShiptDateFormat,
      STATUS: statusFormat,
      CURR_CASE_COST: getinvmstRecord[i]["sell_in"],
      QTY_HARD_LIMIT: getinvmstRecord[i]["qtylmt_in"],
      HDESC: getinvmstRecord[i]["ordhdr_in"],
      DEPARTMENT: getinvmstRecord[i]["dept_in"],
      UPC: getinvmstRecord[i]["upc_in"],
      SUBS_KEY: getinvmstRecord[i]["subkey_in"],
      REG_CASE_COST: getinvmstRecord[i]["bssell_in"],
      BUYER: getinvmstRecord[i]["buyer_in"],
      HISTYD: getinvmstRecord[i]["histyd_in"],
      SALELY: getinvmstRecord[i]["salely_in"],
      WKCNT: getinvmstRecord[i]["wkcnt_in"],
      WKSLY: getinvmstRecord[i]["wkavg_in"]
    });
  }

  console.timeEnd("Grabbing new Mobile Frame Warehouse Items.");
  // console.log(warehouseItemArray);

  //STEP3: Insert new records into MF PW_CHNG_ITEM_test table.
  console.time("Writing new Mobile Frame Warehouse Items.");

  var detailQuery =
    `INSERT INTO ${mobileUpdate2Table} ` +
    "(icode, " +
    "ITEM_CODE, " +
    "ITEM_DESC, " +
    "PACK_SIZE, " +
    "LAST_SHIP_DT, " +
    "STATUS, " +
    "CURR_CASE_COST, " +
    "QTY_HARD_LIMIT, " +
    "HDESC, " +
    "DEPARTMENT, " +
    "UPC, " +
    "SUBS_KEY, " +
    "REG_CASE_COST, " +
    "BUYER, " +
    "HISTYD, " +
    "SALELY, " +
    "WKCNT, " +
    "WKSLY) " +
    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  try {
    console.log(`inserting data into mobile frame holding data`)
    // pjs.data.add(pjs.getDB(`${mobileFrameUpdateDB}`), "MobileUpdateUpdate.${mobileUpdate2Table}", warehouseItemArray);
    pjs.query(pjs.getDB(`${mobileFrameUpdateDB}`), detailQuery, warehouseItemArray, null, null);

    if (sqlstate >= "02000") {
      //sqlError = true;
      sqlMsg = "SQL error during MobileFrame Warehouse Write. SQLstate: " + sqlstate;
      // log.pushToLog(storeNumber, userName, sqlMsg);
      console.log(sqlMsg);
      return;
    }
  } catch (err) {
    //sqlError = true;
    sqlMsg = "Sql Insert catch error during MobileFrame Warehouse Write.";
    // log.pushToLog(storeNumber, userName, sqlMsg);
    console.log(`STOP: ${err}`);
    return;
  }
  console.timeEnd("Writing new Mobile Frame Warehouse Items.");

  //STEP 4: Insert Orders that will be shipped in the next five days
  console.time(`Writing new Mobile Frame Pending Orders`);

  // Original Query
  //  let ordersSelectQuery = 
  // "SELECT RIGHT('000' || OSTORE, 3) AS STORE," +
  // "TRIM(OITEM) AS ICODE," +
  // "ODATE AS SHIPDATE," +
  // "TRIM(OQTY) AS QTY," +
  // "TRIM(ODEPT) AS DEPARTMENT" +
  // "FROM pwafil.BULTONO AS BTO" +
  // "LEFT JOIN pwafil.invmst AS INV" +
  // "ON BTO.OITEM = INV.ITMCDE"
  // "Where ODATE <= (Select Max(ODATE) -2 From PWAFIL.BULTONO)" +
  // "ORDER BY ODATE";

  // let ordersSelectQuery = "SELECT   RIGHT('000' || OSTORE, 3) AS STORE,TRIM(OITEM) AS ICODE,ODATE AS SHIPDATE,TRIM(OQTY) AS QTY,TRIM(ODEPT) AS DEPARTMENT FROM PWAFIL.BULTONO Where ODATE <= (Select Max(ODATE) -2 From PWAFIL.BULTONO) ORDER BY  ODATE"

  //Formatting dates
  let currentDate = dayjs();
  let nextFiveDays = currentDate.add(5, 'day');
  nextFiveDays = parseInt(dayjs(nextFiveDays).format("YYYYMMDD"));
  currentDate = parseInt(dayjs(currentDate).format("YYYYMMDD"));

  console.log(`Current Date: ${currentDate} vs nextFiveDays: ${nextFiveDays}`)

  //Updated Query
  let ordersSelectQuery = 
  `SELECT   RIGHT('000' || OSTORE, 3) AS STORE, 
  TRIM(OITEM) AS ICODE, 
  ODATE AS SHIPDATE ,
  TRIM(OQTY) AS QTY,
  TRIM(ODEPT) AS DEPARTMENT,
  TRIM(INV.CHKDIG) AS CHECKDIGIT
  FROM PWAFIL.BULTONO AS BTO
  LEFT JOIN ${invMasterTable} AS INV
  ON BTO.oitem = INV.itmcde
  Where ODATE between ${currentDate} AND ${nextFiveDays}
  AND ODATE <= (Select Max(ODATE) -2 From PWAFIL.BULTONO)
  ORDER BY  ODATE 
  `;

  try {
    console.log(`Grabbing order data.`);
    var orderData = pjs.query(ordersSelectQuery);

    if (sqlstate >= "02000") {
      //sqlError = true;
      sqlMsg = "SQL error during read. SQLstate: " + sqlstate;
      // log.pushToLog(storeNumber, userName, sqlMsg);
      console.log(sqlMsg);
      return;
    }
  } catch (err) {
    //sqlError = true;
    sqlMsg = "Sql select catch error while grabbing order data.";
    // log.pushToLog(storeNumber, userName, sqlMsg);
    console.log(err);
    return;
  }

  console.log(`Records from select query: ${orderData.length}`);
  let array1 = []; 

  console.log(`Looping through orderData to format data`);
  for (let i = 0; i < orderData.length; i++) {
    // console.log(`Formatting the following bultono record for inserting: ${JSON.stringify(orderData[0])}`);

    // Concatenate item code and check digit
    let itemNumber = orderData[i]["icode"] + (orderData[i]["checkdigit"] ? orderData[i]["checkdigit"] : "");

    // Get the day of the week as a number from the DayJS object
    let dbDateFormat = dayjs(String(orderData[i]["shipdate"]), 'YYYYMMDD').format('YYYY-MM-DD');
    let shipDayOfWeek = dayjs(dbDateFormat).day();
    let day;

    switch (shipDayOfWeek) {
        case 0:
            day = "Sunday";
            break;
        case 1:
            day = "Monday";
            break;
        case 2:
            day = "Tuesday";
            break;
        case 3:
            day = "Wednesday";
            break;
        case 4:
            day = "Thursday";
            break;
        case 5:
            day = "Friday";
            break;
        case 6:
            day = "Saturday";
            break;
    }

    array1.push({
      STORE: orderData[i]["store"],
      ITEM: itemNumber,
      QUANTITY: orderData[i]["qty"],
      SHIP_DAY: day,
      SHIP_DATE : dbDateFormat,
    });

    // console.log(`Database Date : ${orderData[i]["shipdate"]} vs Formated Date: ${dbDateFormat} vs Day of Week: ${day}`);

  }  

  console.log(`Looping through formatted orderData to insert into mobile frame`);
  for (let i = 0; i < array1.length; i++) {

    let iQuery = `INSERT INTO PW_ITEM_INBOUND_COPY (STORE, ITEM, QUANTITY, SHIP_DAY, SHIP_DATE) VALUES (@STORE,@ITEM,@QUANTITY,@SHIP_DAY, @SHIP_DATE)`; //Test
    // let iQuery = `INSERT INTO PW_ITEM_INBOUND (STORE, ITEM, QUANTITY, SHIP_DAY, SHIP_DATE) VALUES (@STORE,@ITEM,@QUANTITY,@SHIP_DAY, @SHIP_DATE)`; //Prod        
    // console.log(`Inserting into PW_ITEM_INBOUND table with the following record: ${JSON.stringify(array1[i])}`);

    // try{
    //   var insertResult = pjs.query(
    //     pjs.getDB("MF-SQL"),
    //     iQuery,
    //     array1[i],
    //     null,
    //     null) 

    //     if (sqlstate >= "02000") {
    //       //sqlError = true;
    //       sqlMsg = "SQL error during insert for PW_ITEM_INBOUND_COPY table. SQLstate: " + sqlstate;
    //       // log.pushToLog(storeNumber, userName, sqlMsg);
    //       console.log(sqlMsg);
    //       return;
    //     }

    // } catch (err) {
    //   console.log(`Error: ${err}`);
    // }
  }

  console.timeEnd(`Writing new Mobile Frame Pending Orders`);

  console.timeEnd("Updating Mobile Frame Warehouse Items.");
}
exports.run = mobileFrameUpdate;

