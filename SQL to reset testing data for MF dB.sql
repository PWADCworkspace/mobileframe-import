select
  oh.GUID,
  oh.id as HeaderID,
  oh.FLAGGED,
  oh.LINE_COUNT,
  count(od.id) as DetailCount
from
  HeaderCopy as oh
  inner join DetailCopy as od on od.ORDER_HEADER_GUID = oh.GUID
where
  oh.FLAGGED = ''
  and oh.deleted_by < 0
  and oh.VENDOR_NO not in (4, 7, 8, 10)
group by
  oh.id,
  oh.GUID,
  oh.LINE_COUNT,
  od.deleted_by,
  oh.FLAGGED;


select
  od.id as DetailId,
  oh.id as HeaderId,
  od.LINE_ITEM_NO as lineItmNum,
  oh.STORE_NO as Store,
  oh.ORDER_DT as OrderDate,
  oh.ORDER_TYPE as orderType,
  oh.VENDOR_NO as vendorNumber,
  od.QUANTITY as quantity,
  od.ORDER_HEADER_GUID as Guid
from
  HeaderCopy as oh
  inner join DetailCopy as od on od.ORDER_HEADER_GUID = oh.GUID
where
  od.FLAGGED = 'b'
  and od.deleted_by < 0
  and oh.GUID = '78e863a7-280a-4082-8405-ee1dae096a07'
select
  *
from
  PW_ORDER_DETAIL
where
  deleted_by < 0
  and FLAGGED = ''
  and ORDER_HEADER_GUID = '6ff57bc8-ce4a-49a9-975b-96ebd86e02d8'
update DetailCopy
set
  FLAGGED = ''
where
  deleted_by < 0
  and FLAGGED = 'b'
  and ORDER_HEADER_GUID = '78e863a7-280a-4082-8405-ee1dae096a07'
update HeaderCopy
set
  FLAGGED = ''
where
  deleted_by < 0
  and FLAGGED = 'b'
  and GUID = '78e863a7-280a-4082-8405-ee1dae096a07'