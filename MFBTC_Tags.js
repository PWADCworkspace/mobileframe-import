const dayjs = require("dayjs");
const fs = require('fs');
const path = require('path');
const readline = require('readline');
const { parse } = require('csv-parse');
const Client = require('ftp');
const auto = pjs.require("pjslogging/autoLog.js");
const csv = require("csv-writer").createObjectCsvWriter;
const pathToHotFolder =  '\\\\cfdata\\BartenderHOT\\BTC_PriceChangeTags';


/*
  Created By: Samuel Gray
  Date Create: 08/12/2024
  Modified By: Samuel Gray
  Date Modified: 10/28/2024

  Purpose: Retrieve item and store item files from BTC FTP site and insert data into Mobile Frame
*/

async function MF_BTC_Tags() {
    
  const programName = "MF_BTC_Tags";
  const currentPort = profound.settings.port;

  const oldServer = "Server2012-MFS";
  const newServer = "MF-SQL";

  let btcPriceBookTable = '[MobileFrame].[dbo].[PW_BTC_ITEM_TEST]';
  let btcItemsTable = '[CrossDock].[dbo].[BTC_Items_Test]';

  btcPriceBookTable = "[MobileFrame].[dbo].[PW_BTC_ITEM]";
  btcItemsTable = "[CrossDock].[dbo].[BTC_Items]";  
  
  if(currentPort == 8083){
    btcPriceBookTable = "[MobileFrame].[dbo].[PW_BTC_ITEM]";
    btcItemsTable = "[CrossDock].[dbo].[BTC_Items]";  
  }

  log(`${programName} is running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, 5);

  // await generateItemUpdateTags(oldServer);
  await generateStoreSetupTags(oldServer);

  log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, 5);

  async function generateItemUpdateTags(serverName){
    //Declare sql query
    const retrieveUpdatedStoreItemsQuery =  `
        SELECT
            si.STORE_NUMBER AS StoreNumber, 
            si.BTC_ITEM_NO AS BTCItemNum, 
            si.UNIT_SRP AS UnitSRP, 
            si.UNIT_SRP AS CaseSRP,
            i.Status,
            i.Description,
            i.Pack,
            i.UnitUPC
        FROM ${btcPriceBookTable} as si
        LEFT JOIN ${btcItemsTable} as i ON si.BTC_ITEM_NO = i.BTCItemNumber
        WHERE 
            i.Status != '' AND
            (
              --STORE_NUMBER in (388, 448, 287, 285, 290, 328, 138, 348, 130, 134)
              --STORE_NUMBER in (388, 448, 287, 285, 328, 138, 348, 130, 134)
               STORE_NUMBER in (55,65,75,114,147,148,384)
            )
        ORDER BY si.STORE_NUMBER , si.BTC_ITEM_NO
    `;

    console.log(retrieveUpdatedStoreItemsQuery);

    //Check if the directory is accessable
    fs.access(pathToHotFolder, fs.constants.F_OK, (err) => {
      if(err){
        log(`${pathToHotFolder} is not accessible: ${err}`, 20);
        return;
      }
    });

    log(`${pathToHotFolder} is accessible`, 5);
    log(`Fetching all store items that are flagged with a status.`, 5);

    let itemsRequireTags = [];

    try{
      itemsRequireTags = await pjs.query(pjs.getDB(serverName),retrieveUpdatedStoreItemsQuery, null,null);
    } catch(err){
      log(`Catch error occurred while fetching all store items that are flagged with a status. Error: ${err}`, 5);
      return;
    }

    log(`Number of BTC items returned: ${itemsRequireTags.length}`, 5);

    console.log(itemsRequireTags[0]);

    //Declare CSV file column headers
    const headers = [
      {id: 'StoreNumber', title: 'StoreNumber' },
      {id: 'BTCItemNum', title: 'BTCItemNum' },
      {id: 'UnitSRP', title: 'UnitSRP' },
      {id: 'CaseSRP', title: 'CaseSRP' },
      {id: 'ChangeStatus', title: 'ChangeStatus' },
      {id: 'Description', title: 'Description' },
      {id: 'PackSize', title: 'PackSize' },
      {id: 'UPC', title: 'UPC' },
    ]

    const groupByStore = {};

    //Iterate through each store item record
    itemsRequireTags.forEach((record) => {
      //Set the store from current record
      const storeNum = record.StoreNumber;
      //Check if the groupByStore object already has an array for this store
      if(!groupByStore[storeNum]){
        //If not, initilze an empty array for this store
        groupByStore[storeNum] =[];
      }

      //Add the current record to the array for the this store
      groupByStore[storeNum].push({
        StoreNumber : String(record.StoreNumber).trim(),
        BTCItemNum : String(record.BTCItemNum).trim(),
        UnitSRP : String(parseFloat(record.UnitSRP).toFixed(2)),
        CaseSRP : String(parseFloat(record.CaseSRP).toFixed(2)), 
        ChangeStatus : String(record.Status.trim()),
        Description : String(record.Description.trim()),
        PackSize: String(record.Pack.trim()),
        UPC: String(record.UnitUPC.trim())
      });
    });

    //Iterate thrugh each entry in the groupByStore object
    Object.entries(groupByStore).forEach(([storeNum, storeRecords]) => {
      //Define path with store file name
      const localFilePath = path.join(pathToHotFolder, `Store ${storeNum}.csv`);
      //Create CSV writer instance with the specified path and column headers
      const csvWriter = csv({
        path : localFilePath,
        header: headers,

      });
      csvWriter
        .writeRecords(storeRecords) //Write the records for the current store to CSV file
        .then(() => console.log(`CSV created for store ${storeNum}`)) //Log sucess message once CSV file is create
        .catch((err) => console.error(`Catch error occurred while creating CSV file for store ${storeNum}. Error ${err}`)); //Log an error if the CSV did not get created
    });
  } 
  async function generateStoreSetupTags(serverName){
    //Declare sql query
    const retrieveUpdatedStoreItemsQuery =  `
        SELECT
            si.STORE_NUMBER AS StoreNumber, 
            si.BTC_ITEM_NO AS BTCItemNum, 
            si.UNIT_SRP AS UnitSRP, 
            si.UNIT_SRP AS CaseSRP,
            i.Status,
            i.Description,
            i.Pack,
            i.UnitUPC
        FROM ${btcPriceBookTable} as si
        LEFT JOIN ${btcItemsTable} as i ON si.BTC_ITEM_NO = i.BTCItemNumber
        WHERE 
            --i.Status != '' AND
            (
              --STORE_NUMBER in (388, 448, 287, 285, 290, 328, 138, 348, 130, 134)
              --STORE_NUMBER in (388, 448, 287, 285, 328, 138, 348, 130, 134)
              --STORE_NUMBER in (55,65,75,114,147,148,384)
              STORE_NUMBER in (103)

            )
        ORDER BY si.STORE_NUMBER,si.BTC_ITEM_NO
    `;

    console.log(retrieveUpdatedStoreItemsQuery);

    //Check if the directory is accessable
    fs.access(pathToHotFolder, fs.constants.F_OK, (err) => {
      if(err){
        log(`${pathToHotFolder} is not accessible: ${err}`, 20);
        return;
      }
    });

    log(`${pathToHotFolder} is accessible`, 5);
    log(`Fetching all store items that are flagged with a status.`, 5);

    let itemsRequireTags = [];

    try{
      itemsRequireTags = await pjs.query(pjs.getDB(serverName),retrieveUpdatedStoreItemsQuery, null,null);
    } catch(err){
      log(`Catch error occurred while fetching all store items that are flagged with a status. Error: ${err}`, 5);
      return;
    }

    log(`Number of BTC items returned: ${itemsRequireTags.length}`, 5);

    console.log(itemsRequireTags[0]);

    //Declare CSV file column headers
    const headers = [
      {id: 'StoreNumber', title: 'StoreNumber' },
      {id: 'BTCItemNum', title: 'BTCItemNum' },
      {id: 'UnitSRP', title: 'UnitSRP' },
      {id: 'CaseSRP', title: 'CaseSRP' },
      {id: 'ChangeStatus', title: 'ChangeStatus' },
      {id: 'Description', title: 'Description' },
      {id: 'PackSize', title: 'PackSize' },
      {id: 'UPC', title: 'UPC' },
    ]

    const groupByStore = {};

    //Iterate through each store item record
    itemsRequireTags.forEach((record) => {
      //Set the store from current record
      const storeNum = record.StoreNumber;
      //Check if the groupByStore object already has an array for this store
      if(!groupByStore[storeNum]){
        //If not, initilze an empty array for this store
        groupByStore[storeNum] =[];
      }

      //Add the current record to the array for the this store
      groupByStore[storeNum].push({
        StoreNumber : String(record.StoreNumber).trim(),
        BTCItemNum : String(record.BTCItemNum).trim(),
        UnitSRP : String(parseFloat(record.UnitSRP).toFixed(2)),
        CaseSRP : String(parseFloat(record.CaseSRP).toFixed(2)), 
        ChangeStatus : String(record.Status.trim()),
        Description : String(record.Description.trim()),
        PackSize: String(record.Pack.trim()),
        UPC: String(record.UnitUPC.trim())
      });
    });

    //Iterate thrugh each entry in the groupByStore object
    Object.entries(groupByStore).forEach(([storeNum, storeRecords]) => {
      //Define path with store file name
      const localFilePath = path.join(pathToHotFolder, `Store ${storeNum}.csv`);
      //Create CSV writer instance with the specified path and column headers
      const csvWriter = csv({
        path : localFilePath,
        header: headers,

      });
      csvWriter
        .writeRecords(storeRecords) //Write the records for the current store to CSV file
        .then(() => console.log(`CSV created for store ${storeNum}`)) //Log sucess message once CSV file is create
        .catch((err) => console.error(`Catch error occurred while creating CSV file for store ${storeNum}. Error ${err}`)); //Log an error if the CSV did not get created
    });
  } 
  async function log(messageIn,code){
        pjs.define("message", {type: 'char', length: 246});
        message = messageIn;
        console.log(message);
        auto.autoLog(message, programName, code);

        if(code == 20){
            sendEmail(`${programName}: Errors During Processing`,message);
        }

        return;
  }
  function sendEmail(subject,message){
        const emailSend =`SNDSMTPEMM RCP((sgray@pwadc.com)(gweldon@pwadc.com)) SUBJECT('${subject}') ` +
        `NOTE('${message}')`;
    
        try {
            pjs.runCommand(emailSend);
        }
        catch(err) {
            console.error(`Error sending email. Error: ${err}`);
            auto.autoLog(`Error sending email. Error: ${err}`, programName, 20);
        }
  }
}

exports.run=MF_BTC_Tags;
