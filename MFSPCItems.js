const fs = pjs.wrap(require('fs'));
const fspath = require('path');
const { send } = require('process');
const dayjs = require('dayjs');
const auto = pjs.require("pjslogging/autoLog.js");

async function MF_Special_Items() {

    const currentPath = __dirname + fspath.sep; // C:\tyler\modules\
    const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\mobileframe-import\

    const programName = `MF_Buyers`;

    const currentPort = profound.settings.port;
    auto.autoLog("currentPort" + currentPort, programName, 5);

    //Setting tables
    if(currentPort === 8081){
        buyerTable = 'pwafil.rpmsbuyl';
    } else {
        buyerTable = 'testrpms.rpmsbuyl';
    }

    auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs()}`, programName, 5); 
  
    let subject = `${programName}: Error Detected During Processing`;
    let message = "";


}

exports.run = MF_Special_Items;