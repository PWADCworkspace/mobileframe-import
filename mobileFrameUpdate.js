//Dottis CL PGM to update the mfcripuf table before running this pgm: CLMFCRIPU

/*

Author: Samuel Gray
Date: 10/18/2024
Purpose: Update Mobile Frame with Latest Item, Store Item, Pending Orders, and Special Items information
*/

async function mobileFrameUpdate() {

  //Importing modules
  const dayjs = require('dayjs');
  const auto = pjs.require("pjslogging/autoLog.js");

  const programName = `MF_Update`;
  const currentPort = profound.settings.port;
  let message = "";

  auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
  console.log(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);

  //Setting mobile frame servers/databases
  const newMFServerTemp = "MF-SQL-Update"; // New temp database
  const newMFServer = "MF-SQL"; // New real database

  const oldMFServerTemp = "Server2012-MFS-UpdateUpdate"; // Old temp database
  const oldMFServer = "Server2012-MFS"; // Old real database

  //Setting MF DEV tables
  let storeItemsTableTemp = "PW_CHNG_ITEMS_STORE_COPY"; // Temp Store Items Table on TEMP database
  let itemsTableTemp = "PW_CHNG_ITEM_COPY"; // Temp Items Table on TEMP database
  let pendingOrdersTable = "PW_ITEM_INBOUND_COPY"; //Pending Orders Table on REAL database
  let specialItemsTable = "PW_ALLOW_ITEM_COPY"; //Special Items Table on REAL database

  //Setting MF stored procedures
  const itemsStoredProcedure = "dbo.sp_Upd_MF_PW_Items";
  const storeItemsStoredProcedure = "sp_Upd_MF_PW_Store_Items";

  const cripFile = 'MFCRIPUF';

  if(currentPort == 8083){
    storeItemsTableTemp = "PW_CHNG_ITEMS_STORE";
    itemsTableTemp = "PW_CHNG_ITEM";
    pendingOrdersTable = "PW_ITEM_INBOUND";
    specialItemsTable = "PW_ALLOW_ITEM";
  }

  // //START OF STORE ITEMS LOGIC----------------------------------------------------------------------------------------------------------------------
  ///*
  // Clear store items table in temp databases
  await clearStoreItemRecords(newMFServerTemp); // New Server
  await clearStoreItemRecords(oldMFServerTemp); // Old Server

  // Gather new store items from the iSeries 
  let cripFileArray = [];
  auto.autoLog(`Gathering new store item data from MFCRIPUF table.`, programName, 5);
  console.log(`Gathering new store item data from MFCRIPUF table.`);
  
  let getStoreItemsQuery = `
    SELECT mfstrno, mfitem, mfmulti, mfcrpsrp, mfytdpur, mfwkpurc, mftrlpct, mflpurdt, mftrlsrp, mfbook, mfsrpcd, mfstatus 
    FROM MFCRIPUF
  `;

  var c1 = pjs.allocStmt();
  c1.executeDirect(getStoreItemsQuery);

  if (sqlstate == "00000") {
    while (c1.hasMoreRows()) {
      var records = c1.fetch(10000); //# of records to pull at a time.

    for (var i = 0; i < c1.rowCount(); i++) {
      var record = records[i];

      if (typeof record !== "undefined") {
        var lastYearMovement = record.mftrlsrp;
        var yearToDatePurchase = record.mfytdpur;
        var lastYearWeekly = record.mftrlpct;
        var weeklyCycle = record.mfwkpurc;

        if (records.mftrlpct == 0 && records.mfwkpurc == 0) {
          var averageWeeklyMovementCalculation = 0;
        } else {
          averageWeeklyMovementCalculation = (lastYearMovement + yearToDatePurchase) / (lastYearWeekly + weeklyCycle);
        }

        averageWeeklyMovement = averageWeeklyMovementCalculation.toString();
        record.mfavgWklyMvmt = averageWeeklyMovement.slice(0, 10);
        cripFileArray.push({
          storeNumber: record.mfstrno, //STORE_NO
          itemCode: record.mfitem.padStart(6, "0"), //Pad the item code to 6 digits with leading zeros
          // itemCode: record.mfitem, //ITEM_CODE
          multi: record.mfmulti, //MULTI
          cripSRP: record.mfcrpsrp, //SRP
          yearToDatePurchase: record.mfytdpur, //YTD_PURCHASE
          lastYearPurchase: record.mftrlsrp, //LY_PURCHASE
          weeklyPurchase: record.mfwkpurc, //YTD_Weeks_In_INV
          weeksInInventory: record.mftrlpct, // LYWEEK
          book: record.mfbook, //BOOK
          srpCode: record.mfsrpcd, //SRP_CODE
          averageWeeklyMovement: record.mfavgWklyMvmt //AVG_WEEKLY_MVT_UNITS
        });
      }
    }
    }
    c1.close();
  }
  
  auto.autoLog(`Store item records pulled from MFCRIPUF: ${cripFileArray.length}`, programName, 5);    
  console.log(`Store item records pulled from MFCRIPUF: ${cripFileArray.length}`);

  if(cripFileArray.length == 0){
    message = `No store item records found in MFCRIPUF :${cripFileArray.length}.`
    console.log(message);
    auto.autoLog(message, programName, 20);

    console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);

    return;
  }

  //Write new store items to mobile frame servers
  await writeStoreItemRecords(cripFileArray, newMFServerTemp,newMFServer);
  await writeStoreItemRecords(cripFileArray, oldMFServerTemp, oldMFServer);

  //*/

  //END OF STORE ITEMS LOGIC-------------------------------------------------------------------------------------------------------------------------
  
  //START OF ITEMS LOGIC----------------------------------------------------------------------------------------------------------------------------

  // /*
  // Clear items table in temp databases
  await clearItemRecords(newMFServerTemp);
  await clearItemRecords(oldMFServerTemp);

  // Gather new items from the iSeries 
  auto.autoLog(`Gathering new item data from iSeries tables.`, programName, 5);
  console.log(`Gathering new item data from iSeries tables.`);

  var invmstQuery =
    `SELECT * 
      FROM (
        SELECT 
            itmcde AS itemcode, 
            0 AS invItem,
            0 AS tempItemCheck,  -- Add NULL or a default value if sckdig is not in the first table 
            '' as invDept,
            CONCAT(RIGHT('00000' || ITMCDE, 5), CHKDIG) AS ITMCDECHK_IN,  
            TRIM(REPLACE(ITMDSC, ',', '')) AS ITMDSC_IN,  
            TRIM(PKSIZE) AS PKSIZE,  
            CASE 
                WHEN LENGTH(LTRIM(RTRIM(LSHPDT))) < 6 THEN RIGHT('000000' || LSHPDT, 6) 
                ELSE NULL 
            END AS LSHPDT_IN,  
            CASE 
                WHEN STATUS = 'D' THEN 'D' 
                WHEN STATUS = 'L' THEN 'L' 
                WHEN STATUS = 'I' THEN 'I' 
                ELSE 'U' 
            END AS STATUS_IN,  
            CASE 
                WHEN SELL > 0 THEN SELL 
                WHEN CWSELL > 0 THEN CWSELL 
                ELSE NULL 
            END AS SELL_IN,  
            TRIM(QTYLMT) AS QTYLMT_IN,  
            ORDHDR AS ORDHDR_IN,  
            CASE 
                WHEN DEPT = 'A' THEN 'Grocery'  
                WHEN DEPT = 'B' THEN 'Supplies'  
                WHEN DEPT = 'C' THEN 'Continutie'  
                WHEN DEPT = 'D' THEN 'Dairy'  
                WHEN DEPT = 'E' THEN 'Gro Repack'  
                WHEN DEPT = 'F' THEN 'Froz. Food'  
                WHEN DEPT = 'G' THEN 'H.B.A.'  
                WHEN DEPT = 'H' THEN 'Cigarettes'  
                WHEN DEPT = 'I' THEN 'Tobacco'  
                WHEN DEPT = 'K' THEN 'Oil'  
                WHEN DEPT = 'M' THEN 'Meat'  
                WHEN DEPT = 'N' THEN 'Froz. Meat'  
                WHEN DEPT = 'P' THEN 'Produce'  
                WHEN DEPT = 'S' THEN 'Gro. Deli'  
                WHEN DEPT = 'X' THEN 'PerishDeli'  
                WHEN DEPT = 'Z' THEN 'Froz. Deli'  
                ELSE 'UNKNOWN' 
            END AS DEPT_IN,  
            CASE 
                WHEN LENGTH(UPC) < 5 THEN RIGHT('00000' || UPC, 5) 
                WHEN LENGTH(UPC) > 5 THEN RIGHT('00000000000' || UPC, 11) 
                ELSE NULL 
            END AS UPC_IN,  
            SUBKEY AS SUBKEY_IN,  
            BSSELL AS BSSELL_IN,  
            TRIM(BUYER) AS BUYER_IN,  
            TRIM(HISTYD) AS HISTYD_IN,  
            TRIM(SALELY) AS SALELY_IN,  
            TRIM(WKCNT) AS WKCNT_IN,  
            TRIM(WKAVG) AS WKAVG_IN  
        FROM INVMST
    
        UNION  
    
        SELECT 
            sitem AS itemcode, 
            su.sitem2 AS invItem,
            su.sckdig as tempItemCheck, 
            su.sbcode as invDept,
            CONCAT(RIGHT('00000' || ITMCDE, 5), CHKDIG) AS ITMCDECHK_IN,  
            TRIM(REPLACE(ITMDSC, ',', '')) AS ITMDSC_IN,  
            TRIM(PKSIZE) AS PKSIZE,  
            CASE 
                WHEN LENGTH(LTRIM(RTRIM(LSHPDT))) < 6 THEN RIGHT('000000' || LSHPDT, 6) 
                ELSE NULL 
            END AS LSHPDT_IN,  
            CASE 
                WHEN STATUS = 'D' THEN 'D' 
                WHEN STATUS = 'L' THEN 'L' 
                WHEN STATUS = 'I' THEN 'I' 
                ELSE 'U' 
            END AS STATUS_IN,  
            CASE 
                WHEN SELL > 0 THEN SELL 
                WHEN CWSELL > 0 THEN CWSELL 
                ELSE NULL 
            END AS SELL_IN,  
            TRIM(QTYLMT) AS QTYLMT_IN,  
            ORDHDR AS ORDHDR_IN,  
            CASE 
            WHEN DEPT = 'A' and su.sitem2 = 0 THEN 'Grocery'  
            WHEN DEPT = 'B' and su.sitem2 = 0 THEN 'Supplies'  
            WHEN DEPT = 'C' and su.sitem2 = 0 THEN 'Continutie'  
            WHEN DEPT = 'D' and su.sitem2 = 0 THEN 'Dairy'  
            WHEN DEPT = 'E' and su.sitem2 = 0 THEN 'Gro Repack'  
            WHEN DEPT = 'F' and su.sitem2 = 0 THEN 'Froz. Food'  
            WHEN DEPT = 'G' and su.sitem2 = 0 THEN 'H.B.A.'  
            WHEN DEPT = 'H' and su.sitem2 = 0 THEN 'Cigarettes'  
            WHEN DEPT = 'I' and su.sitem2 = 0 THEN 'Tobacco'  
            WHEN DEPT = 'K' and su.sitem2 = 0 THEN 'Oil'  
            WHEN DEPT = 'M' and su.sitem2 = 0 THEN 'Meat'  
            WHEN DEPT = 'N' and su.sitem2 = 0 THEN 'Froz. Meat'  
            WHEN DEPT = 'P' and su.sitem2 = 0 THEN 'Produce'  
            WHEN DEPT = 'S' and su.sitem2 = 0 THEN 'Gro. Deli'  
            WHEN DEPT = 'X' and su.sitem2 = 0 THEN 'PerishDeli'  
            WHEN DEPT = 'Z' and su.sitem2 = 0 THEN 'Froz. Deli' 
            
            WHEN su.sbcode = 'A' and su.sitem2 != 0 THEN 'Grocery'
            WHEN su.sbcode = 'B' and su.sitem2 != 0 THEN 'Supplies'
            WHEN su.sbcode = 'C' and su.sitem2 != 0 THEN 'Continutie'                
            WHEN su.sbcode = 'D' and su.sitem2 != 0 THEN 'Dairy'                
            WHEN su.sbcode = 'E' and su.sitem2 != 0 THEN 'Gro Repack'                
            WHEN su.sbcode = 'F' and su.sitem2 != 0 THEN 'Froz. Food'               
            WHEN su.sbcode = 'G' and su.sitem2 != 0 THEN 'H.B.A.'                                                
            WHEN su.sbcode = 'H' and su.sitem2 != 0 THEN 'Cigarettes'                                                
            WHEN su.sbcode = 'I' and su.sitem2 != 0 THEN 'Tobacco'                                                
            WHEN su.sbcode = 'K' and su.sitem2 != 0 THEN 'Oil'                                                
            WHEN su.sbcode = 'M' and su.sitem2 != 0 THEN 'Meat'         
            WHEN su.sbcode = 'N' and su.sitem2 != 0 THEN 'Froz. Meat'                                                                                 
            WHEN su.sbcode = 'P' and su.sitem2 != 0 THEN 'Produce'                                                                                 
            WHEN su.sbcode = 'S' and su.sitem2 != 0 THEN 'Gro. Deli'                                                                                 
            WHEN su.sbcode = 'X' and su.sitem2 != 0 THEN 'PerishDeli'   
            WHEN su.sbcode = 'Z' and su.sitem2 != 0 THEN 'Froz. Deli'         
            ELSE 'UNKNOWN' 
            END AS DEPT_IN,  
            CASE 
                WHEN LENGTH(UPC) < 5 THEN RIGHT('00000' || UPC, 5) 
                WHEN LENGTH(UPC) > 5 THEN RIGHT('00000000000' || UPC, 11) 
                ELSE NULL 
            END AS UPC_IN,  
            SUBKEY AS SUBKEY_IN,  
            BSSELL AS BSSELL_IN,  
            TRIM(BUYER) AS BUYER_IN,  
            TRIM(HISTYD) AS HISTYD_IN,  
            TRIM(SALELY) AS SALELY_IN,  
            TRIM(WKCNT) AS WKCNT_IN,  
            TRIM(WKAVG) AS WKAVG_IN  
        FROM SUPFILE AS su
        JOIN INVMST ON INVMST.itmcde = su.sitem2
    ) AS fred  
    LEFT JOIN OMITMSI AS B ON fred.itemcode = B.OMITM  
    ORDER BY fred.itemcode;
  `;

  var warehouseItemArray = [];

  try {
    var getinvmstRecord = pjs.query(invmstQuery);

    if (sqlstate >= "02000") {
      message = `SQL state error while fetching new item records. SQLstate: ${sqlstate}`;
      console.log(message);
      auto.autoLog(message, programName, 20);
      return;
    }
  } catch (err) {
    message = `SQL catch error while fetching new item records. Error: ${err}`;
    console.log(message);
    auto.autoLog(message, programName, 20);
    return;
  }

  auto.autoLog(`Item records pulled from iSeries: ${getinvmstRecord.length}`, programName, 5);    
  console.log(`Item records pulled from iSeries: ${getinvmstRecord.length}`);

  if (!getinvmstRecord || getinvmstRecord < 0) {
    message = `No new item records found in iSeries :${cripFileArray.length}.`
    console.log(message);
    auto.autoLog(message, programName, 20);

    console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);

    return;
  }

  for (let i = 0; i < getinvmstRecord.length; i++) {

    var itemCodeShort = getinvmstRecord[i]["itemcode"];
    if (itemCodeShort.length > 5) {
      // console.log(`FOUND: \n ${itemCodeShort}`);
      auto.autoLog(`FOUND: \n ${itemCodeShort}`, programName, logSeverity );
      var itemCodeShort = itemCodeShort.slice(0, 5);
    }

    var itemCodeCheckDigit = getinvmstRecord[i]["itmcdechk_in"];
    if (itemCodeCheckDigit.length > 6) {
      // console.log(`FOUND: \n ${itemCodeCheckDigit}`);
      auto.autoLog(`FOUND: \n ${itemCodeCheckDigit}`, programName, 10 );
      var itemCodeCheckDigit = itemCodeCheckDigit.slice(0, 6);
    }

    var crossReferenceCode = getinvmstRecord[i]["invitem"];
    if(crossReferenceCode !== 0){
      var itemCodeCheckDigit = String(getinvmstRecord[i]["itemcode"]) + String(getinvmstRecord[i]["tempitemcheck"]);
    }

    var lastShipDate = getinvmstRecord[i]["lshpdt_in"];
    if (lastShipDate == null) lastShipDate = "000000";
    var lastShipDateMonth = lastShipDate.slice(0, 2);
    var lastShipDateDay = lastShipDate.slice(2, 4);
    var lastShipDateYear = lastShipDate.slice(4, 6);
    var lastShiptDateFormat = `${lastShipDateMonth}/${lastShipDateDay}/${lastShipDateYear}`;

    var statusFormat = getinvmstRecord[i]["status_in"];
    if (statusFormat == null) statusFormat = "B";

    warehouseItemArray.push({
      icode: itemCodeShort,
      ITEM_CODE: itemCodeCheckDigit,
      ITEM_DESC: getinvmstRecord[i]["itmdsc_in"],
      PACK_SIZE: getinvmstRecord[i]["pksize"],
      LAST_SHIP_DT: lastShiptDateFormat,
      STATUS: statusFormat,
      CURR_CASE_COST: getinvmstRecord[i]["sell_in"],
      QTY_HARD_LIMIT: getinvmstRecord[i]["qtylmt_in"],
      HDESC: getinvmstRecord[i]["ordhdr_in"],
      DEPARTMENT: getinvmstRecord[i]["dept_in"],
      UPC: getinvmstRecord[i]["upc_in"],
      SUBS_KEY: getinvmstRecord[i]["subkey_in"],
      REG_CASE_COST: getinvmstRecord[i]["bssell_in"],
      BUYER: getinvmstRecord[i]["buyer_in"],
      HISTYD: getinvmstRecord[i]["histyd_in"],
      SALELY: getinvmstRecord[i]["salely_in"],
      WKCNT: getinvmstRecord[i]["wkcnt_in"],
      WKSLY: getinvmstRecord[i]["wkavg_in"]
    });
  }

  // await writeItemRecords(warehouseItemArray, newMFServerTemp, newMFServer);
  await writeItemRecords(warehouseItemArray, oldMFServerTemp, oldMFServer);

  // */

  //END OF ITEMS LOGIC------------------------------------------------------------------------------------------------------------------------------

  // START OF SPECIAL ITEMS LOGIC------------------------------------------------------------------------------------------------------------------
  
  // /*
  await clearSpecialItems(newMFServer);
  await clearSpecialItems(oldMFServer);

  console.log(`Grabbing special items from OMITALW table`);
  auto.autoLog(`Grabbing special items from OMITALW table`, programName, 5);

  specialItmsQuery = `
    SELECT concat(inv.itmcde, inv.chkdig) AS item, oitm.oastor AS store  
    FROM OMITALW AS oitm 
    LEFT JOIN pwafil.invmst AS inv ON oitm.oaitem = inv.itmcde
  `;

  try {
    var iSeriesSpecialOrders = pjs.query(specialItmsQuery);

    if (sqlstate >= "02000") {
      message = `SQL state error while fetching special items from OMITALW table. SQLstate: ${sqlstate}`;
      auto.autoLog(message, programName, 20);
      console.log(message);
      return;
    }
  } catch (err) {
    message = `Catch error while fetching special items from OMITALW table. Error: ${err}`;
    auto.autoLog(message, programName, 20);  
    console.log(message);
    return;
  }


  auto.autoLog(`Special item records pulled from OMITALW table: ${iSeriesSpecialOrders.length}`, programName, 5);    
  console.log(`Special item records pulled from OMITALW table : ${iSeriesSpecialOrders.length}`);

  await writeSpecialItems(iSeriesSpecialOrders,newMFServer);
  await writeSpecialItems(iSeriesSpecialOrders,oldMFServer);

  // */
  // END OF SPECIAL ITEMS LOGIC------------------------------------------------------------------------------------------------------------------
  
  // START OF PENDING ORDER LOGIC------------------------------------------------------------------------------------------------------------------
  // /*

  await clearPendingOrders(newMFServer);
  await clearPendingOrders(oldMFServer);

  //Formatting dates
  let currentDate = dayjs();
  let nextFiveDays = currentDate.add(5, 'day');
  nextFiveDays = parseInt(dayjs(nextFiveDays).format("YYYYMMDD"));
  currentDate = parseInt(dayjs(currentDate).format("YYYYMMDD"));
  var pendingOrdersArray = []; 

  let ordersSelectQuery = 
    `SELECT  RIGHT('000' || OSTORE, 3) AS STORE, 
      TRIM(OITEM) AS ICODE, 
      ODATE AS SHIPDATE ,
      TRIM(OQTY) AS QTY,
      TRIM(ODEPT) AS DEPARTMENT,
      TRIM(INV.CHKDIG) AS CHECKDIGIT
    FROM BULTONO AS BTO
    LEFT JOIN pwafil.invmst AS INV
    ON BTO.oitem = INV.itmcde
    Where ODATE between ${currentDate} AND ${nextFiveDays}
    ORDER BY  ODATE 
  `;

  let departmentShipDayQuery = `
   SELECT RIGHT('000' || TRIM(BSTR), 3) AS STORE,
      BSTR AS STORENUM, 
      BWDEPT AS DEPARTMENT,
     BSHIP AS DELDAYINT,
     ABS(? - BSHIP) AS DayDifference
    FROM BULPOLF
    WHERE BSTR = ? AND BWDEPT = ?
    ORDER BY DayDifference ASC, BSHIP DESC
    FETCH FIRST 1 ROW ONLY
  `;

  auto.autoLog(`Grabbing pending orders with ship dates between ${currentDate} and ${nextFiveDays} from the iSeries`, programName, 5);
  console.log(`Grabbing pending orders with ship dates between ${currentDate} and ${nextFiveDays} from the iSeries`);

  try {
    var orderData = pjs.query(ordersSelectQuery);
    if (sqlstate >= "02000") {
      auto.autoLog(`Sql state error occurred while grabbing pending orders. SQL State: ${sqlstate}`, programName, 20);
      console.error(`Sql state error occurred while grabbing pending orders. SQL State: ${sqlstate}`);
      return;
    }
  } catch (err) {
    auto.autoLog(`Catch error occurred while grabbing pending orders. Error: ${err}`, programName, 20);
    console.error(`Catch error occurred while grabbing pending orders. Error: ${err}`);
    return;
  }

  auto.autoLog(`Records pulled from BULTONO table: ${orderData.length}`, programName, 5);
  console.log(`Records pulled from BULTONO table: ${orderData.length}`);

  auto.autoLog(`Looping through pending order data.`, programName, 5);
  console.log(`Looping through pending order data.`);

  var itemsWithErrors = [];

  for (let i = 0; i < orderData.length; i++) {

    // Concatenate item code and check digit
    let itemNumber = orderData[i]["icode"] + (orderData[i]["checkdigit"] ? orderData[i]["checkdigit"] : "");

    // Get the day of the week as a number from the DayJS object
    let dbDateFormat = dayjs(String(orderData[i]["shipdate"]), 'YYYYMMDD').format('YYYY-MM-DD');
    let shipDayOfWeek = dayjs(dbDateFormat).day();

    try {
      var departmentShipDays = pjs.query(departmentShipDayQuery,[Number(shipDayOfWeek),orderData[i]["store"],orderData[i]["department"] ]);
  
      if (sqlstate >= "02000") {
        auto.autoLog(`Sql state error occurred while grabbing ship dates from BULPOLF table for store ${orderData[i]["store"]}, item ${orderData[i]["icode"]}, department ${orderData[i]["department"]}, order date ${orderData[i]["odate"]} and ship day of week ${Number(shipDayOfWeek)}. SQL State: ${sqlstate}`, programName, 20);
        console.error(`Sql state error occurred while grabbing ship dates from BULPOLF table for store ${orderData[i]["store"]}, item ${orderData[i]["icode"]}, department ${orderData[i]["department"]}, order date ${orderData[i]["odate"]} and ship day of week ${Number(shipDayOfWeek)}. SQL State: ${sqlstate}`);
        //  itemsWithErrors.push({ store: orderData[i]["store"], item: orderData[i]["icode"], date: orderData[i]["odate"], department: orderData[i]["department"]  });
        continue;
      }
    } catch (err) {
      auto.autoLog(`Catch error occurred while grabbing ship dates from BULPOLF table for store ${orderData[i]["store"]}, item ${orderData[i]["icode"]}, department ${orderData[i]["department"]}, order date ${orderData[i]["odate"]} and ship day of week ${Number(shipDayOfWeek)}. Error: ${err}`, programName, 20);
      console.error(`Catch error occurred while grabbing ship dates from BULPOLF table for store ${orderData[i]["store"]}, item ${orderData[i]["icode"]}, department ${orderData[i]["department"]}, order date ${orderData[i]["odate"]} and ship day of week ${Number(shipDayOfWeek)}. Error: ${err}`);
      continue;
    }

    // Get today's date and day of the week
    const today = dayjs();
    const todayDayOfWeek = today.day(); // Note: In Day.js, Sunday is 0, Saturday is 6

    // Adjust delDayInt if necessary to match Day.js format (1 = Monday, 7 = Sunday)
    const adjustedDelDayInt = departmentShipDays[0]["deldayint"] === 7 ? 0 : departmentShipDays[0]["deldayint"]; // Adjust if delDayInt uses 1 for Monday, 7 for Sunday
    // Calculate the target day of the week for Day.js
    const targetDayOfWeek = adjustedDelDayInt;

    // Calculate how many days to add to reach the next delDayInt
    let daysToAdd = targetDayOfWeek - todayDayOfWeek;
    if (daysToAdd <= 0) {
      daysToAdd += 7; // Ensure we're calculating the next occurrence, not past
    }

    // Calculate the target date and format it
    const targetDate = today.add(daysToAdd, 'day');
    const formattedDate = targetDate.format('YYYYMMDD');

    switch (departmentShipDays[0]["deldayint"]) {
      case 0:
          day = "Sunday";
          break;
      case 1:
          day = "Monday";
          break;
      case 2:
          day = "Tuesday";
          break;
      case 3:
          day = "Wednesday";
          break;
      case 4:
          day = "Thursday";
          break;
      case 5:
          day = "Friday";
          break;
      case 6:
          day = "Saturday";
          break;
    }

    pendingOrdersArray.push({
      STORE: orderData[i]["store"],
      ITEM: itemNumber,
      QUANTITY: orderData[i]["qty"],
      SHIP_DAY: day,
      SHIP_DATE : formattedDate,
    });

  } 
  
  await writePendingOrders(pendingOrdersArray,newMFServer);
  await writePendingOrders(pendingOrdersArray,oldMFServer);


  // */
  // END OF PENDING ORDER LOGIC------------------------------------------------------------------------------------------------------------------


  //FUNCTIONS------------------------------------------------------------------------------------------------------------------------------------
  async function clearStoreItemRecords(serverName){
    auto.autoLog(`Deleting store item records in ${storeItemsTableTemp} table in ${serverName}`, programName, 5);
    console.log(`Deleting store item records in ${storeItemsTableTemp} table in ${serverName}`);

    try {  
      var deleteResponse = pjs.data.delete(pjs.getDB(serverName), storeItemsTableTemp);
    } catch (err) {
      console.error(`Catch error occurred while deleting store items from ${storeItemsTableTemp} table in ${serverName}. Error: ${err}`);
      auto.autoLog(`Catch error occurred while deleting store items from ${storeItemsTableTemp} table in ${serverName}. Error: ${err}`, programName, 20);
      return;
    }

    console.log(deleteResponse);
    
    return;
  }

  async function writeStoreItemRecords(cripFileArray, tempDBName, prodDBName){
    auto.autoLog(`Writing new store items to ${storeItemsTableTemp} table on temp database ${tempDBName}`, programName, 5);
    console.log(`Writing new store items to ${storeItemsTableTemp} table on temp database ${tempDBName}`);

    const BATCH_SIZE = 100000; // Adjust batch size as needed
    let message = "";
  
    let writeStoreItemsQuery = `INSERT INTO ${storeItemsTableTemp} ` + "(STORE_NO, " + "ITEM_CODE, " + "MULTI, " + "SRP, " + "YTD_PURCHASE, " + "LY_PURCHASE, " + "YTD_Weeks_In_Inv, " +
    "LYWEEK, " +
    "BOOK, " +
    "SRP_CODE, " +
    "AVG_WEEKLY_MVT_UNITS) " +
    "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
  
    try {
      for (let i = 0; i < cripFileArray.length; i += BATCH_SIZE) {
        let batch = cripFileArray.slice(i, i + BATCH_SIZE);
        pjs.query(pjs.getDB(tempDBName), writeStoreItemsQuery, batch, null, null);
  
        auto.autoLog(`Batch ${Math.floor(i / BATCH_SIZE) + 1}: Processed ${batch.length} records`, programName, 5);
        console.log(`Batch ${Math.floor(i / BATCH_SIZE) + 1}: Processed ${batch.length} records`);
      }
        
      if (sqlstate >= "02000") {
        message = `SQL state error occurred while writing new store items to ${storeItemsTableTemp} table on temp database ${tempDBName}. SQLstate: ${sqlstate}`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        return;
      }
    } catch (err) {
      message = `Catch error occurrred while writing new store items to ${storeItemsTableTemp} table on temp database ${tempDBName}. Error: ${err}`;
      // auto.autoLog(message, programName, 20);
      console.log(message);
      return;
    }

    auto.autoLog(`Calling store items ${storeItemsStoredProcedure} procedure on ${prodDBName}`, programName, 5);
    console.log(`Calling store items ${storeItemsStoredProcedure} procedure on ${prodDBName}`);

    try{
      var storeItemProcResponse = pjs.query(pjs.getDB(prodDBName), storeItemsStoredProcedure);    
    } catch(err){
      message = `Catch error while calling ${storeItemsStoredProcedure} procedure. Error: ${err}`;
      auto.autoLog(message, programName, 20);    
      console.error(message);
      return;
    }
    
    console.log(`${storeItemsStoredProcedure} procedure response: ${JSON.stringify(storeItemProcResponse)}`);
    auto.autoLog(`${storeItemsStoredProcedure} procedure response: ${JSON.stringify(storeItemProcResponse)}`, programName, 5);
    return;
  }

  async function clearItemRecords(serverName){
    auto.autoLog(`Deleting item records in ${itemsTableTemp} table in ${serverName} server`, programName, 5);
    console.log(`Deleting item records in ${itemsTableTemp} table in ${serverName} server`);

    try {
      var deleteResponse = pjs.data.delete(pjs.getDB(serverName), itemsTableTemp);
    } catch (err) {
      console.error(`Catch error occurred while deleting store items from ${storeItemsTableTemp} table in ${serverName} server. Error: ${err}`);
      auto.autoLog(`Catch error occurred while deleting store items from ${storeItemsTableTemp} table in ${serverName} server. Error: ${err}`, programName, 20);
      return;    
    }

    console.log(deleteResponse);

    return;
  }

  async function writeItemRecords(warehouseItemArray, tempDBName, prodDBName){
    auto.autoLog(`Writing new items to ${itemsTableTemp} table on temp database ${tempDBName}`, programName, 5);
    console.log(`Writing new items to ${itemsTableTemp} table on temp database ${tempDBName}`);

    let message = "";

    let writeItemsQuery = `INSERT INTO ${itemsTableTemp}  (icode, ITEM_CODE,  ITEM_DESC, PACK_SIZE, LAST_SHIP_DT, STATUS,CURR_CASE_COST, ` +
    "QTY_HARD_LIMIT, " +
    "HDESC, " +
    "DEPARTMENT, " +
    "UPC, " +
    "SUBS_KEY, " +
    "REG_CASE_COST, " +
    "BUYER, " +
    "HISTYD, " +
    "SALELY, " +
    "WKCNT, " +
    "WKSLY) " +
    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  
    try {
      var insertItemsResponse = pjs.query(pjs.getDB(tempDBName), writeItemsQuery, warehouseItemArray, null, null);
  
      if (sqlstate >= "02000") {
        message = `SQL state error while writing new items to ${itemsTableTemp} table on temp database ${tempDBName}. SQLstate: ${sqlstate}`;
        console.log(message);
        auto.autoLog(message, programName, 20);
        return;
      }
    } catch (err) {
      message = `Catch error occurrred while writing new items to ${itemsTableTemp} table on temp database ${tempDBName}. Error: ${err}`;
      // auto.autoLog(message, programName, 20);      
      console.log(message);
      return;
    }

    console.log(insertItemsResponse);

    auto.autoLog(`Calling store items ${itemsStoredProcedure} procedure on ${prodDBName}`, programName, 5);
    console.log(`Calling store items ${itemsStoredProcedure} procedure on ${prodDBName}`);

    try{
      var itemsProcedureResponse = pjs.query(pjs.getDB(prodDBName), itemsStoredProcedure);  
    } catch(err){
      sqlMsg = `Catch error while calling ${itemsStoredProcedure} procedure. Error: ${err}`;
      auto.autoLog(sqlMsg, programName, 20); 
      console.log(sqlMsg);
      return;  
    }

    console.log(`${itemsStoredProcedure} procedure response: ${JSON.stringify(itemsProcedureResponse)}`);
    auto.autoLog(`${itemsStoredProcedure} procedure response: ${JSON.stringify(itemsProcedureResponse)}`, programName, 5);

    return;
  }

  async function clearSpecialItems(serverName){
    auto.autoLog(`Deleting special item records in ${specialItemsTable} table in ${serverName} server`, programName, 5);
    console.log(`Deleting special item records in ${specialItemsTable} table in ${serverName} server`);

    let deleteOldOrdersQuery = `DELETE FROM ${specialItemsTable}`;

    try {
      var deleteResults = pjs.query(pjs.getDB(serverName), deleteOldOrdersQuery, null, null);

    } catch (error) {
      console.error(error);
    }

    console.log(deleteResults);

    return;

  }

  async function writeSpecialItems(specialItemArray, serverName){
    auto.autoLog(`Writing new special items to PW_ALLOW_ITEM table on temp database ${serverName}`, programName, 5);
    console.log(`Writing new special items to PW_ALLOW_ITEM table on temp database ${serverName}`);

    let message = "";

    var writeSpecialItmsQuery = "INSERT INTO PW_ALLOW_ITEM (ITEM_ALLOWED, STORE_ALLOWED)  VALUES (?,?)";

    try {
      var insertSpecialItmsResponse = pjs.query(pjs.getDB(serverName), writeSpecialItmsQuery, specialItemArray, null, null);
 
      if (sqlstate >= "02000") {
        message = `SQL state occurred error while writing special items to PW_ALLOW_ITEM table. SQLstate: ${sqlstate}`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        return;
      }
    } catch (err) {
      message = `Catch error occurred while writing special items to PW_ALLOW_ITEM table. Error: ${err}`;
      auto.autoLog(message, programName, 20);      
      console.log(message);
      return;
    }

    console.log(insertSpecialItmsResponse);
  }

  async function clearPendingOrders(serverName){
    auto.autoLog(`Deleting pending order records in ${pendingOrdersTable} table in ${serverName} server`, programName, 5);
    console.log(`Deleting pending order records in ${pendingOrdersTable} table in ${serverName} server`);

    let deleteOldOrdersQuery = `DELETE FROM ${pendingOrdersTable}`;

    try {
      var deleteResponse = pjs.query(pjs.getDB(serverName), deleteOldOrdersQuery, null, null);

    } catch (error) {
      console.error(`Catch error occurred while deleting pending orders from ${pendingOrdersTable} table in ${serverName}. Error: ${err}`);
      auto.autoLog(`Catch error occurred while deleting pending orders from ${pendingOrdersTable} table in ${serverName}. Error: ${err}`, programName, 20);
    }

    console.log(deleteResponse);
    
    return;
  }

  async function writePendingOrders(pendingOrdersArray, serverName){
    auto.autoLog(`Writing new special items to ${pendingOrdersTable} table on  ${serverName}`, programName, 5);
    console.log(`Writing new special items to  ${pendingOrdersTable} table on ${serverName}`);

    var writePendingOrdersQuery = `INSERT INTO ${pendingOrdersTable} (STORE, ITEM, QUANTITY, SHIP_DAY, SHIP_DATE) VALUES (@STORE,@ITEM,@QUANTITY,@SHIP_DAY, @SHIP_DATE)`; //Test
 
    try{
      var insertPendingOrdersResponse = pjs.query(
        pjs.getDB(serverName),
        writePendingOrdersQuery,
        pendingOrdersArray,
        null,
        null
      );
 
      if (sqlstate >= "02000") {
        auto.autoLog(`Sql state error occurred while writing pending orders to ${serverName} to ${pendingOrdersTable}. SQL State: ${sqlstate}`, programName, 20);
        console.error(`Sql state error occurred while writing pending orders to ${serverName} to ${pendingOrdersTable}. SQL State: ${sqlstate}`);
      }
 
    } catch (err) {
      auto.autoLog(`Catch error occurred while while writing pending orders to ${serverName} to ${pendingOrdersTable}. Error: ${err}`, programName, 20);
      console.error(`Catch error occurred while while writing pending orders to ${serverName} to ${pendingOrdersTable}. Error: ${err}`);
    }

    console.log(insertPendingOrdersResponse);
  }

  auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
  console.log(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);


}


exports.run = mobileFrameUpdate;
