/*
Created By: Jake Lovett
Date Created: 01/07/2025
Modified By: Jake Lovett
Modified Date: 01/07/2025
This program is a re-write of the VMCOrderProcessor C# application.
The program will fetch all VMC orders from the MobileFrame SQL database, format the data, write a CSV file to the iSeries, then send the file to the VMC SFTP server.
*/

// #region Imports
const Client = require("ssh2-sftp-client");
// const csv = require("csv-writer").createArrayCsvWriter;
const csv = require("csv-writer").createObjectCsvWriter;
const dayjs = require("dayjs");
const fs = require("fs");
const path = require("path");
require("dotenv").config();

// #endregion
async function vmcOrderProcessor() {
  // #region Program level variables
  const auto = pjs.require("pjslogging/autoLog.js");
  const programName = "MF_VMC_Order_Processor";
  const currentPort = profound.settings.port;

  // const newMfServer = process.env.MF_SERVER_NEW; //TO DO: Need to add server names to .env file
  // const oldMfServer = process.env.MF_SERVER_OLD; //TO DO: Need to add server names to .env file

  const portToUse = 8083;
  const newMfServer = "MF-SQL"; //Remove once the server names are added to .env file
  const oldMfServer = "Server2012-MFS"; //Remove once the server names are added to .env file

  const localDirectoryPath = path.join("../", "reports/Profound Reports/VMC");
  // const remoteDirectoryPath = "/C:/Users/pigglywiggly/ItemCodes/Test";
  const remoteDirectoryPath = currentPort == portToUse ? "/C:/Users/pigglywiggly/ItemCodes" : "/C:/Users/pigglywiggly/ItemCodes/Test";

  let vmcOrders = [];
  // #endregion

  log(`${programName} running on port ${currentPort}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`, 5);

  const currentDay = dayjs().day();
  // const currentDay = 3; //Turn on during testing

  if(currentDay != 4){
    log(`${programName} was triggered on a non-Thursday. Terminating the process, as it is scheduled to run only on Thursdays`, 20);

    return;
  }  

  createFile(oldMfServer);

  if (vmcOrders.length > 0) {
    writeToVmcServer();
  }

  sendEmail(`${programName} Summary (${currentPort})`, `There were ${vmcOrders.length} orders sent to the VMC FTP Site.`);

  log(`${programName} is done running on port ${currentPort}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`, 5);

  // Fetch vmcOrders, call the writeFileLocal() function if orders exist
  function createFile(server) {
    try {
      // const query = "SELECT * FROM v_VMCOrders WHERE STORE_ = 169";
      const query =
        currentPort == portToUse ? "SELECT * FROM v_VMCOrders ORDER BY STORE_" : "SELECT * FROM v_VMCOrders_Test WHERE STORE_ = 169 ORDER BY STORE_";

      let message = `Getting VMC Orders from ${server}`;
      log(message, 5);

      vmcOrders = pjs.query(pjs.getDB(server), query);
      log(`VMC orders returned: ${vmcOrders.length}`, 5);

      if (vmcOrders && vmcOrders.length > 0) {
        writeFileLocal(vmcOrders);
      } else {
        let message = `No VMC Orders present on server ${server}.`;
        log(message, 10);
        return;
      }
    } catch (err) {
      let message = `Error while fetching VMC Orders from ${server}: ${err}`;
      log(message, 20);
    }
  }

  // Flag orders
  function flagOrders(server) {
    if (vmcOrders && vmcOrders.length > 0) {
      try {
        let message = `Attempting to flag ${vmcOrders.length} orders`;
        log(message, 5);
        vmcOrders.forEach((order) => {
          pjs.query(
            pjs.getDB(server),
            currentPort == portToUse
              ? `EXEC sp_Upd_PW_ORDER_HEADER_FLAGGED_FOR_VMC @id = ${order.OrderId}`
              : `EXEC sp_Upd_PW_ORDER_HEADER_FLAGGED_FOR_VMC_Test @id = ${order.OrderId}`
          );
        });
      } catch (err) {
        let message = `Error while flagging orders: ${err}`;
        log(message, 20);
      }
    } else {
      log(`No VMC orders were flagged since the vmcOrders array is empty`, 5);
    }
  }
  // Generate file name for VMC order CSV
  function getFileName() {
    // #region Function-level variables
    let instanceName = currentPort == portToUse ? "PRODUCTION" : "TEST";
    let dayPortion = dayjs().get("date");
    let monthPortion = dayjs().get("month") + 1; // dayjs().get("month") is zero indexed (i.e. Jan = 0), so we add 1
    let yearPortion = dayjs().get("year");
    let counter = 0;
    // #endregion

    // Initial name pattern generation
    if (dayPortion.toString().length == 1) {
      dayPortion = `0${dayPortion}`;
    }

    if (monthPortion.toString().length == 1) {
      monthPortion = `0${monthPortion}`;
    }

    let fileName = `${instanceName}${monthPortion}${dayPortion}${yearPortion}.csv`;
    return fileName;
  }

  function log(messageIn, code) {
    message = messageIn;

    console.log(message);
    auto.autoLog(message, programName, code);

    switch (code) {
      case 10:
        // sendEmail(`${programName}: Informational Message`, message);
        break;
      case 20:
        sendEmail(`${programName}: Errors During Processing`, message);
        break;

      default:
        break;
    }
  }

  function sendEmail(subject, message) {
    const emailSend = `SNDSMTPEMM RCP((sgray@pwadc.com)(cbates@pwadc.com)(retsystems@pwadc.com)(support@pwadc.com)) SUBJECT('${subject}') ` + `NOTE('${message}')`;

    try {
      pjs.runCommand(emailSend);
    } catch (err) {
      console.error(`Error sending email. Error: ${err}`);
      // auto.autoLog(`Error sending email. Error: ${err}`, programName, 20);
    }
  }

  function writeFileLocal(vmcOrders) {
    const headers = ["STORE_", "NameSlug", "SAJitem", "VMCitem", "UPC", "ItmDesc", "Pack", "Size", "OrderQty", ","];
    const localFilePath = path.join(localDirectoryPath, getFileName());
    const csvWriter = csv({
      path: localFilePath,
      header: headers,
    });
    try {
      let message = `Writing file ${getFileName()} to ${localDirectoryPath}...`;
      log(message, 5);
      if (vmcOrders && vmcOrders.length > 0) {
        csvWriter.writeRecords(vmcOrders);

        log(`Emailing VMC file to retail team.`, 5);

        const emailFile =`SNDSMTPEMM RCP((sgray@pwadc.com)(jlovett@pwadc.com)(cjphillips@pwadc.com)(bedwards@pwadc.com)(cbates@pwadc.com)) SUBJECT('Backup Order ${getFileName()}') ` +
        `ATTACH(('${localFilePath}'))`;
        
        try {
          pjs.runCommand(emailFile);
        }
        catch(err) {
          log(`Error while emailinng vmc file to retail team`, 20);
          // auto.autoLog(`Error sending email. Error: ${err}`, programName, 20);
        }        
      } else {
        log("No VMC Orders to write to file.", 10);
      }
    } catch (err) {
      let message = `Error writing records to ${localDirectoryPath}: ${err}`;
      log(message, 20);
    }
  }

  // Establish connection with SFTP server and attempt to transmit the .CSV file
  async function writeToVmcServer() {
    const sftpOptions = {
      host: process.env.VMC_SFTP_HOST,
      user: process.env.VMC_SFTP_USERNAME,
      password: process.env.VMC_SFTP_PASSWORD,
    };
    const c = new Client();
    let fileName = getFileName();
    let localFilePath = `${localDirectoryPath}/${fileName}`;
    let remoteFilePath = `${remoteDirectoryPath}/${fileName}`;

    log("Connecting to SFTP server", 5);

    // Break out of process if file doesn't exist on local machine
    if (!fs.existsSync(localFilePath)) {
      log(`File "${localFilePath}" does not exist on the local system.`, 20);
      return;
    }

    try {
      await c.connect(sftpOptions);

      const dirExists = await c.exists(remoteDirectoryPath);
      if (dirExists === "d") {
        console.log(dirExists);

        const readStream = fs.createReadStream(localFilePath);
        await c.put(readStream, remoteFilePath);
      } else {
        log("Remote directory does not exist.", 20);
        return;
      }

      const fileExists = await c.exists(remoteFilePath);
      console.log(fileExists);

      if (fileExists === "-") {
        log("File successfully written to VMC server.", 5);
        flagOrders(oldMfServer);
      } else {
        log("File not written to VMC server.", 20);
        return;
      }
    } catch (err) {
      console.error(err);
    } finally {
      await c.end().catch((e) => {
        // Manually suppressing ECONNRESET err, because it happens any time c.end() uses
        if (e.code !== "ECONNRESET") {
          console.error("Error during disconnect: ", e);
        }
      });
    }
  }


}

exports.run = vmcOrderProcessor;
