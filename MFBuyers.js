const fs = pjs.wrap(require('fs'));
const fspath = require('path');
const { send } = require('process');
const dayjs = require('dayjs');
const auto = pjs.require("pjslogging/autoLog.js");

async function MF_Buyers() {

    const currentPath = __dirname + fspath.sep; // C:\tyler\modules\
    const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\mobileframe-import\

    const programName = `MF_Buyers`;

    const currentPort = profound.settings.port;
    auto.autoLog("currentPort" + currentPort, programName, 5);

    let buyerTable = 'rpmsbuyl';

    let mfBuyerTable = "PW_BUYER";

    const newMFServerRealDB = "MF-SQL";

    //Setting tables
    if(currentPort === 8083){

    }

    auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs()}`, programName, 5); 
  
    let subject = `${programName} (${currentPort}): Error Detected During Processing`;
    let message = "";

    var reportSummaryVariables = {};
    reportSummaryVariables.delete = 0;
    reportSummaryVariables.put = 0;
    reportSummaryVariables.post = 0;
    reportSummaryVariables.invalidCallType = [];
    reportSummaryVariables.missingInfo = [];
    reportSummaryVariables.httpErrors = [];
    reportSummaryVariables.flagErrors = [];

    let getNewBuyerRecords = [];

    const buyerTableQuery = `SELECT * FROM ${buyerTable} WHERE rbproc2 != 'Y'`;


    //GET DATA FROM RPMSBUYL AS400 TABLE
    try {
        auto.autoLog(`Querying ${buyerTable} table to grab buyer records.`, programName, 5);

        getNewBuyerRecords = pjs.query(buyerTableQuery);
    } catch (err) {
        message = `An error occurred while retreiving buyer records from ${buyerTable}: ${err}`;
        auto.autoLog(message, programName, 20);

        await sendEmail(subject,message);

        auto.autoLog(`${programName}is done running...${dayjs()}`, programName, 5);

        return;
    } 

    auto.autoLog(`Records pulled from ${buyerTable}: ${getNewBuyerRecords.length}`, programName, 5);

    if(getNewBuyerRecords.length == 0){
    message = `No records found in ${buyerTable} :${getNewBuyerRecords.length}.`
    console.log(message);
    auto.autoLog(message, programName, 20);

    await sendEmail(subject,message);

    console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);

    return;
  }

    console.log(getNewBuyerRecords);

    for (let i = 0; i < getNewBuyerRecords.length; i++) {
        var callType = getNewBuyerRecords[i]["rbevent"];
        callType = callType.trim();
    
        var buyerCode = getNewBuyerRecords[i]["rbcode"];
        buyerCode = buyerCode.trim();
    
        var firstName = getNewBuyerRecords[i]["rbfirst"];
        firstName = firstName.trim();
    
        var lastName = getNewBuyerRecords[i]["rblast"];
        lastName = lastName.trim();
    
        var phoneNumber = getNewBuyerRecords[i]["rbphone"];
        // Jacks API may not accept 0 phone number OR it could be because I convert it here which may be unneccesary.
        phoneNumber = phoneNumber.trim();
        phoneNumber = parseInt(phoneNumber);
    
        var emailAddress = getNewBuyerRecords[i]["rbemail"];
        emailAddress = emailAddress.trim();
    
        // if(callType.length == 0 || buyerCode.length == 0 || firstName.length == 0 || lastName.length == 0 || phoneNumber.length == 0 || emailAddress.length == 0){
        //   message = `One of the key fields did not have a value for buyer code ${buyerCode}. callType:${callType} ,firstName: ${firstName} ,lastName: ${lastName} ,phoneNumber: ${phoneNumber} ,emailAddress: ${emailAddress} `;
        //   auto.autoLog(message, programName, 20);
    
        //   reportSummaryVariables.missingInfo.push(`buyerCode: ${buyerCode}. callType:${callType} ,firstName: ${firstName} ,lastName: ${lastName} ,phoneNumber: ${phoneNumber} ,emailAddress: ${emailAddress}`) 
    
        //   await sendEmail(subject,message);
        //   continue;
        // }

        let sqlStatement = "";

        if (callType == "D") {
            auto.autoLog(`Deleting record in ${newMFServerRealDB} server for buyer code ${buyerCode}`, programName, 5);

            ++reportSummaryVariables.delete;

            sqlStatement = `DELETE FROM ${mfBuyerTable} WHERE BUYER_ID = '${buyerCode}'`;
      
          } else if (callType == "A") {
            auto.autoLog(`Adding new record in ${newMFServerRealDB} server for buyer code ${buyerCode}`, programName, 5);

            sqlStatement = `INSERT INTO ${mfBuyerTable} (BUYER_ID, FNAME, LNAME, PHONE, EMAIL) VALUES ('${buyerCode}', '${firstName}', '${lastName}', '${phoneNumber}', '${emailAddress}')`;

            ++reportSummaryVariables.post;
      
          } else if (callType == "U") {
            auto.autoLog(`Updating record in ${newMFServerRealDB} server for buyer code ${buyerCode}`, programName, 5);
            sqlStatement = `UPDATE ${mfBuyerTable} SET BUYER_ID = '${buyerCode}',FNAME = '${firstName}',LNAME = '${lastName}', PHONE = '${phoneNumber}', EMAIL = '${emailAddress}' WHERE BUYER_ID = '${buyerCode}'`;
            ++reportSummaryVariables.put;
      
          } else {
            reportSummaryVariables.invalidCallType.push(`buyerCode: ${buyerCode}, callType: ${callType} `);
      
            message = `The callType of ${callType} is not valid for buyer code ${buyerCode}. Please contact IT.`;
      
            auto.autoLog(message, programName, 20);
      
      
            await sendEmail(subject,message);
      
            continue;
        }

        console.log(sqlStatement);

        try{
            var sqlResponse = pjs.query(pjs.getDB(newMFServerRealDB), sqlStatement, null, null, null);

        } catch(err){
            console.log(err);
        }


        if(sqlResponse.affectedRows == 0){

            message = `The record for buyer code ${buyerCode} did not update, delete, or post the new record. Affected Row Count: ${JSON.stringify(sqlResponse)}`;
      
            auto.autoLog(message, programName, 20);
      
      
            await sendEmail(subject,message);
      
            continue;
        }

        const localTimeStamp = dayjs().format();
        const timeStamp = dayjs(localTimeStamp).format('HHmmss');
        const dateStamp = dayjs(localTimeStamp).format('YYYYMMDD');
      
        try {
          auto.autoLog(`Flagging buyer code ${buyerCode} in ${buyerTable}.`, programName, 5);
    
          var buyerUpdateResponse = pjs.query(`UPDATE ${buyerTable} SET rbproc2 = 'Y', rbsdate = ?, rbstime = ? WHERE rbproc2  != 'Y' and rbcode = ? LIMIT 1`, 
          [dateStamp, timeStamp, buyerCode]);
    
          if (sqlstate != "00000") {
            message = `Sql error while flagging buyer code ${buyerCode} in ${orderHeaderTable} table. SQL state: ${sqlstate}`;

            auto.autoLog(errorMessage, programName, 20);
    
            reportSummaryVariables.flagErrors.push(`Error: ${message}`);
            return await sendEmail(subject,message);
          }      
    
        } catch (err) {

          message = `An error occurred while flagging buyer code ${buyerCode} in ${buyerTable}: ${err}`;
          auto.autoLog(message, programName, 20);
    
          reportSummaryVariables.flagErrors.push(`Error: ${message}`);
    
          return await sendEmail(errorSubject,message);
        }
    }


    subject = `${programName}: Report Summary`
    reportSummary = 
    `
    End of MobileFrame Buyer Processing: Summary Report
    Date: ${dayjs()}
    Total Records Retrieved from ${buyerTable}: ${getNewBuyerRecords.length}
      - Number of Records Updated (PUT): ${reportSummaryVariables.put}
      - Number of Records Created (POST): ${reportSummaryVariables.post}
      - Number of Records Deleted: ${reportSummaryVariables.delete}
    
    Records with Incorrect Call Type: 
    ${reportSummaryVariables.invalidCallType.join("\n")}
    
    Records With Missing Key Information:
    ${reportSummaryVariables.missingInfo.join("\n")}
  
    Records with Errors During Processing:
    ${reportSummaryVariables.httpErrors.join("\n")}
  
    Records with Errors During Flagging:
    ${reportSummaryVariables.flagErrors.join("\n")}
  
  
    ** In case of any processing errors, an email notification was dispatched to the developers **
    `
  
    console.log(reportSummary);
  
    await sendEmail(subject,reportSummary);
  
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs()}`, programName, 5);


    async function sendEmail(subject, message){
        auto.autoLog(`sendEmail is running...`, programName, 5);
    
        let emailSend =
        `SNDSMTPEMM RCP((sgray@pwadc.com)(gweldon@pwadc.com)) SUBJECT('${subject}') ` +
        `NOTE('${message}')`;
    
        try {
          pjs.runCommand(emailSend);
        }
        catch(err) {
          // auto.autoLog(`Error sending email: ${err}`, programName, 20);
          console.log(`Error sending email: ${err}`);
    
        }
    
        return;
    
      }

}

exports.run = MF_Buyers;
