-- BTC Item Table------------------------------------------------------------------------
select count(*) from [CrossDock].[dbo].[BTC_Items]; --PROD
select * from [CrossDock].[dbo].[BTC_Items]; -- PROD

select count(*) from [CrossDock].[dbo].[BTC_Items_Test]; -- DEV
select * from [CrossDock].[dbo].[BTC_Items_Test]; --DEV
--delete from [CrossDock].[dbo].[BTC_Items_Test]; --DEV

--insert into [CrossDock].[dbo].[BTC_Items]
select BTCItemNumber,Description,ItemType,BTCHeader,Pack,UnitUPC,CartonUPC,Status,MF_Upd_Flg,Tag_Change_Flg from [CrossDock].[dbo].[BTC_Items_Test]


-- BTC Store Item Table------------------------------------------------------------------
select count(*) from [CrossDock].[dbo].[BTC_StoreItems];  --  --PROD
select * from [CrossDock].[dbo].[BTC_StoreItems];  --  --PROD

select count(*) from [CrossDock].[dbo].[BTC_StoreItems_Test];  --  DEV
select * from [CrossDock].[dbo].[BTC_StoreItems_Test];  --  DEV
--delete from [CrossDock].[dbo].[BTC_StoreItems_Test]; -- DEV

insert into [CrossDock].[dbo].[BTC_StoreItems]
select BTCCustomerID,StoreNumber,BTCItem,UnitCost,CaseCost,UnitSRP,CaseSRP,RecordDate,ItemUpdate,Modified from [CrossDock].[dbo].[BTC_StoreItems_Test]

select BTCCustomerID,StoreNumber,count(*) from [CrossDock].[dbo].[BTC_StoreItems]
group by BTCCustomerID,StoreNumber;

-- BTC Price Book Table------------------------------------------------------------------
select count(*) FROM [MobileFrame].[dbo].[PW_BTC_ITEM]; --  --PROD
select * FROM [MobileFrame].[dbo].[PW_BTC_ITEM]; --  --PROD

SELECT STORE_NUMBER, COUNT(*) as ItemCount 
FROM [MobileFrame].[dbo].[PW_BTC_ITEM]
GROUP BY STORE_NUMBER
ORDER BY STORE_NUMBER;


select * from [MobileFrame].[dbo].[PW_BTC_ITEM_TEST];  --  DEV
select count(*) FROM [MobileFrame].[dbo].[PW_BTC_ITEM_TEST]; --  DEV
--delete from [MobileFrame].[dbo].[PW_BTC_ITEM_TEST]; --  DEV

--insert into [MobileFrame].[dbo].[PW_BTC_ITEM]
select create_date,last_update,created_by,last_updated_by,deleted_by,BTC_ITEM_NO,DESCRIPTION,PACK,CARTON_UPC,UNIT_SRP,CASE_COST,STORE_NUMBER,PACK_UPC from [MobileFrame].[dbo].[PW_BTC_ITEM_TEST]

SELECT STORE_NUMBER, COUNT(*) as ItemCount 
FROM [MobileFrame].[dbo].[PW_BTC_ITEM_TEST]
GROUP BY STORE_NUMBER
ORDER BY STORE_NUMBER;

SELECT STORE_NUMBER, COUNT(*) as ItemCount 
FROM [MobileFrame].[dbo].[PW_BTC_ITEM_TEST]
WHERE STORE_NUMBER IN (55,65,75,114,147,148,384)
GROUP BY STORE_NUMBER
ORDER BY STORE_NUMBER;
----------------------------------------------------------------------------------------

--Query to Generate Tags From Price Book Table------------------------------------------
-- Price Book Table Tag Query
SELECT
    si.STORE_NUMBER AS StoreNumber, 
    si.BTC_ITEM_NO AS BTCItemNum, 
    si.UNIT_SRP AS UnitSRP, 
    si.UNIT_SRP AS CaseSRP,
    i.Status,
    i.Description,
    i.Pack,
    i.UnitUPC
FROM [MobileFrame].[dbo].[PW_BTC_ITEM_TEST] as si
LEFT JOIN [CrossDock].[dbo].[BTC_Items_Test] as i ON si.BTC_ITEM_NO = i.BTCItemNumber
WHERE 
    i.Status != '' AND
    (
    --STORE_NUMBER in (388, 448, 287, 285, 290, 328, 138, 348, 130, 134)
    --STORE_NUMBER in (388, 448, 287, 285, 328, 138, 348, 130, 134)
    STORE_NUMBER in (55)
    )
ORDER BY si.STORE_NUMBER 

--Query to Generate Tags From Store Items Table--------------------------------------------
SELECT
  si.StoreNumber, 
  si.BTCItem, 
  si.UnitSRP, 
  si.CaseSRP,
  si.ItemUpdate,
  i.Description,
  i.Pack,
  i.UnitUPC
FROM [CrossDock].[dbo].[BTC_StoreItems] as si
LEFT JOIN [CrossDock].[dbo].[BTC_Items] as i ON si.BTCItem = i.BTCItemNumber
WHERE 
  si.ItemUpdate != ''
ORDER BY si.StoreNumber    
