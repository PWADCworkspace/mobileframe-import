
/*
    Created By : Samuel Gray
    Date Created: 11/11/2024
    Modified By: Samuel Gray
    Modified Date: 11/12/2024

    Purpose: 
    Fetch all claims created the previous day and write data to an excel file containing the totals per claim type for a buyer, total per buyer, and total per department
*/

//Importing npm packages
const dayjs = require("dayjs");
const path = require("path");
var xl = require("excel4node");

async function mobileFrameOrdersByDeptReport(){

    const programName = "Mobile_Frame_Orders_By_Dept_Report_V1"; // Set program name
    const currentPort = profound.settings.port; // Retrieve current port pgm is running on

    //Set tables
    let cwFile = 'pwafil.CWMETOUT';
    let meatFile = 'pwafil.METOUT';
    let iceFile = 'pwafil.ICEOUT';
    let regFile = 'pwafil.REGOUT';
    let proFile = 'pwafil.PROOUT';
    let invFile = 'pwafil.invmst';

    const path = '';  // Set location excel file will be written to
    const fileName = `${yesterday}_Claims.xlsx`; // Declare excel file name
    const fullPath = path + fileName; // Concatonate path and file name

    //Use prod eClaims database on 8083 instance
    if(currentPort === 8083){

    }  

    auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
    console.log(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);

    const ordersByDeparmentQuery = `

    WITH ordersByDeparment AS (
        SELECT 
            SUM(CAST(substring(rout.regout, 4,3) as INT)) as Quantity,
            inv.dept AS Department,
            '${regFile}' as File
        FROM  ${regFile} AS rout
        LEFT JOIN ${invFile} AS inv ON dec(substring(rout.regout, 7, 5), 5, 0) = inv.itmcde
        GROUP BY inv.dept


        UNION ALL
        SELECT 
            SUM(CAST(substring(pout.proout, 4,3) as INT)) as Quantity,
            inv.dept AS Department,
            '${proFile}' as File
        FROM  ${proFile} AS pout
        LEFT JOIN ${invFile} AS inv ON dec(substring(pout.proout, 7, 5), 5, 0) = inv.itmcde
        GROUP BY inv.dept

        UNION ALL
        SELECT 
            mout.sum(qty) AS Quantity,
            inv.dept AS Department,
            '${meatFile}' AS File
        FROM ${meatFile} as mout
        LEFT JOIN ${invFile} AS inv ON mout.code = inv.itmcde
        GROUP BY inv.dept

        UNION ALL
        SELECT 
            cwmout.sum(qty) AS Quantity,
            inv.dept AS Department,
            '${cwFile}' AS File
        FROM ${cwFile} as cwmout
        LEFT JOIN ${invFile} AS inv ON cwmout.code = inv.itmcde
        GROUP BY inv.dept


        UNION ALL
        SELECT 
            iceout.sum(qty) AS Quantity,
            inv.dept AS Department,
            '${iceFile}' AS File
        FROM ${iceFile} as iceout
        LEFT JOIN ${invFile} AS inv ON iceout.code = inv.itmcde
        GROUP BY inv.dept
        )
         
        SELECT * FROM ordersByDeparment ORDER BY ;

    `;

    const ordersByStoresQuery = `
    
    WITH ordersByStore AS (
        SELECT 
            SUBSTRING(rout.regout, 1,3) as Store,
            SUM(CAST(substring(rout.regout, 4,3) as INT)) as Quantity,
            inv.dept AS Department,
            '${regFile}' as File
        FROM  ${regFile} AS rout
        LEFT JOIN ${invFile} AS inv ON dec(substring(rout.regout, 7, 5), 5, 0) = inv.itmcde
        GROUP BY SUBSTRING(rout.regout, 1,3), inv.dept


        UNION ALL
        SELECT 
            SUBSTRING(pout.proout, 1,3) as Store,
            SUM(CAST(substring(pout.proout, 4,3) as INT)) as Quantity,
            inv.dept AS Department,
            '${proFile}' as File
        FROM  ${proFile} AS pout
        LEFT JOIN ${invFile} AS inv ON dec(substring(pout.proout, 7, 5), 5, 0) = inv.itmcde
        GROUP BY SUBSTRING(pout.proout, 1,3), inv.dept

        UNION ALL
        SELECT 
            mout.strno as Store,
            mout.sum(qty) AS Quantity,
            inv.dept AS Department,
            '${meatFile}' AS File
        FROM ${meatFile} as mout
        LEFT JOIN ${invFile} AS inv ON mout.code = inv.itmcde
        GROUP BY mout.strno, inv.dept

        UNION ALL
        SELECT 
            cwmout.strno as Store,
            cwmout.sum(qty) AS Quantity,
            inv.dept AS Department,
            '${cwFile}' AS File
        FROM ${cwFile} as cwmout
        LEFT JOIN ${invFile} AS inv ON cwmout.code = inv.itmcde
        GROUP BY cwmout.strno, inv.dept


        UNION ALL
        SELECT 
            iceout.strno as Store,
            iceout.sum(qty) AS Quantity,
            inv.dept AS Department,
            '${iceFile}' AS File
        FROM ${iceFile} as iceout
        LEFT JOIN ${invFile} AS inv ON iceout.code = inv.itmcde
        GROUP BY iceout.strno, inv.dept
        )
         
        SELECT * FROM ordersByStore ORDER BY Store;
    `;
    
    let ordersByDeparment = [];

    try{
        ordersByDeparment = pjs.query(ordersByDeparmentQuery);
    } catch(err){
        console.log(`Catch error occurred while retrieving data for the ordersByDeparment report`);
        return;
    }   

    let ordersByStore = [];
    
    try{
        ordersByStore = pjs.query(ordersByStoresQuery);
    } catch(err){
        console.log(`Catch error occurred while retrieving data for the ordersByStoresQuery report`);
        return;
    }   

    // Create a new instance of a Workbook class
    var wb = new xl.Workbook();

    //Create a new worksheets
    var byDeptWS = wb.addWorksheet('Orders By Department');
    var byStoreWS = wb.addWorksheet('Orders By Store');

    //Declare column headers
    const byDepartmentHeaders = [
        'Department',
        'Total Quantity',
        'File',
    ];   
    
    const byStoreHeaders = [
        'Department',
        'Total Quantity',
        'File',
    ]; 

    //Declare title style
    const titleStyles = wb.createStyle({
        font: {bold: true, color: '#FFFFFF', size: 18},
        aligment: {horizontal: 'center', vertical: 'center'},
        fill: {type: 'pattern', patternType: 'solid', bgColor: '#FF0000',fgColor: '#FF0000'}
    });

    //Declare column header style
    const headerStyles = wb.createStyle({
        font: {bold: true, color: '#FFFFFF'},
        fill: {type: 'pattern', patternType: 'solid', fgColor: '#ff5533'}
    });

    //Declare sub total style
    const subtotalStyles = wb.createStyle({
        font: {bold: true, italic: true, color: '#000000'},
        fill: {type: 'pattern', patternType: 'solid', fgColor: '#D9D9D9'}
    });

    //Declare regular row white style
    const regularRowStyleWhite = wb.createStyle({
        font: {color: '#000000'},
        fill: {type: 'pattern', patternType: 'solid', fgColor: '#FFFFFF'}
    });

    //Declare regular row red style
    const regularRowStyleRed = wb.createStyle({
        font: {color: '#000000'},
        fill: {type: 'pattern', patternType: 'solid', fgColor: '#FFCCCC'}
    });

    //Declare regular row red style
    const currencyStyle = wb.createStyle({
        numberFormat: '$#,##0.00; ($#,##0.00); -',
    });    

    //Write sheet titlees
    byDeptWS.cell(1,1,1,14, true).string(`Mobile Frame Orders By Department`).style(titleStyles);
    byStoreWS.cell(1,1,1,14, true).string(`Mobile Frame Orders By Store`).style(titleStyles);

    //Write column headers to orders by department tab
    byDepartmentHeaders.forEach((header, colIndex) => {
        byDeptWS.cell(2, colIndex + 1).string(header).style(headerStyles);
    });

    //Write column headers to orders by store tab
    byStoreHeaders.forEach((header, colIndex) => {
        byStoreWS.cell(2, colIndex + 1).string(header).style(headerStyles);
    });    

    let rowIndex = 0; // track current excel row

    let lastDpt = ''; // track current department 
    let totalPerDept = 0; // track total quantity per department
    let lastStore = ''; //track current store
    let totalPerStore = 0; // track total sum of total per file
    
    let totalQuantity = 0; // track total quantity


    if(ordersByDeparment.length > 0){
        
        // Loop through each order record and assign data to rows
        for(let i = 0; i < ordersByDeparment.length; i++){

            const row = ordersByDeparment[i]; 
    
            const quantity = row.quantity;
            const department = row.department;
            const file = row.file;

            const regularRowStyle = (rowIndex % 2 == 0) ? regularRowStyleRed : regularRowStyleWhite;

            // Assign data rows
            byDeptWS.cell(rowIndex + 3, 1).string(file).style(regularRowStyle); // Column A
            byDeptWS.cell(rowIndex + 3, 2).string(department).style(regularRowStyle); // Column B
            byDeptWS.cell(rowIndex + 3, 3).string(quantity).style(regularRowStyle); // Column C

            // If the deparment changes write the total case quantity for the LAST DEPARMENT on its own row
            if(department = lastDpt && rowIndex != 0){

                rowIndex += 1; // Adjust row number

                byDeptWS.cell(rowIndex + 3, 1).string('').style(subtotalStyles); // Column A
                byDeptWS.cell(rowIndex + 3, 10).string(`Department '${lastDpt}' Case Quantity Total: `).style(subtotalStyles); // Column B
                byDeptWS.cell(rowIndex + 3, 10).number(totalPerDept).style(subtotalStyles); // Column C

                totalPerDept = 0; // Reset total when department changes
                rowIndex += 1; // Adjust row number
            }

            // If it's the last record write the total case quantity on its own row
            if(ordersByDeparment.length - i == 1){                
                byDeptWS.cell(rowIndex + 3, 1).string('').style(subtotalStyles); // Column A
                byDeptWS.cell(rowIndex + 3, 10).string(`Case Quantity Total: `).style(subtotalStyles); // Column B
                byDeptWS.cell(rowIndex + 3, 10).number(totalQuantity).style(subtotalStyles); // Column C
            }

            lastDpt = row.department;
            totalQuantity += row.quantity;
            totalPerDept += row.quantity;

        }

        totalPerDept = 0; // reset for next report
        totalQuantity = 0; // reset for next report
        rowIndex = 0; // reset for next report
    }

    if(ordersByStore.length > 0){
     
        for(let i = 0; i < ordersByStore.length; i++){

            const row = ordersByStore[i]; 

            const store = row.store;
            const quantity = row.quantity;
            const department = row.department;
            const file = row.file;

            const regularRowStyle = (rowIndex % 2 == 0) ? regularRowStyleRed : regularRowStyleWhite;

            // Assign data rows
            byStoreWS.cell(rowIndex + 3, 1).string(file).style(regularRowStyle); // Column A
            byStoreWS.cell(rowIndex + 3, 1).string(store).style(regularRowStyle); // Column A
            byStoreWS.cell(rowIndex + 3, 2).string(department).style(regularRowStyle); // Column B
            byStoreWS.cell(rowIndex + 3, 3).string(quantity).style(regularRowStyle); // Column C

            // If the store changes write the total case quantity for the LAST STORE on its own row
            if(store = lastStore && rowIndex != 0){

                rowIndex += 1; // Adjust row number

                byStoreWS.cell(rowIndex + 3, 1).string('').style(subtotalStyles); // Column A
                byStoreWS.cell(rowIndex + 3, 10).string(`Store '${lastStore}' Case Quantity Total: `).style(subtotalStyles); // Column B
                byStoreWS.cell(rowIndex + 3, 10).number(totalPerStore).style(subtotalStyles); // Column C

                totalPerStore = 0; // Reset total when department changes
                rowIndex += 1; // Adjust row number
            }                  

            lastStore = store;
            totalPerStore += quantity;
        }

        totalPerStore = 0; // reset for next report
        rowIndex = 0; // reset for next report
    }
    
    wb.write(fullPath);

    auto.autoLog(`${programName} is done running on ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
    console.log(`${programName} is done running on ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);   

}

exports.run = mobileFrameOrdersByDeptReport;

