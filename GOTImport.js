/*
Process:
 - This program is a part of the Mobile Frame import process.
 - MobileframeImport.js will call this program before calling CLORDERSIN
 - It will look at the InboundOrders folder in the IFS path on line 27 below server/got to check for new .txt file orders
 - When there are new files it will read through each of them and write a record to PWAFIL.ORDERSIN for each line item in the text file.
 - Once all of the records for that file are read and imported it will rename that .txt file and move it into OrderAcks for GOT to reference.

Author: Tyler Hobbs
Date: 01/10/2022
*/

const fs = pjs.wrap(require('fs'));
const fspath = require('path');

function getGOTOrders() {

  var returnObj = {};
  returnObj.Message = 'GOTImport PGM ran successfully.';
  returnObj.ErrMessage = '';

// Adjust library here to switch to testing.
  pjs.defineTable("ordersin","PWAFIL/ORDERSIN", {write: true, userOpen: true, keyed:true})
  
  const directoryPath = '/pjsprod' + fspath.sep + 'modules' + fspath.sep + 'mobileframe-import' + fspath.sep + 'localGOT' + fspath.sep + 'InboundOrders';

  let writeArray = [];
  
// Get current CallID count from CALLIdCount.txt file and increment for each order.
  var count;
  try {
    let data =  fs.readFileSync('/pjsprod' + fspath.sep + 'modules' + fspath.sep + 'mobileframe-import' + fspath.sep + 'localGOT' + fspath.sep + 'CallIDCount' + fspath.sep + 'CallIDCount.txt');
    // console.log('data:' + data);
    count = parseInt(data);
    if (count >= 9999) {
      count = 1001;
    }
  } catch (err) {
    console.error(err);
    returnObj.ErrMessage = err
    return returnObj;
  }

//Read hot folder to find all new files and call loopFiles for them.
function readFolder(path) {
  console.log('starting GOTOrdersCopy Process.');

  var files = fs.fiber.readdir(path);
  // console.log(files);

  if (files.length === 0){
    var error = "NO NEW FILES IN INBOUNDORDERS!";
    returnObj.ErrMessage = error
    return returnObj;
  }

  ordersin.open();
  loopFiles(files, path);
  ordersin.close();
}

// loop through each file found and call readFiles funciton on them.
function loopFiles(files, path) {
  var endWithTxt = /\.txt$/i;
  for (var i=0; i<files.length; i++) {
    if (endWithTxt.test(files[i])) {
      const filePath = path + fspath.sep + files[i];
      readFiles(filePath);
    }
  }
}

// Read each txt file and gather their information.
function readFiles(filePath) {
  try {
    let text = fs.readFileSync(filePath, "utf8")
  
    let fileStoreNumber = text.slice(1, 4);
    fileStoreNumber = fileStoreNumber.padStart(3, '0');

    // NOTE: This will use the date they sent for when the order was generated but todays time for what time it was written into our system.
    let timeNow = pjs.timestamp(); 
    let fileOrderDate = text.slice(4, 12) + ' ' + pjs.char(timeNow).substr(11,10).replace(/\./g, '') + '.0';
  
    let fileDeliveryDate = text.slice(12, 20);
  
    let itemLines = text.toString("utf-8").trim().split("\n");
    writeArray = [];
    for (var i = 1, iMax = itemLines.length; i < iMax; i++) {
      let fileItemNumber = itemLines[i].slice(1, 7);
  
      let fileItemQuantity = itemLines[i].slice(7, 10);
      fileItemQuantity.trim();
    
      writeArray.push({
        storeNumber: fileStoreNumber,
        orderDate: fileOrderDate,
        deliveryDate: fileDeliveryDate,
        itemNumber: fileItemNumber,
        itemQuantity: fileItemQuantity
      });
    }

    writeTo400(writeArray);
    copyFilesForAck(filePath);
  } catch (err) {
    console.error(err);
    returnObj.ErrMessage = err
    console.log(returnObj.ErrMessage);
    return returnObj;
  }
}

// COPY AND RENAME THE FILE TO OrderAcks FOLDER, THEN DELETE THE ORIGINAL FILE
function copyFilesForAck(filePath) {

  const acknowledgePath = '/pjsprod' + fspath.sep + 'modules' + fspath.sep + 'mobileframe-import' + fspath.sep + 'localGOT' + fspath.sep + 'OrderAcks' + fspath.sep;

  let file = filePath.substring(59, filePath.length - 4); //NOTE: SUBSTRING 59 IS BASED ON THE LEGNTH OF THE FILE BEING PASSED TO copyFilesForACks!
  fs.fiber.copyFile(filePath,acknowledgePath + file + ".ack");
  
  //REMOVE FILES AFTER THEIR COPIED TO ACK
  fs.fiber.unlink(filePath);
  console.log(`FILE ${file} REMOVED FROM INBOUND.`); 
}

function writeTo400(writeArray) {

  let today = pjs.date();
  let todayStr = pjs.char(today,"*ymd0");  // eg 220105 for Jan 5
  let callID = count.toString() + String(todayStr);

  for (var i = 0, iMax = writeArray.length; i < iMax; i++) { 

    let itemNumber = writeArray[i]['itemNumber'];
    let itemNumberNoCheck = itemNumber.substring(0, itemNumber.length -1 );

    //NOTE: This if check will be used to writie future orders.
    //NOTE: It is NOT READY YET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // if (todaysHour > 11) {
    // // if it is AFTER 11 we have to add it to bultin file to be processed later

    // let itemDept = '';
    // const sqlqueryIn ='select DEPT from PWAFIL.INVMST WHERE ITMCDE = ?';
    // let itemRecordSet = pjs.query(sqlqueryIn, [itemNumberNoCheck]);
    // if (itemRecordSet && itemRecordSet.length>0 )  itemDept = itemRecordSet[0]['dept'];

      // console.time(`writing ${writeArray[i]['itemNumber']} to Bultin File`);
      
      // BSTORE = 3s0 = 451
      // BITEM = 5s0 = 25777
      // BDATE = 8s0 = YYYYMMDD
      // BQTY = 3s0 = 2
      // BDEPT = 1 = 'A'
      // BEDAT = 8s0 = YYYYMMDD + 50 years from today. 

      // const writeBultinQuery = `insert into TYLER.BULTIN (BSTORE, BITEM, BDATE, BQTY, BDEPT, BEDAT) values(?,?,?,?,?,?) with NONE`;
      
      // pjs.query(writeBultinQuery, [
      //   writeArray[i]['storeNumber'],
      //   writeArray[i]['itemNumber'],
      //   writeArray[i]['deliveryDate'],
      //   writeArray[i]['itemQuantity'],
      //   itemDept,
      //   writeArray[i]['orderDate'],
      // ]);

      // console.timeEnd(`writing ${writeArray[i]['itemNumber']} to Bultin File`);

    // } else {
      // otherwise write it to ordersin to be sent ASAP.

      // console.time(`writing ${writeArray[i]['itemNumber']} to Ordersin File`);

      // let callID = String(todayStr) + count.toString().padStart(4, '0');
  
        var storeNumber = writeArray[i]['storeNumber'];
        var type = 'O'; 
        var vendor = '0';  
        var qty = writeArray[i]['itemQuantity']; 
        var orderDate = writeArray[i]['orderDate'];
        var processed = "";
        var Flag1 = "";
        var Flag2 = "";

        oincal = callID;
        oinstr = storeNumber;
        ointyp = type;
        oinvnd = vendor;
        oinitm = itemNumber;
        oinqty = qty;
        oindte = orderDate;
        oinprs = processed;
        oinfg1 = Flag1;
        oinfg2 = Flag2;
      
      ordersin.write();
    // }
  }
  count++;
  if (count >= 9999) {
    count = 1001;
  }
  // update the CallIDCount.txt file with the new CallID value after each order is written.
  fs.writeFile('/pjsprod' + fspath.sep + 'modules' + fspath.sep + 'mobileframe-import' + fspath.sep + 'localGOT' + fspath.sep + 'CallIDCount' + fspath.sep + 'CallIDCount.txt',
    count,
    (err) => {
      if (err) {
        console.error(err);
        returnObj.ErrMessage = err
        return returnObj;
      }
    }
  );
}

// start here:
  try{ 
    readFolder(directoryPath);
  } catch (err) {
    returnObj.ErrMessage = err
    return returnObj;
  }
  return returnObj;

}
exports.getGOTOrders = getGOTOrders;