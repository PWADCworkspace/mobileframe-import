const dayjs = require("dayjs");
const fs = require('fs');
const path = require('path');
const readline = require('readline');
const { parse } = require('csv-parse');
const Client = require('ftp');
const auto = pjs.require("pjslogging/autoLog.js");
const csv = require("csv-writer").createObjectCsvWriter;

/*
  Created By: Samuel Gray
  Date Create: 08/12/2024
  Modified By: Samuel Gray
  Date Modified: 10/28/2024

  Purpose: Retrieve item and store item files from BTC FTP site and insert data into Mobile Frame
*/

async function MF_BTC_Import_V2() {

  const programName = "MF_BTC_Import_V2";
  const currentPort = profound.settings.port;

  let btcPriceBookTable = '[MobileFrame].[dbo].[PW_BTC_ITEM_TEST]';
  let btcStoreItemsTable = '[CrossDock].[dbo].[BTC_StoreItems_Test]'
  let btcItemsTable = '[CrossDock].[dbo].[BTC_Items_Test]';

  const oldServer = "Server2012-MFS";
  const newServer = "MF-SQL";

  const itemFile = "ALLITEMS.DAT";
  const itemFileShort = "ALLITEMS";
  const storeItemFile = "000900CI";
  const priceBookFile = 'PBALL.TXT';
  const priceBookFileShort = 'PBALL';
  const pathToHotFolder =  '\\\\cfdata\\BartenderHOT\\BTC_PriceChangeTags';

  if(currentPort == 8083){
    btcPriceBookTable = "[MobileFrame].[dbo].[PW_BTC_ITEM]";
    btcStoreItemsTable = "[CrossDock].[dbo].[BTC_StoreItems]";
    btcItemsTable = "[CrossDock].[dbo].[BTC_Items]";
  }

  //Update Price Book Information----------------------------------------------------------------------------------------------------------
  let priceBookFileName = await getBTCFiles(priceBookFile);
  // let priceBookFileName = ['PBALL-217 test']; //TURN ON DURING TESTING

  if(priceBookFileName.length == 0){
    console.log(`The ${priceBookFile} file was not found on the FTP site: ${priceBookFileName.length}`);
  }

  for (const fileName of priceBookFileName) {

    //Format data in the file
    let formattedPriceBookData = await getBTCFilesLocally(fileName);
  
    console.log(`Records found in ${fileName}: ${formattedPriceBookData.length}`);
  
    //Skip processing file if there is no data
    if(formattedPriceBookData.length == 0){
      console.log(`No records found in ${fileName} :${formattedPriceBookData.length}.`);
      continue;
    } else {
      await updatePriceBookTable(formattedPriceBookData,oldServer);
      await updatePriceBookTable(formattedPriceBookData,newServer);
    }
  };  
  
  //Update General Store Item Information----------------------------------------------------------------------------------------------------
  let storeItemFileNames = await getBTCFiles(storeItemFile);

  if(storeItemFileNames.length == 0){
    console.log(`The ${storeItemFile} file was not found on the FTP site: ${storeItemFileNames.length}`);
  }

  for (const fileName of storeItemFileNames) {

   //Format data in the file
    let formattedStoreItemData = await getBTCFilesLocally(fileName);      
  
    console.log(`Records found in ${fileName}: ${formattedStoreItemData.length}`);
    ////Skip processing file if there is no data
    if(formattedStoreItemData.length == 0){
      console.log(`No records found in ${fileName} :${formattedStoreItemData.length}.`);
      continue;
    } else {
      await updateStoreItemsTable(formattedStoreItemData,oldServer);
      await updateStoreItemsTable(formattedStoreItemData,newServer);
    }  
  };    

  //Update Item Information------------------------------------------------------------------------------------------------------------------
  let itemFileName = await getBTCFiles(itemFile);

  if(itemFileName.length == 0){
    console.log(`The ${itemFile} file was not found on the FTP site: ${itemFileName.length}`);
  }

  for (const fileName of itemFileName) {

    //Format data in the file
    let formattedItemData = await getBTCFilesLocally(fileName);      

    console.log(`Records found in ${fileName}: ${formattedItemData.length}`);
  
    //Skip processing file if there is no data
    if(formattedItemData.length == 0){
      console.log(`No records found in ${fileName} :${formattedItemData.length}.`);
      continue;
    } else {
      await updateItemsTable(formattedItemData,oldServer);
      await updateItemsTable(formattedItemData,newServer);
    }  
  };
  
  if(storeItemFileNames.length > 0 || itemFileName.length > 0){
    updateStoreItemInformation(oldServer);
    updateStoreItemInformation(newServer);
  }
    
  // await generateStoreSetupTags();

  async function getBTCFiles(fileType){
      const remoteDirectoryPath = '/QDLS/MAILBOX/OUT/000900';
      const localDirectoryPath = path.join('/', `Reports/Profound Reports/BTC`);

      const c = new Client();  
  
      return new Promise((resolve, reject) => {
        const records = [];
  
        c.on('ready', function() {
          c.cwd(remoteDirectoryPath, function(err) {
            if (err) {
              reject(`Error changing directory: ${err}`);
              c.end();
              return;
            }
  
            c.list(async function(err, list) {
              if (err) {
                reject(`Error occurred while listing files: ${err}`);
                c.end();
                return;
              }
  
              let regex = "";
              
              if(fileType == itemFile || fileType == priceBookFile) {
                regex = new RegExp(`${fileType}`);
              } else {
                regex = new RegExp(`^${fileType}\\.\\d+$`);
              }
  
              let filteredFiles = list.filter(file => regex.test(file.name));
   
              const downloadPromises = filteredFiles.map(file => {
                const remoteFilePath = file.name;
                records.push(remoteFilePath);
                const localFilePath = path.join(localDirectoryPath, remoteFilePath);
  
                return new Promise((resolveDownload, rejectDownload) => {
                  c.get(remoteFilePath, function(err, stream) {
                    if (err) {
                      rejectDownload(`Error downloading file: ${err}`);
                      return;
                    }
  
                    const writeStream = fs.createWriteStream(localFilePath);
                    stream.pipe(writeStream);
  
                    writeStream.on('finish', () => {
                      console.log(`File downloaded successfully: ${localFilePath}`);
  
                      // c.delete(remoteFilePath, err => {
                      //   if(err){
                      //     rejectDownload(`Error deleting file on FTP site: ${err}`);
                      //   }
  
                      //   console.log(`File deleted successfully from FTP site: ${remoteFilePath}`);
                      //   // resolveDownload();
                      // });
                      resolveDownload();
                    });
  
                    writeStream.on('error', err => {
                      rejectDownload(`Error writing file: ${err}`);
                    });
                  });
                });
              });
  
              // Wait for all downloads to complete
              Promise.all(downloadPromises)
                .then(() => {
                  c.end();
                  resolve(records); // Resolve with the list of downloaded files
                })
                .catch(error => {
                  c.end();
                  reject(error);
                });
            });
          });
        });
  
        c.connect({
          host: '12.150.232.250',
          user: 'piggly',
          password: 'piggly'
        });
  
        console.log(`Connecting to FTP Site.`);
      });
  }
  async function getBTCFilesLocally(fileName){
        const localFilePath = path.join('/', `Reports/Profound Reports/BTC/${fileName}`);
        const shortFileName = fileName.split('.')[0];
        const number = fileName.split('.')[1];

        let quotesOn = fileName == itemFileShort ? true : false;

        let record = {};
        let records = [];

        return await new Promise((resolve, reject) => {
            const rl = readline.createInterface({
              input: fs.createReadStream(localFilePath),
              output: process.stdout,
              terminal: false
            });
      
            //Read each line in the file
            rl.on('line', (line) => {
              parse(line, {
                trim: true,
                skip_empty_lines: true,
                relax: true,
                quote: quotesOn
              }, (err, output) => {
                //Throw an error if there is an issue parsing the line data
                if (err) {
                  console.error(`Error parsing line: ${err}`);
                  return;
                }
      
                //Check if output and output[0] are defined before proceeding
                if (output && output[0]) {
                  const values = output[0];  
                  
                  if(shortFileName == priceBookFileShort){
                    record = { //Price Book File
                      BTC_Item_No : values[1],
                      Description: values[2].replace(/(^"|"$)/g, ''), // Remove unnecessary quotes
                      Pack: values[7].replace(/(^"|"$)/g, ''), // Remove unnecessary quotes
                      Carton_UPC : values[9],
                      Unit_SRP: values[11],
                      Case_Cost : values[10],
                      Store_Number : values[12],
                      Pack_UPC : values[8],
                    }
                  }

                  if(shortFileName == itemFileShort){
                    record = { //Items file
                      BTC_Item_Number: values[0],
                      Description: values[1].replace(/(^"|"$)/g, ''),
                      Item_Type: values[2],
                      Pack: values[3].replace(/(^"|"$)/g, ''),
                      Unit_UPC: values[4],
                      Carton_UPC: values[5],
                      Status: values[6].replace(/(^"|"$)/g, ''),
                      BTCHeader: values[7],
                    }
                  }

                  if(storeItemFile == shortFileName) {
                    record = {
                      BTC_Store_Number: values[0],
                      Store_Number: values[6],
                      BTC_Item_Number: values[1],
                      Unit_Cost: values[2],
                      Case_Cost: values[3],
                      Unit_SRP: values[4],
                      Case_SRP: values[5],
                      RecordDate: dayjs().format('YYYYMMDD')
                    }; 
                  }           
    
                  records.push(record);
              } else {
                console.log(line);
                console.warn(`Skipped empty or malformed line in ${localFilePath}`);
                auto.autoLog(`Skipped empty or malformed line in ${localFilePath}`, programName, 10); 
      
              }
            });
          });
      
          //After reading the whole file return results
          rl.on('close', () => {
            console.log(`Finished reading ${localFilePath}`);
            resolve(records);
          });
      
          //Throw an error if there are issues reading the file
          rl.on('error', (err) => {
            console.error(`Error reading file ${localFilePath}. Error: ${err}`);
            reject(err);
          });    
      });   
  }
  async function updatePriceBookTable(formattedData,serverName){
    console.log(`Updating ${serverName} with price book information`);
    const currentDate = dayjs().format('YYYYMMDD');


    try{
      let deleteResponse = pjs.query(pjs.getDB(serverName),`DELETE FROM ${btcPriceBookTable}`);
      console.log(deleteResponse);

    } catch(err){
      console.log(`Catch error occurred while deleting price book information. Error: ${err}`);
      return;
    };

    const insertPriceBookQuery = `
      INSERT INTO ${btcPriceBookTable} (BTC_ITEM_NO, DESCRIPTION, PACK, CARTON_UPC, UNIT_SRP, CASE_COST, STORE_NUMBER, PACK_UPC)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?)
    `;
    try{
      let insertResponse =  await pjs.query(pjs.getDB(serverName), insertPriceBookQuery, formattedData,null,null);
      console.log(insertResponse);

    } catch(err){
      console.log(`Catch error occurred while inserting new price book information. Error: ${err}`);
      return;
    }
  }
  async function updateItemsTable(formattedData,serverName){
    console.log(`Updating ${serverName} with item information`);

    const currentDate = dayjs().format('YYYYMMDD');

    // try{
    //   let deleteItemsResponse = await pjs.query(pjs.getDB(serverName), `DELETE FROM ${btcItemsTable} WHERE STATUS = 'D'`, null, null);
    //   let deleteStoreItemsResponse = await pjs.query(pjs.getDB(serverName), `DELETE FROM ${btcStoreItemsTable} WHERE ITEMUPDATE = 'D'`, null, null);
    //   console.log(deleteItemsResponse);
    //   console.log(deleteStoreItemsResponse);
    // } catch(err){
    //   console.log(`Catch error occurred while deleting flagged items (D) from ${btcItemsTable} and ${btcStoreItemsTable}. Error: ${err}`);
    //   return;
    // }

    try{
      let deleteItemsResponse = pjs.query(pjs.getDB(serverName), `DELETE FROM ${btcItemsTable}`, null, null); 
      console.log(deleteItemsResponse);
    } catch(err){
      console.log(`Catch error occurred while deleting ALL items from ${btcItemsTable}. Error: ${err}`);
      return;
    }

    try{
      let insertResponse = await pjs.query(
        pjs.getDB(serverName), 
        `INSERT INTO ${btcItemsTable} (BTCItemNumber, Description, ItemType, Pack, UnitUPC, CartonUPC, Status, BTCHeader) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, 
        formattedData,null,null
      );
      console.log(insertResponse);
    } catch(err){
      console.log(`Catch error occurred while inserting items into ${btcItemsTable}. Error: ${err}`);
      return;
    }

    // try{
    //   // let removeStatusesResponse = pjs.query(pjs.getDB(serverName), `UPDATE ${btcStoreItemsTable}  SET ItemUpdate = '' WHERE`, null, null); 
    //   // console.log(removeStatusesResponse);

    //   let updateStatusesResponse = await pjs.query(
    //     pjs.getDB(serverName), 
    //     `UPDATE si SET si.ItemUpdate = i.Status FROM ${btcStoreItemsTable}  si INNER JOIN ${btcItemsTable} i ON si.BTCItem = i.BTCItemNumber WHERE si.RecordDate = '${currentDate}'`, 
    //     null,null
    //   );

    //   console.log(updateStatusesResponse);

    // } catch(err){
    //   console.log(`Catch error occurred while updating the item statuses in the ${btcStoreItemsTable} table. Error: ${err}`);
    //   return;      
    // }

    // try{
    //   let itemUpdatesResponse = await pjs.query(
    //     pjs.getDB(serverName), 
    //     `UPDATE ${btcPriceBookTable}
    //     SET Description = i.Description, Pack = i.Pack, Carton_UPC = i.CartonUPC, Pack_UPC = i.UnitUPC
    //     FROM ${btcPriceBookTable} as pb
    //     INNER JOIN ${btcItemsTable} as i ON pb.BTC_ITEM_NO = i.BTCItemNumber
    //     WHERE i.Status = 'U'`, 
    //     null,null
    //   );

    //   console.log(itemUpdatesResponse);

    //   let storeItemUpdatesResponse = await pjs.query(
    //     pjs.getDB(serverName), 
    //     `UPDATE ${btcPriceBookTable}
    //     SET CASE_COST = SI.CaseCost, Unit_SRP = SI.CaseCost
    //     FROM ${btcPriceBookTable} as pb
    //     INNER JOIN ${btcStoreItemsTable} as SI ON pb.BTC_ITEM_NO = SI.BTCItem and pb.Store_Number = SI.StoreNumber
    //     WHERE SI.ItemUpdate = 'U'`, 
    //     null,null
    //   );

    //   console.log(storeItemUpdatesResponse);

    // } catch(err){
    //   console.log(`Catch error occurred while updating price book information in ${btcPriceBookTable} table. Error: ${err}`);
    //   return;
    // }
  }
  async function updateStoreItemsTable(formattedData,serverName){
    console.log(`Updating ${serverName} with store item information`);
    const currentDate = dayjs().format('YYYYMMDD');


    try{
      let insertResponse = await pjs.query(
        pjs.getDB(serverName), 
        `INSERT INTO ${btcStoreItemsTable} (BTCCustomerID, StoreNumber, BTCItem, UnitCost, CaseCost, UnitSRP, CaseSRP, RecordDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, 
        formattedData,null,null
      );      
      console.log(insertResponse);
    } catch(err){
      console.log(`Catch error occurred while inserting new store item updates into ${btcStoreItemsTable}. Error: ${err}`);
      return;
    }

  }
  async function updateStoreItemInformation(serverName){

    const currentDate = dayjs().format('YYYYMMDD');

    try{
      // let removeStatusesResponse = pjs.query(pjs.getDB(serverName), `UPDATE ${btcStoreItemsTable}  SET ItemUpdate = '' WHERE`, null, null); 
      // console.log(removeStatusesResponse);

      let updateStatusesResponse = await pjs.query(
        pjs.getDB(serverName), 
        `UPDATE si SET si.ItemUpdate = i.Status FROM ${btcStoreItemsTable}  si INNER JOIN ${btcItemsTable} i ON si.BTCItem = i.BTCItemNumber WHERE si.RecordDate = '${currentDate}'`, 
        null,null
      );

      console.log(updateStatusesResponse);

    } catch(err){
      console.log(`Catch error occurred while updating the item statuses in the ${btcStoreItemsTable} table. Error: ${err}`);
      return;    
    }   
    
    
    try{
      let itemUpdatesResponse = await pjs.query(
        pjs.getDB(serverName), 
        `UPDATE ${btcPriceBookTable}
        SET Description = i.Description, Pack = i.Pack, Carton_UPC = i.CartonUPC, Pack_UPC = i.UnitUPC
        FROM ${btcPriceBookTable} as pb
        INNER JOIN ${btcItemsTable} as i ON pb.BTC_ITEM_NO = i.BTCItemNumber
        WHERE i.Status = 'U'`, 
        null,null
      );

      console.log(itemUpdatesResponse);

      let updatePriceBookTableResponse = await pjs.query(
        pjs.getDB(serverName), 
        `UPDATE ${btcPriceBookTable}
        SET CASE_COST = SI.CaseCost, Unit_SRP = SI.CaseSRP
        FROM ${btcPriceBookTable} as pb
        INNER JOIN ${btcStoreItemsTable} as SI ON pb.BTC_ITEM_NO = SI.BTCItem and pb.Store_Number = SI.StoreNumber
        WHERE SI.ItemUpdate = 'U' AND SI.RecordDate = '${currentDate}'`, 
        null,null
      );

      console.log(updatePriceBookTableResponse);

    } catch(err){
      console.log(`Catch error occurred while updating price book information in ${btcPriceBookTable} table. Error: ${err}`);
      return;
    }
  }
  async function generateStoreSetupTags(serverName,storeSetup){

    let retrieveTagItems = "";

    if(storeSetup){
      retrieveTagItems = `
        SELECT
          si.STORE_NUMBER AS StoreNumber, 
          si.BTC_ITEM_NO AS BTCItemNum, 
          si.UNIT_SRP AS UnitSRP, 
          si.UNIT_SRP AS CaseSRP,
          i.Status,
          i.Description,
          i.Pack,
          i.UnitUPC
        FROM ${btcPriceBookTable} as si
        LEFT JOIN ${btcItemsTable} as i ON si.BTC_ITEM_NO = i.BTCItemNumber
        WHERE 
          i.Status != ''          
            --STORE_NUMBER in (388, 448, 287, 285, 290, 328, 138, 348, 130, 134)
            --STORE_NUMBER in (388, 448, 287, 285, 328, 138, 348, 130, 134)
            --STORE_NUMBER in (13)          
        ORDER BY si.STORE_NUMBER,si.BTC_ITEM_NO
      
      `;
    } else {
      retrieveTagItems = `
          SELECT
          si.STORE_NUMBER AS StoreNumber, 
          si.BTC_ITEM_NO AS BTCItemNum, 
          si.UNIT_SRP AS UnitSRP, 
          si.UNIT_SRP AS CaseSRP,
          i.Status,
          i.Description,
          i.Pack,
          i.UnitUPC
      FROM ${btcPriceBookTable} as si
      LEFT JOIN ${btcItemsTable} as i ON si.BTC_ITEM_NO = i.BTCItemNumber
      LEFT JOIN ${btcStoreItemsTable} as i ON si.BTC_ITEM_NO = i.BTCItemNumber
      WHERE 
          i.Status != '' AND
          (
            --STORE_NUMBER in (388, 448, 287, 285, 290, 328, 138, 348, 130, 134)
            --STORE_NUMBER in (388, 448, 287, 285, 328, 138, 348, 130, 134)
            STORE_NUMBER in (55)
          )
      ORDER BY si.STORE_NUMBER , si.BTC_ITEM_NO
      
      `;
      
    }
    //Declare sql query
    const retrieveUpdatedStoreItemsQuery =  `
        SELECT
            si.STORE_NUMBER AS StoreNumber, 
            si.BTC_ITEM_NO AS BTCItemNum, 
            si.UNIT_SRP AS UnitSRP, 
            si.UNIT_SRP AS CaseSRP,
            i.Status,
            i.Description,
            i.Pack,
            i.UnitUPC
        FROM ${btcPriceBookTable} as si
        LEFT JOIN ${btcItemsTable} as i ON si.BTC_ITEM_NO = i.BTCItemNumber
        WHERE 
            i.Status != '' AND
            (
              --STORE_NUMBER in (388, 448, 287, 285, 290, 328, 138, 348, 130, 134)
              --STORE_NUMBER in (388, 448, 287, 285, 328, 138, 348, 130, 134)
              STORE_NUMBER in (13)
            )
        ORDER BY si.STORE_NUMBER 
    `;

    console.log(retrieveUpdatedStoreItemsQuery);

    //Check if the directory is accessable
    fs.access(pathToHotFolder, fs.constants.F_OK, (err) => {
      if(err){
        log(`${pathToHotFolder} is not accessible: ${err}`, 20);
        return;
      }
    });

    log(`${pathToHotFolder} is accessible`, 5);
    log(`Fetching all store items that are flagged with a status.`, 5);

    let itemsRequireTags = [];

    try{
      itemsRequireTags = await pjs.query(pjs.getDB(serverName),retrieveUpdatedStoreItemsQuery, null,null);
    } catch(err){
      log(`Catch error occurred while fetching all store items that are flagged with a status. Error: ${err}`, 5);
      return;
    }

    log(`Number of BTC items returned: ${itemsRequireTags.length}`, 5);

    console.log(itemsRequireTags[0]);

    //Declare CSV file column headers
    const headers = [
      {id: 'StoreNumber', title: 'StoreNumber' },
      {id: 'BTCItemNum', title: 'BTCItemNum' },
      {id: 'UnitSRP', title: 'UnitSRP' },
      {id: 'CaseSRP', title: 'CaseSRP' },
      {id: 'ChangeStatus', title: 'ChangeStatus' },
      {id: 'Description', title: 'Description' },
      {id: 'PackSize', title: 'PackSize' },
      {id: 'UPC', title: 'UPC' },
    ]

    const groupByStore = {};

    //Iterate through each store item record
    itemsRequireTags.forEach((record) => {
      //Set the store from current record
      const storeNum = record.StoreNumber;
      //Check if the groupByStore object already has an array for this store
      if(!groupByStore[storeNum]){
        //If not, initilze an empty array for this store
        groupByStore[storeNum] =[];
      }

      //Add the current record to the array for the this store
      groupByStore[storeNum].push({
        StoreNumber : String(record.StoreNumber).trim(),
        BTCItemNum : String(record.BTCItemNum).trim(),
        UnitSRP : String(parseFloat(record.UnitSRP).toFixed(2)),
        CaseSRP : String(parseFloat(record.CaseSRP).toFixed(2)), 
        ChangeStatus : String(record.Status.trim()),
        Description : String(record.Description.trim()),
        PackSize: String(record.Pack.trim()),
        UPC: String(record.UnitUPC.trim())
      });
    });

    //Iterate thrugh each entry in the groupByStore object
    Object.entries(groupByStore).forEach(([storeNum, storeRecords]) => {
      //Define path with store file name
      const localFilePath = path.join(pathToHotFolder, `Store ${storeNum}.csv`);
      //Create CSV writer instance with the specified path and column headers
      const csvWriter = csv({
        path : localFilePath,
        header: headers,

      });
      csvWriter
        .writeRecords(storeRecords) //Write the records for the current store to CSV file
        .then(() => console.log(`CSV created for store ${storeNum}`)) //Log sucess message once CSV file is create
        .catch((err) => console.error(`Catch error occurred while creating CSV file for store ${storeNum}. Error ${err}`)); //Log an error if the CSV did not get created
    });
    
  }
  async function log(messageIn,code){
      pjs.define("message", {type: 'char', length: 246});
      message = messageIn;
      console.log(message);
      // auto.autoLog(message, programName, code);

      if(code == 20){
        // sendEmail(`${programName}: Errors During Processing`,message);
      }

      return;
  }
  function sendEmail(subject,message){
      const emailSend =`SNDSMTPEMM RCP((sgray@pwadc.com)(gweldon@pwadc.com)) SUBJECT('${subject}') ` +
      `NOTE('${message}')`;
  
      try {
          pjs.runCommand(emailSend);
      }
      catch(err) {
          console.error(`Error sending email. Error: ${err}`);
          auto.autoLog(`Error sending email. Error: ${err}`, programName, 20);
      }
  }    

}

exports.run=MF_BTC_Import_V2;
