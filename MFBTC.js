const dayjs = require("dayjs");
const fs = require('fs');
const path = require('path');
const readline = require('readline');
const { parse } = require('csv-parse');
const Client = require('ftp');
const auto = pjs.require("pjslogging/autoLog.js");
const csv = require("csv-writer").createObjectCsvWriter;

/*
  Created By: Samuel Gray
  Date Create: 08/12/2024
  Modified By: Samuel Gray
  Date Modified: 10/28/2024

  Purpose: Retrieve item and store item files from BTC FTP site and insert data into Mobile Frame
*/

async function MF_BTC_Import() {
    
  const programName = "MF_BTC_Import";
  const currentPort = profound.settings.port;
  const currentDay = dayjs().day();

  //Setting MF databases
  const newMFCrossDockDB = "MF-SQL-Cross-Dock"; 
  const newMFDatabase = "MF-SQL";

  const oldMFCrossDockDB = "Server2012-SQL-Cross-Dock"; 
  const oldMFDatabase = "Server2012-MFS";

  //Setting file names
  const itemFile = "ALLITEMS.DAT";
  const itemFile2 = "ALLITEMS";
  const storeItemFile = "PBALL.TXT";
  const storeItemFile2 = "PBALL";
  const storeSetupFile = "000900CI";

  //Setting dev tables
  let btcItemsTable = "BTC_ITEMS";
  let btcStoreItemsTable = "BTC_STOREITEMS";
  let btcStoreItemsTable2 = "PW_BTC_ITEM";

  console.log(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
  auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5); 

  let subject = `${programName}: Error Detected During Processing`;
  let message = ""; 


  //// START OF STORE ITEM SETUP FILE ---------------------------------------------------------------------------------------------------------------------
  ////Download store setup files and return file names
  console.log(`Grabbing ${storeSetupFile} file from FTP site.`);
  auto.autoLog(`Grabbing ${storeSetupFile} file from FTP site.`, programName, 5); 

  let storeItemResults = await fetchInformation(storeSetupFile);

  if(storeItemResults.length == 0){
    console.log(`The ${storeSetupFile} file was not found on the FTP site: ${storeItemResults.length}`);
    auto.autoLog(`The ${storeSetupFile} file was not found on the FTP site: ${storeItemResults.length}`, programName, 10); 
  }

  ////Loop through each file and retured from function
  for (const fileName of storeItemResults) {

    //Format data in the file
    let storeItemArray = await formatInformation(fileName);

    console.log(`Records found in ${fileName}: ${storeItemArray.length}`);
    auto.autoLog(`Records found in ${fileName}: ${storeItemArray.length}`, programName, 5); 

    ////Skip processing file if there is no data
    if(storeItemArray.length == 0){
      console.log(`No records found in ${fileName} :${storeItemArray.length}.`);
      auto.autoLog(`No records found in ${fileName} :${storeItemArray.length}`, programName, 5); 
      continue;
    }

    // ////Write formatted data to mobile frame
    let writeStoreItemToNewServer = await insertInformation(newMFDatabase,newMFCrossDockDB,storeSetupFile,storeItemArray);
    let writeStoreItemToOldServer = await insertInformation(oldMFDatabase,oldMFCrossDockDB,storeSetupFile,storeItemArray);

    console.log(writeStoreItemToNewServer);
    console.log(writeStoreItemToOldServer);

  };

  // START OF STORE ITEMS FILE ---------------------------------------------------------------------------------------------------------------------
    
  //Download store items files and return file names
  console.log(`Grabbing ${storeItemFile} file from FTP site.`);
  auto.autoLog(`Grabbing ${storeItemFile} file from FTP site.`, programName, 5); 

  let storeItemsResults = await fetchInformation(storeItemFile); 

  // let storeItemsResults = ['PBALL.TXT']; //Turn on during testing

  if(storeItemResults.length == 0){
    console.log(`The ${storeItemFile} file was not found on the FTP site: ${storeItemsResults.length}`);
    auto.autoLog(`The ${storeItemFile} file was not found on the FTP site: ${storeItemsResults.length}`, programName, 10); 
  }

  for (const fileName of storeItemsResults) {

    ////Format data in the file
    let storeItemArray = await formatInformation(fileName);

    console.log(`Records found in ${fileName}: ${storeItemArray.length}`);
    auto.autoLog(`Records found in ${fileName}: ${storeItemArray.length}`, programName, 5); 

    ////Skip processing file if there is no data
    if(storeItemArray.length == 0){
      console.log(`No records found in ${fileName} :${storeItemArray.length}.`);
      auto.autoLog(`No records found in ${fileName} :${storeItemArray.length}`, programName, 5); 
      continue;
    }

    ////Write formatted data to mobile frame
    let writeStoreItemToNewServer = await insertInformation(newMFDatabase,newMFCrossDockDB,storeItemFile2,storeItemArray);
    let writeStoreItemToOldServer = await insertInformation(oldMFDatabase,oldMFCrossDockDB,storeItemFile2,storeItemArray);


    console.log(writeStoreItemToNewServer);
    console.log(writeStoreItemToOldServer);
  };   

  //// END OF STORE ITEM SETUP FILE ---------------------------------------------------------------------------------------------------------------------


  //// START OF ITEM FILE ---------------------------------------------------------------------------------------------------------------------
  //If the current day is Monday process the items file
  if(currentDay == 1){
    // //Download item file and return file name
    console.log(`Grabbing ${itemFile} file from FTP site.`);
    auto.autoLog(`Grabbing ${itemFile} file from FTP site.`, programName, 5); 

    let itemResults = await fetchInformation(itemFile);

    console.log(itemResults);

    // let itemResults = ['ALLITEMS.DAT']; //Turn on during testing

    ////Format data in the file
    if(itemResults.length > 0){
      let itemArray = await formatInformation(itemResults[0]);

      console.log(`Records found in ${itemResults[0]}: ${itemArray.length}`);
      auto.autoLog(`Records found in ${itemResults[0]}: ${itemArray.length}`, programName, 5); 
  
      ////Skip processing file if there is no data
      if(itemArray.length == 0){
        console.log(`No records found in ${itemResults[0]} :${itemArray.length}.`);
        auto.autoLog(`No records found in ${itemResults[0]} :${itemArray.length}`, programName, 5); 
        return;
      } 
  
      let writeItemsToNewServer = await insertInformation(newMFDatabase,newMFCrossDockDB,itemFile2,itemArray);
      let writeItemsToNewOld = await insertInformation(oldMFDatabase,oldMFCrossDockDB,itemFile2,itemArray);

  
      console.log(writeItemsToNewServer);
      console.log(writeItemsToNewOld);
    }
  }
  // END OF STORE ITEM SETUP FILE ---------------------------------------------------------------------------------------------------------------------

  await generateTagFiles(); //choose office

  console.log(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
  auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5); 
  
  /*
    Retrieves  incoming BTC information
  */
  async function formatInformation(fileName) {
    console.log("fileName");

    console.log(fileName);

    //Splitting file
    const foltupe = fileName.split('.')[0];
    const storeNo = fileName.split('.')[1];
    const localFilePath = path.join("../", 'BTC_Files', fileName);

    let record = {};
    let records = [];

    let quotesOn = fileName == itemFile2 ? true : false;

    console.log(`Formatting data for file ${localFilePath}`);
    auto.autoLog(`Formatting data for file ${localFilePath}`, programName, 5); 

    //
    return new Promise((resolve, reject) => {
      const rl = readline.createInterface({
        input: fs.createReadStream(localFilePath),
        output: process.stdout,
        terminal: false
      });

      //Read each line in the file
      rl.on('line', (line) => {
        parse(line, {
          trim: true,
          skip_empty_lines: true,
          relax: true,
          quote: quotesOn
        }, (err, output) => {
          //Throw an error if there is an issue parsing the line data
          if (err) {
            console.error(`Error parsing line: ${err}`);
            return;
          }

          //Check if output and output[0] are defined before proceeding
          if (output && output[0]) {
            const values = output[0];

            //Assign the line data to an object then push to the return array based on the file type

            if (foltupe === storeSetupFile) { //000900CI file
              record = {
                BTC_Store_Number: values[0],
                Store_Number: values[6],
                BTC_Item_Number: values[1],
                Unit_Cost: values[2],
                Case_Cost: values[3],
                Unit_SRP: values[4],
                Case_SRP: values[5],
                RecordDate: dayjs().format('YYYYMMDD')
              };

              records.push(record);
            }

            if(foltupe === itemFile2){ //ALLITEMS.DAT file
              record = {
                BTC_Item_Number: values[0],
                Description: values[1].replace(/(^"|"$)/g, ''),
                Item_Type: values[2],
                Pack: values[3].replace(/(^"|"$)/g, ''),
                Unit_UPC: values[4],
                Carton_UPC: values[5],
                Status: values[6].replace(/(^"|"$)/g, ''),
                BTCHeader: values[7],
              }

              records.push(record);

            }

            if(foltupe == storeItemFile2){ //PBALL.TXT
              record = {
                BTC_Item_No : values[1],
                Description: values[2].replace(/(^"|"$)/g, ''), // Remove unnecessary quotes
                Pack: values[3].replace(/(^"|"$)/g, ''), // Remove unnecessary quotes
                Carton_UPC : values[8],
                Unit_SRP: values[11],
                Case_Cost : values[10],
                Store_Number : values[12],
                Pack_UPC : values[9],
              }
              records.push(record);
            }
          } else {
            console.warn(`Skipped empty or malformed line in ${localFilePath}`);
            auto.autoLog(`Skipped empty or malformed line in ${localFilePath}`, programName, 10); 

          }
        });
      });

      //After reading the whole file return results
      rl.on('close', () => {
        console.log(`Finished reading ${localFilePath}`);
        resolve(records);
      });

      //Throw an error if there are issues reading the file
      rl.on('error', (err) => {
        console.error(`Error reading file ${localFilePath}. Error: ${err}`);
        reject(err);
      });
    });
  }

  /*
    Formats incoming BTC information
  */
  async function fetchInformation(fileType) {
    const remoteDirectoryPath = '/QDLS/MAILBOX/OUT/000900';
    const localDirectoryPath = path.join('/', 'btc_files');
    const c = new Client();


    return new Promise((resolve, reject) => {
      const records = [];

      c.on('ready', function() {
        c.cwd(remoteDirectoryPath, function(err) {
          if (err) {
            reject(`Error changing directory: ${err}`);
            c.end();
            return;
          }

          c.list(async function(err, list) {
            if (err) {
              reject(`Error occurred while listing files: ${err}`);
              c.end();
              return;
            }


            let regex = "";
            
            if(fileType == itemFile || fileType == storeItemFile) {
              regex = new RegExp(`${fileType}`);
            } else {
              regex = new RegExp(`^${fileType}\\.\\d+$`);
            }

            console.log(regex);

            let filteredFiles = list.filter(file => regex.test(file.name));

            console.log(filteredFiles);

            const downloadPromises = filteredFiles.map(file => {
              const remoteFilePath = file.name;
              records.push(remoteFilePath);
              const localFilePath = path.join(localDirectoryPath, remoteFilePath);

              return new Promise((resolveDownload, rejectDownload) => {
                c.get(remoteFilePath, function(err, stream) {
                  if (err) {
                    rejectDownload(`Error downloading file: ${err}`);
                    return;
                  }

                  const writeStream = fs.createWriteStream(localFilePath);
                  stream.pipe(writeStream);

                  writeStream.on('finish', () => {
                    console.log(`File downloaded successfully: ${localFilePath}`);

                    c.delete(remoteFilePath, err => {
                      if(err){
                        rejectDownload(`Error deleting file on FTP site: ${err}`);
                      }

                      console.log(`File deleted successfully from FTP site: ${remoteFilePath}`);
                      // resolveDownload();
                    });
                    resolveDownload();
                  });

                  writeStream.on('error', err => {
                    rejectDownload(`Error writing file: ${err}`);
                  });
                });
              });
            });

            // Wait for all downloads to complete
            Promise.all(downloadPromises)
              .then(() => {
                c.end();
                resolve(records); // Resolve with the list of downloaded files
              })
              .catch(error => {
                c.end();
                reject(error);
              });
          });
        });
      });

      c.connect({
        host: '12.150.232.250',
        user: 'piggly',
        password: 'piggly'
      });

      console.log(`Connecting to FTP Site.`);
    });
  }

  /*
    Inserts incoming BTC information to Mobile Frame database
  */
  async function insertInformation(mfDatabase,crossDockDatabase,fileType, dataSet){

    console.log(`Inserting information for server ${mfDatabase}`);
    auto.autoLog(`Inserting information for server ${mfDatabase}`, programName, 5); 
    
    let message = "";

    if(fileType == itemFile2){

      console.log(`Deleting flagged items that have a status of 'D' in the ${btcItemsTable} & ${btcStoreItemsTable} tables from the previous run`);
      auto.autoLog(`Deleting flagged items that have a status of 'D' in the ${btcItemsTable} & ${btcStoreItemsTable}  tables from the previous run`, programName, 5); 

      try{
        var deleteItemsResponse = await pjs.query(pjs.getDB(crossDockDatabase), `DELETE FROM ${btcItemsTable} WHERE STATUS = 'D'`, null, null);
        var deleteStoreItemsResponse = await pjs.query(pjs.getDB(crossDockDatabase), `DELETE FROM ${btcStoreItemsTable} WHERE ITEMUPDATE = 'D'`, null, null);

      } catch(err){
        message = `Catch error occurred while deleting items from ${btcItemsTable} & ${btcStoreItemsTable}. Error: ${err}`;
        console.log(message);
        return;
      }

      console.log(`Number of items that were deleted from ${btcItemsTable} table: ${deleteItemsResponse.affectedRows}`);
      console.log(`Number of store items that were deleted from ${btcStoreItemsTable}  table: ${deleteStoreItemsResponse.affectedRows}`);

      auto.autoLog(`Number of items that were deleted from ${btcItemsTable} table: ${deleteItemsResponse.affectedRows}`, programName, 5); 
      auto.autoLog(`Number of store items that were deleted from ${btcStoreItemsTable}  table: ${deleteStoreItemsResponse.affectedRows}`, programName, 5); 


      console.log(`Deleting all items from ${btcItemsTable} table`);
      auto.autoLog(`Deleting all items from ${btcItemsTable} table`, programName, 5); 


      try {
        var deleteAllItemsResponse = pjs.query(pjs.getDB(crossDockDatabase), `DELETE FROM ${btcItemsTable}`, null, null);
      } catch (error) {
        console.error(error);
        message = `Catch error occurred while deleting all items from ${btcItemsTable} table. Error: ${err}`;
        auto.autoLog(message, programName, 20); 
        console.log(message);
        return;
      }

      console.log(deleteAllItemsResponse);

      console.log(`Inserting incoming item data into the ${btcItemsTable} table`);
      auto.autoLog(`Inserting incoming item data into the ${btcItemsTable} table`, programName, 5); 


      try{
        var insertItemsResponse = await pjs.query(
          pjs.getDB(crossDockDatabase), 
          `INSERT INTO ${btcItemsTable} (BTCItemNumber, Description, ItemType, Pack, UnitUPC, CartonUPC, Status, BTCHeader) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, 
          dataSet,null,null
        );
      } catch(err){
        console.error(err);
        message = `Catch error occurred while inserting incoming item data into the ${btcItemsTable} table. Error: ${err}`;
        auto.autoLog(message, programName, 20); 
        console.log(message);
        return;
      }
  
      console.log(`Number of items that were inserted into the ${btcItemsTable} table: ${insertItemsResponse.affectedRows}`);
      auto.autoLog(`Number of items that were inserted into the ${btcItemsTable} table: ${insertItemsResponse.affectedRows}`, programName, 5); 

      console.log(`Setting the status field for all store items to blank in the ${btcStoreItemsTable} table`);
      auto.autoLog(`Setting the status field for all store items to blank in the ${btcStoreItemsTable} table`, programName, 5); 
      console.log(`Updating the status field for all store items in the ${btcStoreItemsTable}  table to relfect the status from ${btcItemsTable} table`);
      auto.autoLog(`Updating the status field for all store items in the ${btcStoreItemsTable}  table to relfect the status from ${btcItemsTable} table`, programName, 5); 


      try{
        var removeStoreItemsStatusResponse = await pjs.query(
          pjs.getDB(crossDockDatabase), `UPDATE ${btcStoreItemsTable}  SET ItemUpdate = ''`, null, null
        );

        var updateStoreItemsStatusResponse = await pjs.query(
          pjs.getDB(crossDockDatabase), 
          `UPDATE si SET si.ItemUpdate = i.Status FROM ${btcStoreItemsTable}  si INNER JOIN ${btcItemsTable} i ON si.BTCItem = i.BTCItemNumber`, 
          null,null
        );
      
      } catch(err){
        message = `Catch error occurred while removing status codes or adding new status codes to store items in the ${btcItemsTable} table. Error: ${err}`;
        auto.autoLog(message, programName, 20); 
        console.log(message);
        return;
      }
  
      console.log(`Number of store items that were updated with a status of '': ${removeStoreItemsStatusResponse.affectedRows}`);
      auto.autoLog(`Number of store items that were updated with a status of '': ${removeStoreItemsStatusResponse.affectedRows}`, programName, 5); 

      console.log(`Number of store items that were updated with a status to reflect the status from items table: ${updateStoreItemsStatusResponse.affectedRows}`);
      auto.autoLog(`Number of store items that were updated with a status to reflect the status from items table: ${updateStoreItemsStatusResponse.affectedRows}`, programName, 5); 

      console.log(`Updating ${btcStoreItemsTable} & ${btcStoreItemsTable2} tables with updated item and store item information for any items with a status of 'U'`);
      auto.autoLog(`Updating ${btcStoreItemsTable} & ${btcStoreItemsTable2} tables with updated item and store item information for any items with a status of 'U'`, programName, 5); 

      try{
        var updateStoreItemsWithItemInfo = await pjs.query(
          pjs.getDB(mfDatabase), 
          `UPDATE [MobileFrame].[dbo].${btcStoreItemsTable2} 
          SET Description = i.Description, Pack = i.Pack, Carton_UPC = i.CartonUPC, Pack_UPC = i.UnitUPC
          FROM PW_BTC_ITEM INNER JOIN [CrossDock].[dbo].[BTC_Items] as i ON PW_BTC_ITEM.BTC_ITEM_NO = i.BTCItemNumber
          WHERE i.Status = 'U'`, 
          null,null
        );
      } catch(err){
        message = `Catch error occurred while updating items in ${btcStoreItemsTable2} tables. Error: ${err}`;
        // auto.autoLog(message, programName, 20); 
        console.log(message);
        return;
      }
    
      console.log(updateStoreItemsWithItemInfo);
    
      try{
        var updateStoreItemsWithStoreItemInfo = await pjs.query(
          pjs.getDB(mfDatabase), 
          `UPDATE [MobileFrame].[dbo].${btcStoreItemsTable2} 
          SET CASE_COST = SI.CaseCost, Unit_SRP = SI.CaseCost
          FROM PW_BTC_ITEM as BI
          INNER JOIN [CrossDock].[dbo].[BTC_StoreItems] as SI ON BI.BTC_ITEM_NO = SI.BTCItem and BI.Store_Number = SI.StoreNumber
          WHERE SI.ItemUpdate = 'U'`, 
          null,null
        );
      } catch(err){
        message = `Catch error occurred while updating items in ${btcStoreItemsTable2} tables. Error: ${err}`;
        // auto.autoLog(message, programName, 20); 
        console.log(message);
        return;
      }
    
      console.log(updateStoreItemsWithStoreItemInfo);
      return;

    } else if(fileType == storeSetupFile){
      console.log(`Inserting store item records into ${btcStoreItemsTable} `);
      auto.autoLog(`Inserting store item records into ${btcStoreItemsTable} `, programName, 5); 

      let insertStoreItemsQuery = `
        INSERT INTO ${btcStoreItemsTable}  (BTCCustomerID, StoreNumber, BTCItem, UnitCost, CaseCost, UnitSRP, CaseSRP, RecordDate)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?)
      `;

      try{
        var insertResponse = await pjs.query(pjs.getDB(crossDockDatabase), insertStoreItemsQuery, dataSet,null,null);
      }catch(err){
        message = `Catch error occurred while inserting store item data into the ${btcStoreItemsTable}  table. Error: ${err}`;
        console.log(message);
        auto.autoLog(message, programName, 5); 
      }
  
      console.log(insertResponse);
  
      return;

    } else if(fileType == storeItemFile2){

      let insertStoreItemsQuery = `INSERT INTO ${btcStoreItemsTable2}  (BTC_ITEM_NO, DESCRIPTION, PACK, CARTON_UPC, UNIT_SRP, CASE_COST, STORE_NUMBER, PACK_UPC)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;

      try{
        var insertResponse = await pjs.query(pjs.getDB(mfDatabase), insertStoreItemsQuery, dataSet,null,null);
  
      } catch(err){
        console.log(err);
        message = `Catch error occurred while inserting store item data into the ${btcStoreItemsTable2}  table. Error: ${err}`;
        console.error(message);
        auto.autoLog(message, programName, 5); 
      }
  
      console.log(insertResponse);

    } else {
      console.log(`A valid file type was not passed in. Filetype ${fileType} `);
      auto.autoLog(`A valid file type was not passed in. Filetype ${fileType}  `, programName, 20); 
    }
    
    return;
  }

  /*
    Generates txt files for each store that have updated item information
  */
  async function generateTagFiles(){

    //Declare path to hot folder
    const directoryPath =  '\\\\cfdata\\BartenderHOT\\BTC_PriceChangeTags';
    // const directoryPath = path.join("../", 'BTC_Files'); //Turn on during testing


    //Declare sql query
    const retrieveUpdatedStoreItemsQuery =  `
      SELECT
        si.StoreNumber, 
        si.BTCItem, 
        si.UnitSRP, 
        si.CaseSRP,
        si.ItemUpdate,
        i.Description,
        i.Pack,
        i.UnitUPC
      FROM [CrossDock].[dbo].[BTC_StoreItems] as si
      LEFT JOIN [CrossDock].[dbo].[BTC_Items] as i ON si.BTCItem = i.BTCItemNumber
      WHERE 
        si.ItemUpdate != ''
      ORDER BY si.StoreNumber    
    `;

    //Check if the directory is accessable
    fs.access(directoryPath, fs.constants.F_OK, (err) => {
      if(err){
        console.error(`${directoryPath} is not accessible: ${err}`);
        auto.autoLog(`${directoryPath} is not accessible: ${err}`, programName, 20); 
        return;
      }
    });

    console.error(`${directoryPath} is accessible`);
    auto.autoLog(`${directoryPath} is accessible`, programName, 5); 

    console.log(`Fetching all store items that are flagged with a status.`);
    auto.autoLog(`Fetching all store items that are flagged with a status.`, programName, 5); 

    try{
      var flaggedStoreItems = await pjs.query(pjs.getDB(newMFCrossDockDB),retrieveUpdatedStoreItemsQuery, null,null);
    } catch(err){
    
      console.log(`Catch error occurred while fetching all store items that are flagged with a status. Error: ${err}`);
      auto.autoLog(`Catch error occurred while fetching all store items that are flagged with a status. Error: ${err}`, programName, 5); 
      return;
    }

    //Declare CSV file column headers
    const headers = [
      {id: 'StoreNumber', title: 'StoreNumber' },
      {id: 'BTCItemNum', title: 'BTCItemNum' },
      {id: 'UnitSRP', title: 'UnitSRP' },
      {id: 'CaseSRP', title: 'CaseSRP' },
      {id: 'ChangeStatus', title: 'ChangeStatus' },
      {id: 'Description', title: 'Description' },
      {id: 'PackSize', title: 'PackSize' },
      {id: 'UPC', title: 'UPC' },
    ]

    const groupByStore = {};

    //Iterate through each store item record
    flaggedStoreItems.forEach((record) => {
      //Set the store from current record
      const storeNum = record.StoreNumber;
      //Check if the groupByStore object already has an array for this store
      if(!groupByStore[storeNum]){
        //If not, initilze an empty array for this store
        groupByStore[storeNum] =[];
      }

      //Add the current record to the array for the this store
      groupByStore[storeNum].push({
        StoreNumber : String(record.StoreNumber).trim(),
        BTCItemNum : String(record.BTCItem).trim(),
        UnitSRP : String(parseFloat(record.UnitSRP).toFixed(2)),
        CaseSRP : String(parseFloat(record.CaseSRP).toFixed(2)), 
        ChangeStatus : String(record.ItemUpdate.trim()),
        Description : String(record.Description.trim()),
        PackSize: String(record.Pack.trim()),
        UPC: String(record.UnitUPC.trim())
      });
    });

    //Iterate thrugh each entry in the groupByStore object
    Object.entries(groupByStore).forEach(([storeNum, storeRecords]) => {
      //Define path with store file name
      const localFilePath = path.join(directoryPath, `Store ${storeNum}.csv`);
      //Create CSV writer instance with the specified path and column headers
      const csvWriter = csv({
        path : localFilePath,
        header: headers,

      });
      csvWriter
        .writeRecords(storeRecords) //Write the records for the current store to CSV file
        .then(() => console.log(`CSV created for store ${storeNum}`)) //Log sucess message once CSV file is create
        .catch((err) => console.error(`Catch error occurred while creating CSV file for store ${storeNum}. Error ${err}`)); //Log an error if the CSV did not get created
    });
  }

  /*
    Send email notification to IT
  */
  async function sendEmail(subject, message){
      console.log(`sendEmail is running...`);
      
      let emailSend =
      `SNDSMTPEMM RCP((sgray@pwadc.com)) SUBJECT('${subject}') ` +
      `NOTE('${message}')`;

      try {
        pjs.runCommand(emailSend);
      }
      catch(err) {
        console.error("Error sending email:", err);
      }

    return;
  }
}

exports.run=MF_BTC_Import;
