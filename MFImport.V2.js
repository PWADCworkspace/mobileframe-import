const dayjs = require("dayjs");
/*

  Created By: Samuel Gray
  Date Create: 10/18/2024
  Modified By: Samuel Gray
  Modified Date: 11/20/2024


  Process:
  - This program is called automatically via CLVOCLINK. once its finished running it will run again after a 120 second delay.
  - This program will read PW_ORDER_HEADER on the Mobile Frame SQL database to look for headers that have a complete count of detail records.
  - When a complete header is found it will query PW_ORDER_DETAIL with that headers GUID to pull all of the associated detail records. 
  - Those detail records will be written to PWAFIL.ORDERSIN on the iSeries. After which, this program will call CLORDERSIN which will process those orders.
  - It will then flag the detail records on the SQL db that have been imported and flag the header record as well to show that its records were imported.
  - Finally, if it is between 11 & 11:15 am it will import all header records regardless of whether or not they have all detail records in. 
  - If any incomplete headers are found and their details are imported it will flag that header record on the SQL db with the actual number of records that were imported.
  - Error alerts are built in throughout using the errorNotify function which uses MFIMPORT.SNDEMAIL and MFIMPORT.SNDMESSAGE CL programs.
*/

async function mobileFrameImportV2() {
  // #region Program-level variables
  //Import modules
  const auto = pjs.require("pjslogging/autoLog.js");

  const programName = "MF_Import_V2";
  const currentPort = profound.settings.port;

  //Define MF server instances
  const oldMFServer = "Server2012-MFS";
  const newMFServer = "MF-SQL";

  //Define tables
  let headerTable = "HeaderCopy";
  let detailTable = "DetailCopy";
  let programForCLCall = "CLORDERSIX"; // ISeries test pgm
  let programForEmail = "SNDEMAIL25";

  pjs.defineTable("ordersin", "ORDERSIN", { write: true, userOpen: true, keyed: true }); // ISeries holding table
  // pjs.defineTable("ordersinh","ORDERSINH", {write: true, userOpen: true, keyed:true}); // ISeries holding table

  //Use prod tables and pgms on 8083 instance
  if (currentPort === 8083) {
    headerTable = "PW_ORDER_HEADER";
    detailTable = "PW_ORDER_DETAIL";
    programForCLCall = "CLORDERSIN";
    programForEmail = "SNDEMAIL";
  }
  // #endregion

  auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`, programName, 5);
  console.log(`${programName} is running on  ${currentPort}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`);

  //Retrieve orders from new Mobile Frame Server
  await fetchHeaderRecords(newMFServer);

  //Retrieve orders from the current(old) Mobile Frame Server
  await fetchHeaderRecords(oldMFServer);

  console.log(`Calling ${programForCLCall} program.`);
  auto.autoLog(`Calling ${programForCLCall} program.`, programName, 5);

  //Call CL pgm to move orders downstream to history table & other tables as well
  try {
    pjs.call(programForCLCall);
  } catch (err) {
    console.log(`Catch error occurred while calling ${programForCLCall} program. Error: ${err}.`);
    auto.autoLog(`Catch error occurred while calling ${programForCLCall} program. Error: ${err}.`, programName, 20);
  }

  /*
    Retreive ALL unflagged order HEADER records
  */

  async function fetchHeaderRecords(serverName) {
    let getHeaderCount = [];
    let message = "";

    //Declare header query
    const mfHeaderQuery = `
      SELECT 
        oh.GUID,
        oh.id As HeaderID,
        oh.FLAGGED,
        oh.LINE_COUNT,
        oh.STORE_NO,
        count(od.id) as DetailCount
      FROM ${headerTable} AS oh
      INNER join ${detailTable} AS od ON od.ORDER_HEADER_GUID = oh.GUID
      WHERE oh.FLAGGED = '' AND oh.deleted_by < 0  AND oh.VENDOR_NO not in ('4','5','7','8','10')
      GROUP BY oh.id, oh.GUID, oh.LINE_COUNT, od.deleted_by, oh.FLAGGED,oh.STORE_NO
    `;

    auto.autoLog(`Fetching incoming header orders from ${headerTable} table from ${serverName} server`, programName, 5);
    console.log(`Fetching incoming header orders from ${headerTable} table from ${serverName} server`);

    //Execute query to retrieve ALL unflagged order header records
    try {
      getHeaderCount = pjs.query(pjs.getDB(serverName), mfHeaderQuery);
    } catch (err) {
      //If an error occurres email IT and end the pgm
      message = `Server: ${serverName} \n Catch error occurred while fetching orders from ${headerTable}. Error: ${err}`;
      console.log(message);
      auto.autoLog(message, programName, 20);
      errorNotify(message, 10);
      return;
    }

    auto.autoLog(`Header record count returned from ${headerTable} table: ${getHeaderCount.length}`, programName, 5);
    console.log(`Header record count returned from ${headerTable} table: ${getHeaderCount.length}`);

    //Loop through each order header record
    for (let i = 0; i < getHeaderCount.length; i++) {
      if (!getHeaderCount[i]["STORE_NO"] || getHeaderCount[i]["STORE_NO"] == null) {
        message = `Server: ${serverName} \n The store number field is not populated for guid ${getHeaderCount[i]["GUID"]}: ${getHeaderCount[i]["STORE_NO"]}.`;
        console.log(message);
        auto.autoLog(message, programName, 20);
        errorNotify(message, 10);
        continue;
      }

      if (!getHeaderCount[i]["HeaderID"] || getHeaderCount[i]["HeaderID"] == null) {
        message = `Server: ${serverName} \n The headerID field is not populated for guid ${getHeaderCount[i]["GUID"]}: ${getHeaderCount[i]["HeaderID"]}.`;
        console.log(message);
        auto.autoLog(message, programName, 20);
        errorNotify(message, 10);
        continue;
      }

      if (!getHeaderCount[i]["LINE_COUNT"] || getHeaderCount[i]["LINE_COUNT"] == null) {
        message = `Server: ${serverName} \n The line count field is not populated for guid ${getHeaderCount[i]["GUID"]}: ${getHeaderCount[i]["LINE_COUNT"]}.`;
        console.log(message);
        auto.autoLog(message, programName, 20);
        errorNotify(message, 10);
        continue;
      }

      if (getHeaderCount[i]["LINE_COUNT"] !== getHeaderCount[i]["DetailCount"]) {
        message = `Server: ${serverName} \n The header line count and detail line count for guid ${getHeaderCount[i]["GUID"]} and store ${getHeaderCount[i]["STORE_NO"]} do not match.`;
        console.log(message);
        auto.autoLog(message, programName, 20);
        errorNotify(message, 10);
        continue;
      }

      //Call fetchDetailRecords function retreive the detail records for the headers records valid orders
      if (getHeaderCount[i]["LINE_COUNT"] === getHeaderCount[i]["DetailCount"]) {
        let completeGUIDs = getHeaderCount[i]["GUID"];

        // orderDetailQuery(completeGUIDs);
        fetchDetailRecords(serverName, completeGUIDs);
      }
    }
  }

  /*
    Retreive ALL unflagged order DETAIL records
  */
  async function fetchDetailRecords(serverName, guid) {
    let getDetailRecord = [];
    let message = "";

    const detailQueryParams = { valueGUID: guid };
    //Declare detail query
    const detailQuery = `
      SELECT
      od.id AS DetailId, 
      oh.id AS HeaderId, 
      od.LINE_ITEM_NO AS lineItmNum, 
      od.STORE AS Store, 
      oh.ORDER_DT AS OrderDate, 
      oh.ORDER_TYPE AS orderType, 
      oh.VENDOR_NO AS vendorNumber, 
      od.QUANTITY AS quantity, 
      od.ORDER_HEADER_GUID AS Guid 
      FROM ${headerTable} AS oh 
      INNER JOIN ${detailTable} AS od 
      ON od.ORDER_HEADER_GUID = oh.GUID 
      WHERE od.FLAGGED = '' 
      AND od.deleted_by < 0 
      AND oh.GUID = @valueGUID
    `;

    //Execute query to retrieve ALL unflagged order detail records
    try {
      getDetailRecord = pjs.query(pjs.getDB(serverName), detailQuery, detailQueryParams);
    } catch (err) {
      //If an error occurres email IT and end the pgm
      message = `Server: ${serverName} \n Catch error occurred while fetching detail records from ${detailTable} for guid ${guid}. Error: ${err}`;
      console.log(message);
      // auto.autoLog(message, programName, 5);
      errorNotify(message, 10);
    }

    //If there are no unflagged detail records for the GUID send email to IT and skip processing that header record
    if (!getDetailRecord || getDetailRecord.length <= 0) {
      message = `Server: ${serverName} \n No detail records found for GUID ${guid}.`;
      auto.autoLog(message, programName, 20);
      console.log(message);

      errorNotify(message, 10);
      return;
    }

    auto.autoLog(
      `Detail record count returned from ${serverName} from ${detailTable} table for GUID ${guid}: ${getDetailRecord.length}`,
      programName,
      5
    );
    console.log(`Detail record count returned from ${serverName} from ${detailTable} table for GUID ${guid}: ${getDetailRecord.length}`);

    //Call writeToOrdersIn function to write detail orders to iSeries system
    ordersin.open();
    await writeToOrdersIn(serverName, getDetailRecord);
    ordersin.close();
  }

  /*
    Write orders to iSeries table
  */
  async function writeToOrdersIn(serverName, detailRecords) {
    let message = "";
    const processDate = dayjs().format("YYYY-MM-DD HH:mm:ss.SSS");

    //Loop through each detail record
    for (let i = 0; i < detailRecords.length; i++) {
      var guidVal = detailRecords[i]["Guid"];
      var detailIDRecord = detailRecords[i]["DetailId"];
      var callID = detailRecords[i]["HeaderId"];
      var originalStoreNumber = detailRecords[i]["Store"];
      var type = detailRecords[i]["orderType"];
      var vendor = detailRecords[i]["vendorNumber"];
      var lineItemNumber = detailRecords[i]["lineItmNum"];
      var originalQty = detailRecords[i]["quantity"];
      var originalDate = detailRecords[i]["OrderDate"];
      var processed = "";
      var Flag1 = "";
      var Flag2 = "";

      if (!detailIDRecord || detailIDRecord == null) {
        message = `Server: ${serverName} \n The DetailId field is not populated in the detail table for guid ${guidVal}.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      if (!callID || callID == null) {
        message = `Server: ${serverName} \n The HeaderId field is not populated in the header table for guid ${guidVal}.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      if (!originalStoreNumber || originalStoreNumber == null) {
        message = `Server: ${serverName} \n The Store field is not populated in the detail table for guid ${guidVal}.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      if (!type || type == null) {
        message = `Server: ${serverName} \n The orderType field is not populated in the detail table for guid ${guidVal}.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      // If vendor is null, no results will be returned from the query
      // if (!vendor || vendor == null) {
      //   message = `Server: ${serverName} \n The vendor field is not populated in the header table for guid ${guidVal}.`;
      //   auto.autoLog(message, programName, 20);
      //   console.log(message);
      //   errorNotify(message, 10);
      //   return;
      // }

      if (!lineItemNumber || lineItemNumber == null) {
        message = `Server: ${serverName} \n The lineItmNum field is not populated in the detail table for guid ${guidVal}.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      if (!originalQty || originalQty == null) {
        message = `Server: ${serverName} \n The quantity field is not populated in the detail table for guid: ${guidVal}, detail id: ${detailIDRecord}, LINE_ITEM_NO: ${lineItemNumber}. Setting quantity to "1".`;
        auto.autoLog(message, programName, 20);
        console.log(message);

        originalQty = 1;

        errorNotify(message, 10);
        // return;
      }

      if (!originalDate || originalDate == null) {
        message = `Server: ${serverName} \n The OrderDate field is not populated in the header table for guid ${guidVal}.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      var storeNumber = originalStoreNumber.toString().padStart(3, "0");
      var orderDate = convertDate(originalDate);
      if (originalQty < 1) originalQty = 1;
      var qty = originalQty.toString().padStart(3, "0");

      oincal = callID;
      oinstr = storeNumber;
      ointyp = type;
      oinvnd = vendor;
      oinitm = lineItemNumber;
      oinqty = qty;
      oindte = orderDate;
      oinprs = processed;
      oinfg1 = Flag1;
      oinfg2 = Flag2;

      let duplicateLineItem = await checkForDuplicates(callID, storeNumber, type, vendor, lineItemNumber, qty, orderDate);

      if (duplicateLineItem) {
        message = `Server: ${serverName} \n There was a duplicate line item in ordersinh for detailIDRecord ${detailIDRecord} and guid ${guidVal}. Moving to next detail record.`;
        auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 10);
        return;
      }

      ordersin.write();

      let detailParms = {
        valueDate: processDate,
        detailID: detailIDRecord,
      };

      //Declare detail update query
      var detailTestQuery = `UPDATE ${detailTable} SET flagged = 'b', last_update = @valueDate, last_updated_by = -1 WHERE id = @detailID`;

      auto.autoLog(`Flagging detail record ${callID} in ${detailTable} table`, programName, 5);
      console.log(`Flagging detail record ${callID} in ${detailTable} table`);

      //Execute query to flag the detail record once inserted into iSeries table
      try {
        var detailFlagResponse = pjs.query(pjs.getDB(serverName), detailTestQuery, detailParms);
      } catch (err) {
        //If an error occurres email IT and move to next record
        message = `Server: ${serverName} \n Catch error occurred while flagging detail record ${detailIDRecord} in ${detailTable}. Error: ${err}`;
        // auto.autoLog(message, programName, 20);
        console.log(message);
        errorNotify(message, 20);
        return;
      }

      console.log(detailFlagResponse);
      //On the last detail record flag the assiocated header record as processed
      if (i == detailRecords.length - 1) {
        let headerParms = {
          writeDateRecord: processDate,
          timeStampRecord: dayjs().format("YYYY-MM-DD HH:mm:ss"),
          callIDRecord: callID,
        };

        let headerTestQuery = `UPDATE ${headerTable} SET flagged = 'b', last_update = @writeDateRecord, last_updated_by = -1, EXPORTED = @timeStampRecord WHERE id = @callIDRecord`;

        auto.autoLog(`Flagging header record ${callID} in ${headerTable} table`, programName, 5);
        console.log(`Flagging header record ${callID} in ${headerTable} table`);

        //Execute query to flag the header record
        try {
          pjs.query(pjs.getDB(serverName), headerTestQuery, headerParms);
        } catch (err) {
          //If an error occurres email IT and move to next record
          message = `Server: ${serverName} \n Catch error occurred while flagging header record ${callID} in ${headerTable}. Error: ${err}`;
          // auto.autoLog(message, programName, 20);
          console.log(message);

          errorNotify(message, 20);
          return;
        }
      }
    }
  }

  async function checkForDuplicates(callID, storeNumber, type, vendor, lineItemNumber, qty, orderDate) {
    let duplicateOrder = false;
    let message = "";
    let duplicateOrders = [];

    try {
      duplicateOrders = pjs.query(
        `SELECT * FROM ordersinh WHERE oincal = ? AND oinstr = ? AND ointyp = ? AND oinvnd = ? AND oinitm = ? AND oinqty = ? AND oindte = ?`,
        [callID, storeNumber, type, vendor, lineItemNumber, qty, orderDate]
      );
    } catch (err) {
      message = `Catch error occurred validating for duplicate line order for ${callID}. Error: ${err}`;
      auto.autoLog(message, programName, 20);
      console.log(message);
      duplicateOrder = true;
      // errorNotify(message, 20);
      return duplicateOrder;
    }

    if (duplicateOrders.length > 0) {
      duplicateOrder = true;
    }

    return duplicateOrder;
  }

  console.log(`${programName} is done running on  ${currentPort}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`);
  auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`, programName, 5);

  function convertDate(dateIn) {
    var D = dateIn.toISOString();
    D = D.replace(/-|:/g, "");
    D = D.replace(/T/g, " ");
    var newDate = D.slice(0, -3);
    return newDate;
  }

  function errorNotify(error, severity) {
    //severity   action
    // 10        console.log
    // 20        email + console.log
    // 30        send message to qsysopr on 400 + email + console.log

    let messagePrefix = ` Mobile Frame Import Error (${currentPort}): \n `;

    pjs.define("message", { type: "char", length: 512 });
    message = messagePrefix + error;

    if (severity >= 10) {
      pjs.call(programForEmail, message);
    }

    if (severity >= 20) {
      pjs.call("SNDMESSAGE", message); //  TYLER no message was sent
    }
  }
}

exports.run = mobileFrameImportV2;
